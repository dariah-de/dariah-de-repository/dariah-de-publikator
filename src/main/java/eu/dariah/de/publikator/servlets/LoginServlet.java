package eu.dariah.de.publikator.servlets;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.beans.User;
import eu.dariah.de.publikator.utils.I18NUtils;

/**
 * - #%L DARIAHDE :: Publikator Standalone
 * 
 * %% Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de) %%
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 */

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends BaseServlet {

  private static final long serialVersionUID = 6457559609849560357L;
  private static final Logger LOG = Logger.getLogger(EditView.class.getName());
  private static final String views[] = {"logout",};
  private static final String MAIN_VIEW = "mainView";

  /**
   * 
   */
  public LoginServlet() {
    super(new HashSet<String>(Arrays.asList(views)));
  }

  /*
   * (non-Javadoc)
   * 
   * @see eu.dariah.de.publikator.servlets.BaseServlet#index(javax.servlet.http.HttpServletRequest,
   * javax.servlet.http.HttpServletResponse)
   */
  @Override
  protected void index(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // if user is already logged in, redirect him to the main view. for some reason the user has
    // accessed the login servlet twice
    if (request.getSession(false) != null) {
      if (request.getSession(false).getAttribute("authenticated") != null) {
        if ((Boolean) request.getSession(false).getAttribute("authenticated") == true) {
          response.sendRedirect(MAIN_VIEW);

          return;
        }
      }
    }

    boolean valid = false;

    // set default redirection params, so we can call mainView with a dummy token if locally
    // authenticated
    String paramsIfLocallyAuthenticated = "";

    // check if publikator is configured for use with Shibboleth AAI, then the Shibboleth header
    // variables needs to be present
    if (Configuration.getInstance().getConfig().isEnableAAI()) {

      LOG.info("Performing Login via Shibboleth");

      // check if there is really a shibboleth session present
      if (request.getHeader("shib-session-id") != null) {
        request.getSession(true).setAttribute("authenticated", true);
        createShibbolethUserObject(request);
        valid = true;
      } else {
        LOG.severe(
            "Publikator is configured for Shibboleth, but no Shibboleth session has been found. "
                + "Please check you Shibboleth configuration or set the configuration option enableAAI to false.");
      }
    }

    // Publikator is configured for local logins. check the credentials, but only if the form has
    // been submitted
    else {
      if (request.getParameterMap().containsKey("username")) {
        valid = checkLocalCredentials(request);
      }
      if (valid) {
        request.getSession(true).setAttribute("authenticated", true);
        paramsIfLocallyAuthenticated = "?view=token&access_token="
          +Configuration.getInstance().getConfig().getLocalToken();
      }
    }

    // translation array
    request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));

    // no login yet or invalid, show login page
    if (!valid) {
      RequestDispatcher rd = request.getRequestDispatcher("views/login.jsp");
      rd.forward(request, response);
      return;
    }

    // login OK, redirect to the index page
    String redirectTo = MAIN_VIEW + paramsIfLocallyAuthenticated;

    LOG.info("valid: " + valid);
    LOG.info("Redirecting to: " + redirectTo);

    response.sendRedirect(redirectTo);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void logout(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.getSession().invalidate();

    // For Shibboleth redirect the user to the Shibboleth logout page
    if (Configuration.getInstance().getConfig().isEnableAAI()) {
      response.sendRedirect(Configuration.getInstance().getConfig().getLogoutAAI());
    } else {
      response.sendRedirect("login");
    }
  }

  /**
   * Populates the User bean with the Shibboleth header information
   * 
   * @param request
   */
  private static void createShibbolethUserObject(HttpServletRequest request) {

    // we put all header informations into a hash map. this makes null check and access easier
    Map<String, String> headerElements = new HashMap<String, String>();

    Enumeration<String> headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = (String) headerNames.nextElement();
      Enumeration<String> values = request.getHeaders(headerName);
      // note: we only support one value per header entry here
      if (values != null) {
        String value = values.nextElement();
        headerElements.put(headerName, value);
      }
    }

    String displayname = "";
    String eppn = "";
    String affilation = "";
    String email = "";
    String organisation = "";
    String preferredLanguage = "";
    List<String> memberList = new ArrayList<String>();

    if (headerElements.containsKey("displayname")) {
      displayname = fixEncoding(headerElements.get("displayname"));
    }
    if (headerElements.containsKey("eppn")) {
      eppn = fixEncoding(headerElements.get("eppn"));
    }
    if (headerElements.containsKey("affiliation")) {
      affilation = fixEncoding(headerElements.get("affiliation"));
    }
    if (headerElements.containsKey("mail")) {
      email = fixEncoding(headerElements.get("mail"));
    }
    if (headerElements.containsKey("o")) {
      organisation = fixEncoding(headerElements.get("o"));
    }
    // value seems not to be supplied by shibboleth, but maybe in the future?
    if (headerElements.containsKey("preferredlanguage")) {
      preferredLanguage = fixEncoding(headerElements.get("preferredlanguage"));
    }
    if (headerElements.containsKey("ismemberof")) {
      String members = fixEncoding(headerElements.get("ismemberof"));
      // member of is divided by ;
      memberList = Arrays.asList(members.split(";"));
    }

    User user =
        new User(eppn, affilation, memberList, displayname, email, organisation, preferredLanguage);

    if (Configuration.getInstance().getConfig().isDebug()) {
      LOG.info(
          "Created the following user object from the shibboleth information: " + user.toString());
    }

    // now add the bean to the current session
    request.getSession().setAttribute("userObject", user);
  }


  /**
   * <p>
   * Somehow the Shibboleth parameters have a wrong encoding, not UTF-8. Try to convert from
   * ISO8859-1 to UTF-8
   * http://shibboleth.1660669.n2.nabble.com/Shibboleth-Tomcat-AJP-and-UTF-8-conversion-td7628986.html
   * </p>
   * 
   * @param field
   * @return
   */
  private static String fixEncoding(String field) {

    String converted = null;

    try {
      converted = new String(field.getBytes("ISO-8859-1"), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      LOG.info("failed to convert the the field from ISO8859-1 to UTF : " + field);
      converted = field;
    }

    return converted;
  }

  /**
   * @param request
   * @return
   */
  private static boolean checkLocalCredentials(HttpServletRequest request) {

    boolean validCredentials = false;

    LOG.info("Performing local login without Shibboleth");

    String username = null;
    String password = null;

    if (request.getParameterMap().containsKey("username")) {
      username = request.getParameter("username");
    }
    if (request.getParameterMap().containsKey("password")) {
      password = request.getParameter("password");
    }

    if (username != null && password != null) {
      // simply check the username and password against the values in web.xml. we don't use
      // shibboleth here
      if (username.equals(Configuration.getInstance().getConfig().getEppn())
          && password.equals(Configuration.getInstance().getConfig().getLocalPassword())) {
        validCredentials = true;

        // populate some parts from the user bean to mimic the same behavior as the shibboleth login
        User user = new User(Configuration.getInstance().getConfig().getEppn(), "",
            new ArrayList<String>(), username, "", "", "");
        request.getSession().setAttribute("userObject", user);

      } else {
        // set an attribute so that we can show a message on the html page
        request.setAttribute("loginInvalid", true);
      }
    }

    LOG.info("Credentials valid: " + validCredentials);

    return validCredentials;
  }

}
