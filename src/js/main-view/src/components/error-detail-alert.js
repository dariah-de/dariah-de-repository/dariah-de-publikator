/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './error-detail-alert.html';
import $ from 'jquery';

let ErrorDetailAlert = Ractive.extend({
  template : template,

  onrender: function() {
    var collectionId = this.get('storageId');
    $.getJSON(this.get('getPublishStatusUrl'), {
      collectionId : collectionId
    }).done((data) => {
      this.set('publishResponse', data);
      this.set('objects', data.publishObjects);
    }).fail((data) => {
      console.log("++ error fetching error status for " + collectionId + " ++");
          this.set('status', 'service-not-available');
    });
  },
    data: {
      getStorageId: function(url) {
        return /[^/]*$/.exec(url)[0];
      },
    },
});

export default ErrorDetailAlert;
