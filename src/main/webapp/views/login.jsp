<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file="header.jsp" %>

    <style type="text/css">

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
      }
      .form-signin .form-signin-heading {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>


    <div class="container">

      <form class="form-signin" action="login" method="post">
        <h2 class="form-signin-heading">${i18n['login-please']}</h2>
        <c:if test="${requestScope.loginInvalid}">${i18n['login-invalid']}</c:if>
        <input type="text" class="input-block-level" placeholder="${i18n['login-username']}" name="username">
        <input type="password" class="input-block-level" placeholder="${i18n['login-password']}" name="password">
        <button class="btn btn-large btn-primary" type="submit">${i18n['login-button']}</button>
      </form>

    </div> 

<%@ include file="footer.jsp" %>
