var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/dariah-main-view.js',
    output: {
        filename: '../../main/webapp/js/dariah-main-view.js',
        library: 'DariahMainView',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                loader: 'raw'
            },
            {
              test: /\.css$/,
              loader: ExtractTextPlugin.extract('style', 'css')
            },
            {
              test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
              loader: 'url'
            }
        ]
    },
    // This is to load polyfills (http://mts.io/2015/04/08/webpack-shims-polyfills/)
    plugins: [
        new webpack.ProvidePlugin({
            'es6-promise': 'es6-promise'
        }),
        new ExtractTextPlugin('./dist/css/dariah-main-view.css'),
    ]
};
