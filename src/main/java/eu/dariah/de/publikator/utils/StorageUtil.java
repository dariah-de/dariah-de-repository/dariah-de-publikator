package eu.dariah.de.publikator.utils;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Utils
 * %%
 * Copyright (C) 2019 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.zip.Adler32;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.backend.DariahStorageClientWrapper;
import eu.dariah.de.publikator.beans.Collection;
import eu.dariah.de.publikator.beans.StatusMessage;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.RDFConstants;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

/**
 *
 */
public class StorageUtil {

  /**
   * @param storageClient
   * @param storageId
   * @return
   * @throws NumberFormatException
   * @throws IOException
   */
  public static String getTitle(DariahStorageClient storageClient, String storageId)
      throws NumberFormatException, IOException {
    InputStream data = storageClient.readFile(storageId);
    return RDFHelper.dcTitleFromInputStream(data, storageId);
  }

  /**
   * @param storageClient
   * @param container
   * @param unnamedCollectionTitle
   * @throws NumberFormatException
   * @throws IOException
   */
  public static Collection fetchTitleAndDescription(DariahStorageClientWrapper wrapper,
      Collection container, String unnamedCollectionTitle, String pdpToken, String logID,
      Locale locale) throws NumberFormatException {

    String storageId = container.getStorageId();
    String title = unnamedCollectionTitle + " " + storageId;
    String description = "";
    String creator = "";
    String seafileRef = "";
    String error = "";
    int numberOfCollections = -1;
    int numberOfFiles = -1;
    Collection collection;

    try {
      Model model = ModelFactory.createDefaultModel();

      RDFHelper.readWholeCollectionAsModel(model, wrapper, storageId, pdpToken, logID);

      String dcNamespace = model.getNsPrefixURI(RDFConstants.DC_PREFIX);
      String dariahNamespace = model.getNsPrefixURI(RDFConstants.DARIAH_PREFIX);
      String storageUrl = model.getNsPrefixURI(RDFConstants.DARIAHSTORAGE_PREFIX);
      Resource res = model.getResource(storageUrl + storageId);

      Property p = model.createProperty(dcNamespace, "title");
      StmtIterator iter = res.listProperties(p);
      if (iter.hasNext()) {
        title = iter.next().getString();
      }

      p = model.createProperty(dcNamespace, "description");
      iter = res.listProperties(p);
      if (iter.hasNext()) {
        description = iter.next().getString();
      }

      p = model.createProperty(dcNamespace, "creator");
      iter = res.listProperties(p);
      if (iter.hasNext()) {
        creator = iter.next().getString();
      }

      p = model.createProperty(dariahNamespace, "describesSeafileLibrary");
      iter = res.listProperties(p);
      if (iter.hasNext()) {
        seafileRef = iter.next().getString();
      }

      int[] counts = RDFHelper.countCollectionItems(model, logID);

      // Note: if you have just created a new collection and you did not type in any title, the RDF
      // just looks like
      // @prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .
      // @prefix dariahstorage: <https://de.dariah.eu/storage/> .
      // Therefore the number of collections is 0. This looks odd to the user, so in this case set
      // it to 1, which makes sense, because it displays new collection XXXXX
      if (counts[0] > 0) {
        numberOfCollections = counts[0];
      } else {
        numberOfCollections = 1;
      }
      numberOfFiles = counts[1];

    } catch (Exception e) {
      if (e.getMessage().equals("404")) {
        title = I18NUtils.getTranslatedString("error-col-fetchtitle-error", locale,
            container.getStorageId());
        error = I18NUtils.getTranslatedString("error-col-fetchtitle-missing", locale,
            "<a href='" + Configuration.getInstance().getConfig().getDariahStorageUrl()
                + container.getStorageId() + "'>" + container.getStorageId() + "</a>",
            e.getMessage());
      } else {
        title = I18NUtils.getTranslatedString("error-col-fetchtitle-error", locale,
            container.getStorageId());
        error = I18NUtils.getTranslatedString("error-col-fetchtitle-detail", locale,
            "<a href='" + Configuration.getInstance().getConfig().getDariahStorageUrl()
                + container.getStorageId() + "'>" + container.getStorageId() + "</a>",
            e.getMessage());
      }
    }

    collection = new Collection(container.getUserId(),
        container.getStorageId(),
        title,
        description,
        creator,
        "", // date field not read from RDF
        "", // format not set for collection
        seafileRef,
        error,
        numberOfFiles,
        numberOfCollections);

    return collection;
  }

  /**
   * <p>
   * Fetches the RDF model for a collection and returns it as Jena object for further processing.
   * </p>
   * 
   * @param wrapper
   * @param storageId
   * @param pdpToken
   * @param logID
   * @return
   * @throws IOException
   */
  public static Model getRDFModel(DariahStorageClientWrapper wrapper, String storageId,
      String pdpToken, String logID) throws IOException {

    // Set the retries, we catch any exception here in future we can be more specific or exit early
    // if there is a non fixable state.
    RetryPolicy retryPolicy = new RetryPolicy().withDelay(150, TimeUnit.MILLISECONDS)
        .withMaxRetries(3).retryOn(Exception.class);

    DariahStorageClientWrapper.ReadFile rf = wrapper.new ReadFile(storageId, pdpToken, logID);

    InputStream data = Failsafe.with(retryPolicy).get(() -> rf.call());

    Model model = ModelFactory.createDefaultModel();
    model.read(data, null, "TURTLE");

    return model;
  }

  /**
   * @param client
   * @param pdpToken
   * @return
   */
  public static StatusMessage canWriteinStorage(DariahStorageClient client, String pdpToken) {

    StatusMessage message = new StatusMessage();
    try {
      InputStream data =
          new ByteArrayInputStream("Testwrite".getBytes(StandardCharsets.UTF_8.name()));

      // create and delete the dummy file
      String storageId = client.createFile(data, "text/plain", pdpToken);
      client.deleteFile(storageId, pdpToken);
      message.setSuccess(true);
      message.setMessage("Create and delete of file to storage successful");
    } catch (UnsupportedEncodingException e) {
      // should not happen, the data var is a fixed value
      message.setSuccess(false);
      message.setMessage("Unsupported character encoding. You should never see this message.");
    } catch (IOException e) {
      message.setSuccess(false);
      message.setMessage("Writing to storage failed!");
      message.setMessageDetailKey("misc-error-storage");
      message.setStackTrace(ExceptionUtils.getStackTrace(e));
    }
    return message;
  }

  /**
   * <p>
   * Used for generating IDs for log messages.
   * </p>
   * 
   * @return
   */
  public static String createRandomLogId() {
    Adler32 adler = new Adler32();
    Random rn = new Random();
    adler.update(rn.nextInt());

    return "PUBLI_" + String.valueOf(adler.getValue());
  }

}
