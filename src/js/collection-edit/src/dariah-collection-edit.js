/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import { EventAggregator } from 'aurelia-event-aggregator';

import template from './dariah-collection-edit.html';

import TGForm from './components/tgform';
import DataLoader from './utils/dataloader';
import TGFormDataModelUtils from './utils/tgformDataModelUtils';
import N3 from 'n3';

// font-awesome icons
import 'font-awesome/css/font-awesome.css';

import $ from 'jquery';
//import jQuery from 'jquery';
// export for others scripts to use
//window.$ = $;
//window.jQuery = jQuery;
import jstree from 'jstree';

//import _ from "lodash";

import union from 'lodash/union';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import forEach from 'lodash/forEach';
import filter from 'lodash/filter';
import difference from 'lodash/difference';
import isEmpty from 'lodash/isEmpty';
import Escape from './utils/escape'

const _ = {
  union,
  find,
  findIndex,
  forEach,
  filter,
  difference,
  isEmpty,
}

// Remove Ractive debug messages from releases?
Ractive.DEBUG = false;

export default class DariahCollectionEdit {
	
  constructor(options = {}) {

    this.formdata = [];
    this.partsAsListEnabled = options.partsAsListEnabled || false;
    this.dataModelUtils = new TGFormDataModelUtils({formdata: this.formdata});

    if(options.schemaUrls) {
      this.dataLoader = new DataLoader({
          schemaUrl: options.schemaUrls,
          dataModelUtils: this.dataModelUtils,
          partsAsListEnabled: this.partsAsListEnabled,
      });
    } else {
      console.log('ERROR: Please define at least one element in schemaUrls');
    }

    this.updateDataUrl = options.updateDataUrl;

    // CRUD operations on files
    this.deleteFileUrl = options.deleteFileUrl;
    this.deleteSubcollectionUrl = options.deleteSubcollectionUrl;
    this.addFileUrl = options.addFileUrl;
    this.maxUploadSize = options.maxUploadSize;
    this.updateFileUrl = options.updateFileUrl;
    this.storageUrl = options.storageUrl;
    this.storageToken = options.storageToken;
    this.enableAAI = options.enableAAI;

    this.storageLocation = options.storageLocation;
    this.storageUri = options.storageUri;
    this.storageId = /[^:]*$/.exec(options.storageUri)[0];
    this.subcollectionsEnabled = options.subcollectionsEnabled;
    this.autosaveEnabled = options.autosaveEnabled || false;
    this.saveTimerMap = new Map();
    this.debug = options.debug || false;
    //Ractive.DEBUG = this.debug; // show ractive debug messages
    this.selectedObjectUri = options.selectedObjectUri || false;
    // TOOO Ubbo: Add seafile prefix here if applicable?
    this.selectedObjectUri = "dariahstorage:" + this.selectedObjectUri;

    this.seafileEnabled = options.seafileEnabled || false;
    this.seafileAuthToken = options.seafileAuthToken;
    this.seafileDownloadUrl = options.seafileDownloadUrl;
    this.isSeafileLibrary = options.isSeafileLibrary || false;
    this.mainViewUrl = options.mainViewUrl;
    
    this.publishimgpath = options.publishimgpath;
    this.statusUrl = options.statusUrl;

    this.disableUI = false;

    this.i18nMap = options.i18nMap;

    if(this.seafileEnabled &! _.isEmpty(this.seafileAuthToken)) {
      // this is for the publishgui portlet, where we add &storageId=
      // to path, but we need the seafileToken before, so we cut after last &
      let tmpUrl = /^(.*)&[^&]*$/.exec(options.loadFileUrl)[1];
      this.loadFileUrl = `${tmpUrl}&seafileToken=${this.seafileAuthToken}&storageId=`;
    } else {
      this.loadFileUrl = options.loadFileUrl;
    }

    this.elementId = options.elementId;

    // add a publish subscribe bus
    this.pubsub = new EventAggregator();
    this.pubsub.subscribe('data-changed', (message) => {
      this.rapp.update();
      this.autosave(message.uri);
    });
    

    // let ractive have the i18n templating function globally available
    Ractive.prototype.data = {
      debug: this.debug,
      //tgformOptions: options,
      i18n: function(key) {
        if (this.tgformOptions.i18nMap[key] !== undefined) {
          return this.tgformOptions.i18nMap[key];
        } else {
          console.log("No i18n key/value defined for: " + key);
          return key;
        }
      }
    }
    this.escapeHtml = new Escape();

    Ractive.prototype.tgformOptions = options;
    Ractive.prototype.dataModelUtils = this.dataModelUtils;
    Ractive.prototype.pubsub = this.pubsub;

    this.loadAndSetupViews();
    this.beforeUnload();
  }

  loadAndSetupViews() {
    // first load data
    console.log(`loading data from: ${this.loadFileUrl}${this.storageId}`);
    this.dataLoader.loadData(this.loadFileUrl, this.storageUri, 'dariah:Collection').then((data) => {
      // hide all forms
      data.forEach(val => {
        val.hidden = true;
        this.formdata.push(val);
      });
      // data is prepared, now build the views
      this.setupMetadataView();
      this.setupTreeView();
      $(document).ready( () => {
          $('h4#gathering').remove();
        });
    });
  }
  
  setupMetadataView() {
  	  // here we include dropzone.js as a decorator
	  var dropzoneDecorator = function ( node ) {
		  // have reactive object available inside the function
		  var ractive = this;
		  var oldprogress = 0;
		  var status = {};
		  var fileStatus = [];
		  status.fileStatus = fileStatus;
		  var transactionID = '';
		  var dropzone = new Dropzone(node, 
			{
				// Dropzone configuration
				url: ractive.tgformOptions.storageUrl,
				paramName: "file",
				parallelUploads: 3,
				timeout: 7200000, // 2hours
				uploadMultiple: false,
				maxFilesize: ractive.tgformOptions.maxUploadSize,
				dictDefaultMessage: ractive.tgformOptions.i18nMap['add-file-drop'],
				dictFallbackMessage: ractive.tgformOptions.i18nMap['add-file'],
				dictFileTooBig: ractive.tgformOptions.i18nMap['dictFileTooBig'],
				dictResponseError: ractive.tgformOptions.i18nMap['upload-failed '] + ": {{statusCode}}",
			});
			// Dropzone events
			dropzone.on("success", function(file, response) {
				if (ractive.tgformOptions.debug) {
					console.log('upload done');
					console.log("File: "+ JSON.stringify(file));
					console.log("Response:" + response);
					console.log("success location: " + file.xhr.getResponseHeader("Location"));
				}
				// due to direct upload to cdstar we have no json response and thus no extra metadata any more
				//const jsonResponse = JSON.parse(response);
				const filename = file.upload.filename;
				const filetype = file.type;
				  
				//const fileId = jsonResponse.fileId;
				//const fileId = response;
				const location = file.xhr.getResponseHeader("Location");
				var fileId = location.split("/");
				fileId = "dariahstorage:"+fileId[fileId.length-1];
				// old file ID with direct upload
				//fileId = "dariahstorage:"+fileId.substring(this.options.url.length);
								
				const parentUri = ractive.parent.getSelectedParentCollection();
				
				// null because of the empty json we have
				let fileData = ractive.parent.dataLoader.addFile(fileId, null, filename, filetype, 'dariah:DataObject');  
				// hide the form
				fileData.hidden = true;
				fileData.parentUri = parentUri;
				let id = /[^:]*$/.exec(fileId)[0];
				fileData.format = filetype;
				
				console.log('push to fd');
				ractive.get('formdata').push(fileData);
				// add haspart relation
				ractive.parent.dataModelUtils.addPart(parentUri, fileId);
		        ractive.parent.pubsub.publish('resource-added', {fileId});
		        
		        // our JSON status 
		        var entry = {
		          "status": true,
		          "size": file.size
		        }
		        status.fileStatus.push(entry);
					
				// remove the finished upload from the dropzone area to indicate that the file is finished
				if (file.previewElement) {
				    return file.previewElement.classList.add("dz-success"),
				    $(function(){
				      setTimeout(function(){
				        $('.dz-success').fadeOut('slow');
				        dropzone.removeFile(file);
				      },2500);
				    });
				}

			});

	      dropzone.on("error", function (file, msg, xhr) {
	        var entry = {
	          "status": false,
	          "size": file.size,
	          "message" : msg
	        }
	        status.fileStatus.push(entry);
	      });
      			
			// sets the current progress
			dropzone.on("totaluploadprogress", function(progress) {
				if (progress > oldprogress) {
					document.querySelector("#total-progress .bar").style.width = progress + "%";
					oldprogress = progress;
				}
			});
			
			dropzone.on("processing", function(file) {
				ractive.parent.set('showSpinner', true);
			});
			
			// Show the total progress bar when upload starts
			dropzone.on("sending", function(file, xhr) {
				document.querySelector("#total-progress").style.display = "block";
				ractive.parent.set('showSpinner', true);
				ractive.parent.disableGUI();
	
				// Generate a random ID for this batch
				if (transactionID == '') 
					transactionID = "PK-"+Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 6);
	
				// Add the required headers to the request. We did not use the headers options of the widget
				// because content type is different with each upload
				xhr.setRequestHeader("Authorization", "bearer " + ractive.tgformOptions.storageToken);
				xhr.setRequestHeader("X-Transaction-ID", transactionID);
				if (file.type != null && file.type.length > 0)
					xhr.setRequestHeader("Content-Type", file.type);		
				else
					xhr.setRequestHeader("Content-Type", "application/octet-stream");	
								 
				// this part overwrites the default widget handling which wraps the data into a form element
				// we need to post raw data
				var _send = xhr.send;
				xhr.send = function() {
				   _send.call(xhr, file);
				};       

			});			
			
			// Hide the total progress bar when upload is done and reset to 0%
			dropzone.on("queuecomplete", function(progress) {
				document.querySelector("#total-progress").style.display = "none";
				document.querySelector("#total-progress .bar").style.width = "0%";
				ractive.parent.set('showSpinner', false);
		        oldprogress = 0;
		        ractive.parent.enableGUI();
		
		        // After all upload has been done, send the number of files + file size 
		        // + error messages if any to the publikator 
		        status.transactionID = transactionID;
		        const jsondata = JSON.stringify(status);
		        console.log("Transaction ID: " + transactionID);
		        const data = new FormData();
		        data.append('status', new Blob([jsondata], {
		          type: 'text/turtle',
		        }));
		        console.log("status url: " +ractive.tgformOptions.statusUrl);
		        $.ajax({
		          data,
		          type: 'POST',
		          url: ractive.tgformOptions.statusUrl,
		          processData: false,
		          contentType: false,
		        });        
        
			});		

	      document.querySelector("#cancelUploadBtn").onclick = function() {
	        dropzone.removeAllFiles(true);
	 
	      };
			
			return {
			 teardown: function () {
			      // cleanup code
			    }
			 }
	}	  
	  
    this.rapp = new Ractive({
      el: this.elementId,
      template: template,
      components: {
        tgForm: TGForm,
      },
      decorators: {
    	  	dropzone: dropzoneDecorator,
    	  },
      // http://docs.ractivejs.org/latest/magic-mode
      // unfortunately remove fields not working correct with magic mode
      // remove fields also not working correct with ractive 0.7.3
      //magic: true,

      // some functions we want to have on "this" in ractive object
      saveData: this.saveData,
      saveChangedParentCollection: this.saveChangedParentCollection,
      createSubcollection: this.createSubcollection,
      addFile: this.addFile,
      getSelectedParentCollection: this.getSelectedParentCollection,
      selectedObjectUri: this.selectedObjectUri,
      escapeHtml: this.escapeHtml,
      entityMap: this.entityMap,

      // objects
      dataLoader: this.dataLoader,

      data: {
        formdata: this.formdata,
        isSeafileLibrary: this.isSeafileLibrary,
        subcollectionsEnabled: this.subcollectionsEnabled,
        showNonMandatoryFields: false,
        // we trick ractive into recomputing the collectionTitle by changing this property
        collectionTitleChangeTrigger: 0,
        backUrl : this.mainViewUrl + "storageId=" + this.storageUri.split(':')[1],
        publishimgpath : this.publishimgpath,
        disableUI: this.disableUI,
      },

      computed: {
        collectionTitle: function() {
          // because the collectionTitleChangeTrigger property is accessed, recompute is done with every change
          this.get('collectionTitleChangeTrigger');
          return this.escapeHtml.escape(this.dataModelUtils.findEntryForUri(this.tgformOptions.storageUri).formModel.schema[0].fieldvalue);
        }
      },

      disableGUI: function() {
        if (this.get('disableUI') == false) {
          this.set('disableUI', true);
        }
      },
    
      enableGUI: function() {
        this.set('disableUI', false);        
      },   

      onrender: function() {
        this.on('log', () => {
          console.log(this.get('formdata'));
          console.log('selected parent: ' + this.getSelectedParentCollection());
        });

        this.on('addSubcollection', (evt) => {
          this.createSubcollection();
        });

        this.on('addFile', (evt) => {
          //this.set('showSpinner', true);
          this.addFile(evt);
        });

      }
    });

  }
  
  setupTreeView() {
    let treeData = this.dataLoader.getJSTreeData(this.storageUri);

    /*
     * which treeitem to select on load
     */
    let selectedIndex;
    if(this.selectedObjectUri) {
      selectedIndex = _.findIndex(treeData, { id: this.selectedObjectUri} );
      // uri not found?
      if(selectedIndex < 0) {
        selectedIndex = 0;
        this.showForm(this.storageUri);
      } else {
        this.showForm(this.selectedObjectUri);
      }
    } else {
      // select the top entry in treeview
      selectedIndex = 0;
      this.showForm(this.storageUri);
    }
    treeData[selectedIndex].state = { opened: true, selected: true };

    /*
     * setup the treeview
     */
    this.$treeView = $('#jstree');

    this.$treeView.jstree({
      //plugins: ['wholerow'],
    	plugins: [ 'dnd', 'type' ],
      core: {
        data: treeData,
        check_callback: function (operation, node, node_parent, node_position, more) {
          // only dropping items onto collections allowed
          if(operation === 'move_node') {
            if(node_parent.original.type !== 'collection') {
              return false;
            }
          }
          return true;
        },
        dnd: {
          check_while_dragging: true
        },
        //themes: { stripes: true, variant: 'small', dots: false },
      },
    });

    // track treeview selection and show/hide forms
    this.$treeView.on('changed.jstree', (e, content) => {
      if (content.node !== undefined) {
        this.showForm(content.node.id);
      }
    });
 
    // track treeview selection and show/hide forms
    this.$treeView.on('move_node.jstree', (e, content) => {
      this.dataModelUtils.movePart(content.node.id, content.old_parent, content.old_position, content.parent, content.position);
      this.pubsub.publish('data-changed', {uri: content.old_parent});
      this.pubsub.publish('data-changed', {uri: content.parent});
    });

    // add $treeView to main ractive object
    this.rapp.$treeView = this.$treeView;

    // track title changes
    this.rapp.observe( 'formdata.*.formModel.schema.*.fieldvalue', function ( newValue, oldValue, keypath ) {
        // prevent acting on form initialization
        if(oldValue === undefined || newValue === undefined) {
          return;
        }
        let arrayId = keypath.split('.')[1];
        let uri = this.get(`formdata.${arrayId}.uri`);

        // we assume that keypath is like this:
        // formdata.3.formModel.schema.0.fieldvalue
        // and that the main title will always be in ...schema.0
        if(parseInt(keypath.split('.')[4]) === 0) {
          this.$treeView.jstree('rename_node', uri , this.escapeHtml.escape(newValue));
          if(uri === this.tgformOptions.storageUri) {
            // the changed title is the collection title, trigger recompute of computed property
            this.add('collectionTitleChangeTrigger');
          }
        }
        this.pubsub.publish('data-changed', {uri});
    });

    this.pubsub.subscribe('resource-added', (message) => {
      // todo: we could send addedUri and addedForm via pubsub
      let addedUri = message.fileId;
      let addedForm = _.find(this.formdata, { uri: `${addedUri}` });

      console.log(addedForm);

      let icon;
      let type;
      if(addedForm.rdftype === "dariah:Collection") {
        icon = 'fa fa-folder-open-o';
        type = 'collection';
      } else {
        icon = 'fa ' + this.dataLoader.getIconForMime(addedForm.format);
      }

      this.$treeView.jstree().create_node(
        addedForm.parentUri,
        {
          icon,
          type,
          id: addedUri,
          text: this.escapeHtml.escape(addedForm.formModel.schema[0].fieldvalue),
        },
        'last'
      );

      if(message.collectionCreated) {
        // the parent needs to be saved too, as it contains the link to the new collection
        this.saveChangedParentCollection(addedForm.parentUri);
      }
      this.saveChangedParentCollection(addedUri);

      this.rapp.update();
    });

    this.pubsub.subscribe('resource-removed', (message) => {
      const removedUri = message.uri;
      const parentUri = message.parentUri;
      console.log('item was removed: ' + removedUri + ' with parent: ' +  parentUri);

      this.$treeView.jstree('delete_node', removedUri);
      this.$treeView.jstree('select_node', parentUri);

      this.saveChangedParentCollection(parentUri);

      this.rapp.update();
    });
  }

  showForm(uri) {
    let toHide = _.find(this.formdata, { 'hidden': false });
    let toShow = _.find(this.formdata, { 'uri': uri });

    if(! _.isEmpty(toHide)) {
      toHide.hidden = true;
    }
    if(! _.isEmpty(toShow)) {
      toShow.hidden = false;
    }
    this.rapp.update();
  }

  /**
   * called on formdata change
   * @param {*} uri 
   */
  autosave(uri) {
    this.saveChangedParentCollection(uri);
  }

  /**
  * save metadata, which is the uri itself in case of a collection, 
  * but the parentId in case of a file
  * this method tries to be smart and find out, based on the rdftype of the uri
  */
  saveChangedParentCollection(uri) {
    //console.log('saveChangedParentCollection for ' + uri );
    const changedEntry = this.dataModelUtils.findEntryForUri(uri);

    // save the collection which contains the changed entry
    let uriToSave;
    if(changedEntry.rdftype === 'dariah:Collection') {
      uriToSave = changedEntry.uri;
    } else {
      uriToSave = changedEntry.parentUri;
    }

    let data = [];
    // get all members of the collection to save
    let colToSave = _.filter(this.formdata, function(o) { return (o.uri === uriToSave || o.parentUri === uriToSave); });
    colToSave.forEach(val => {
      // we only want full metadata for the collection itself, but not for its subcollections
      if(val.uri === uriToSave || val.rdftype !== 'dariah:Collection') {
        data.push(val.formModel.schema);
      } else {
        // for a subcollection the reference and its type is sufficient
        data.push([{
          subject: val.uri,
          rdf_Property: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
          fieldvalue: 'dariah:Collection',
          isResource: true,
        }]);
        // also add sflib path to collection data
        const seafileLibraryPath = this.dataLoader.getFirstLiteral(val.uri, 'dariah:describesSeafileLibraryPath');
        if(!_.isEmpty(seafileLibraryPath)) {
          data.push([{
            subject: val.uri,
            rdf_Property: 'dariah:describesSeafileLibraryPath',
            fieldvalue: seafileLibraryPath,
          }]);
        }
      }
    });
    const turtle = this.dataLoader.writeTurtle(data, uriToSave);
    if(this.autosaveEnabled) {
      this.saveData(this.updateDataUrl, uriToSave, turtle);
      //console.log('saved: ' + uriToSave);
    } else {
      console.log('autosave off, logging to console instead');
      console.log(turtle);
    }
  }

  /**
  * this method handles saving metadata
  * it also sets manages the savetimer(s)
  * apart from webworkers js is singlethreaded, no need to worry ;-)
  */
  saveData(updateDataUrl, storageUri, ttlData) {
     // make sure that not more than one save per second occur
    //console.log('(re)setting savetimer for ' + storageUri);
    // Disable back button even if the timer is not running yet
    // this avoids that the user leaves the page before saving has been performed.
    this.rapp.set('autoSaveStatus', this.i18nMap.saving);

    let saveTimer = this.saveTimerMap.get(storageUri);
    clearTimeout(saveTimer);
    saveTimer = setTimeout(() => {
      this.rapp.set('showSpinner', true);
      this.saveDataWithoutTimer(updateDataUrl, storageUri, ttlData);
    }, 2000);
    this.saveTimerMap.set(storageUri, saveTimer);
  }

  saveDataWithoutTimer(updateDataUrl, storageUri, ttlData) {
    console.log('saving:' + storageUri);
    const data = new FormData();

    data.append('storageId', storageUri);
    data.append('file', new Blob([ttlData], {
      type: 'text/turtle',
    }));

    $.ajax({
      data,
      type: 'POST',
      url: updateDataUrl,
      processData: false,
      contentType: false,
    }).done(() => {
      this.rapp.set('autoSaveStatus', this.i18nMap['last-autosave']);
      this.rapp.set('showSpinner', false);
    });
  }
  
  /**
  * adds subcollection
  */
  createSubcollection() {
    const data = new FormData();
    const parentUri = this.getSelectedParentCollection();
    // TODO: prevent addFile from running tika for analysis
    // data.append('isSubCol', true);
    data.append('action', 'addFile');

    const rdfString = "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n";
    const blob = new Blob([rdfString], { type: 'text/plain; charset=utf-8' });

    data.append('file', blob);

    $.ajax({
      data,
      type: 'POST',
      url: this.tgformOptions.addFileUrl,
      processData: false,
      contentType: false,
    }).done((response) => {
      console.log(response);
      const jsonResponse = JSON.parse(response);
      const fileId = jsonResponse.fileId;
      console.log(fileId);

      let fileData = this.dataLoader.addFile(fileId, '', '', '', 'dariah:Collection');
      fileData.hidden = true;
      fileData.parentUri = parentUri;
      let id = /[^:]*$/.exec(fileId)[0];

      this.get('formdata').push(fileData);
      // add haspart relation
      this.dataModelUtils.addPart(parentUri, fileId);
      this.pubsub.publish('resource-added', {fileId, collectionCreated: true});
    });
  }

  /**
  *
  */
  getSelectedParentCollection() {
    // TODO: prevent multiselect
    const uri = this.$treeView.jstree('get_selected')[0];
    const entry = this.dataModelUtils.findEntryForUri(uri);
    if(entry.rdftype === 'dariah:Collection') {
      return uri;
    } else {
      return entry.parentUri;
    }
  }

  beforeUnload() {
    var r = this;
    $(window).on('beforeunload', function() {
        // check if something is still running and alert the user about unsaved changes
        if (r.rapp.get('disableUI') == true || r.rapp.get('autoSaveStatus') === r.i18nMap['saving'] ) {
          //return this.i18nMap['edit-unsaved-changes'];
          return r.i18nMap['edit-unsaved-changes'];
        }else{
          // return without value does not show a message
          return;
        }
    });
  }

}

