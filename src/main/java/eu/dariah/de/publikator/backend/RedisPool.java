package eu.dariah.de.publikator.backend;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
public class RedisPool {

	private static RedisPool instance;
	
	private static JedisPool pool;
	
	private RedisPool() {}
	
	public static final RedisPool getInstance() {
		if (instance == null) {
			RedisPool.instance = new RedisPool();
		}
		return RedisPool.instance;
	}
	
	public void connect() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        
        // for a list of options see
        //https://commons.apache.org/proper/commons-dbcp/configuration.html
                
        // The maximum number of active connections that can be allocated from this pool at the same time, or negative for no limit.
        poolConfig.setMaxTotal(Configuration.getInstance().getConfig().getRedisMaxParallel());
        // Tests whether connection is dead when connection
        // retrieval method is called, should be checked, else
        // the pool might run out of connections
        poolConfig.setTestOnBorrow(true);
        // Tests whether if connection is dead when returning to the pool
        poolConfig.setTestOnReturn(true);
        // Number of connections to Redis idling around
        poolConfig.setMaxIdle(5);
        // Minimum number of idle connections to Redis (always idling)
        poolConfig.setMinIdle(1);
        // Tests if connections are dead during idle periods
        poolConfig.setTestWhileIdle(true);
        // Maximum number of connections to test in each idle check
        poolConfig.setNumTestsPerEvictionRun(10);
        // Idle connection checking period
        poolConfig.setTimeBetweenEvictionRunsMillis(60000);
        // Create the jedisPool
        pool = new JedisPool(poolConfig, 
        		Configuration.getInstance().getConfig().getRedisHostname(), 
        		Configuration.getInstance().getConfig().getRedisPort());		
	}
	
	public void disconnect() {
		//pool.close();
		pool.destroy();		
	}
	
	public Jedis getJedis() {
		return pool.getResource();
	}
		
}
