/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './dariah-main-view.html';
import $ from 'jquery';

import InnerContainer from './components/inner-container';

export default class DariahMainView {

  constructor(options = {}) {

    this.debug = options.debug || false;
    this.elementId = options.elementId;
    this.listCollectionsUrl = options.listCollectionsUrl;

    Ractive.prototype.mainViewOptions = options;

    Ractive.prototype.data = {
      editviewURL: options.editviewURL,
      pidServiceUrl: options.pidServiceUrl,
      collectionRegistryUrl: options.collectionRegistryUrl,
      genericSearchUrl: options.genericSearchUrl,
      dhcrudUrl: options.dhcrudUrl,
      mainviewUrl: options.mainviewUrl,
      debug: options.debug,
      refresh: options.refresh,
      serviceTimeout: options.serviceTimeout,
      selectedCollectionUri: options.selectedCollectionUri,
      getPublishInfoUrl: options.getPublishInfoUrl,
	  deleteCollectionUrl: options.deleteCollectionUrl,
	  publishCollectionUrl: options.publishCollectionUrl,
	  publishVersionUrl: options.publishVersionUrl,
	  getPublishStatusUrl: options.getPublishStatusUrl,
	  publishimgpath : options.publishimgpath,
      i18n: function(key) {
        if (this.mainViewOptions.i18nMap[key] !== undefined) {
          return this.mainViewOptions.i18nMap[key];
        } else {
          console.log("No i18n key/value defined for: " + key);
          return key;
        }
      }
    }

    // Set default timeout for service calls (not recommended, we use it anyhow, because it does work!).
	$.ajaxSetup({ timeout: options.serviceTimeout });

    this.setupMainView();
  }

  setupMainView() {
    $.getJSON(this.listCollectionsUrl).done((res) => {
      const indata = {cols: res};
      const collectioncontainers = new Ractive({
        el: this.elementId,
        template: template,
        components: {
          'inner-container' : InnerContainer,
        },
        data: indata,
      });

      $(document).ready( () => {
        $('h4#gathering').remove();
      });

    }).fail((res) => {
      console.log("++ error fetching collection info ++");
    });

    // Set publishInProgress to false, is set to true after reallyPublishAlert button! Just used to let vanish the publish success button!
	var publishInProgress = false;
	// indicates if a request publish status is running
	var publishStatusRunning = false;

  }

}
