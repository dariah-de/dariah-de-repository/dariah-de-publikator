package eu.dariah.de.publikator.beans;

import java.io.Serializable;
import java.util.List;

public final class User implements Serializable {
	
	private static final long serialVersionUID = -2730315670867352562L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((affilation == null) ? 0 : affilation.hashCode());
		result = prime * result + ((displayname == null) ? 0 : displayname.hashCode());
		result = prime * result + ((eppn == null) ? 0 : eppn.hashCode());
		result = prime * result + ((ismemberof == null) ? 0 : ismemberof.hashCode());
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result + ((organisation == null) ? 0 : organisation.hashCode());
		result = prime * result + ((preferredlanguage == null) ? 0 : preferredlanguage.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "User [eppn=" + eppn + ", affilation=" + affilation + ", ismemberof=" + ismemberof + ", displayname="
				+ displayname + ", mail=" + mail + ", organisation=" + organisation + ", preferredlanguage="
				+ preferredlanguage + "]";
	}

	private final String eppn;
	private final String affilation;
	private final List<String> ismemberof;
	private final String displayname;
	private final String mail;
	private final String organisation;
	private final String preferredlanguage;
	
	
	public User(String eppn, String affilation, List<String> ismemberof, String displayname, String mail,
			String organisation, String preferredlanguage) {
		super();
		this.eppn = eppn;
		this.affilation = affilation;
		this.ismemberof = ismemberof;
		this.displayname = displayname;
		this.mail = mail;
		this.organisation = organisation;
		this.preferredlanguage = preferredlanguage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEppn() {
		return eppn;
	}

	public String getAffilation() {
		return affilation;
	}

	public List<String> getIsmemberof() {
		return ismemberof;
	}

	public String getDisplayname() {
		return displayname;
	}

	public String getMail() {
		return mail;
	}

	public String getOrganisation() {
		return organisation;
	}

	public String getPreferredlanguage() {
		return preferredlanguage;
	}


}
