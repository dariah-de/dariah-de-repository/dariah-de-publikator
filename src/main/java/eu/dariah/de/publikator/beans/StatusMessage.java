package eu.dariah.de.publikator.beans;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 * 
 * This class is used as a bean for status messages of the methods 
 * which is later rendered on the GUI.
 */
public class StatusMessage {
	/**
	 * if the operation has been successful
	 */
	private boolean success;
	/**
	 * Untranslated status message or additional details (e.g. field name)
	 */
	private String message;
	/**
	 * i18n key of the message headline (e.g. storage.failure)
	 */
	private String messageHeadlineKey;
	/**
	 * i18n key of the message text (eg. storage.failure.description
	 */
	private String messageDetailKey;
	private String stackTrace;
	public boolean isSuccess() {
		return success;
	}
	public String getMessage() {
		return message;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getStackTrace() {
		return stackTrace;
	}
	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}
	
	public String getMessageHeadlineKey() {
		return messageHeadlineKey;
	}
	public String getMessageDetailKey() {
		return messageDetailKey;
	}
	public void setMessageHeadlineKey(String messageHeadlineKey) {
		this.messageHeadlineKey = messageHeadlineKey;
	}
	public void setMessageDetailKey(String messageDetailKey) {
		this.messageDetailKey = messageDetailKey;
	}

	public StatusMessage(boolean success, String message, String messageHeadlineKey, String messageDetailKey,
			String stackTrace) {
		super();
		this.success = success;
		this.message = message;
		this.messageHeadlineKey = messageHeadlineKey;
		this.messageDetailKey = messageDetailKey;
		this.stackTrace = stackTrace;
	}
	public StatusMessage() {
		super();
	}
		
}
