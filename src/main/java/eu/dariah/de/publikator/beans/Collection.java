package eu.dariah.de.publikator.beans;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 *
 */
public final class Collection {

	private final String userId;
	private final String storageId;
	private final String title;
	private final String description;
	private final String creator;
	private final String date;
	private final String format;
	private final String describesSeafileLibrary;
	private final String error;
	private final int numberOfFiles;
	private final int numberOfCollections;

	/**
	 * @param collection
	 */
	
	
	public Collection (String userId, String storageId) {
		this.userId = userId;
		this.storageId = storageId;
		this.title = "n/a";
		this.description = "n/a";
		this.creator = "n/a";
		this.date = "n/a";
		this.format = "n/a";
		this.describesSeafileLibrary = "n/a";
		this.error = "n/a";
		this.numberOfFiles = -1;
		this.numberOfCollections = -1;		
	}
	

	public Collection(String userId, String storageId, String title, String description, String creator, String date,
			String format, String describesSeafileLibrary, String error, int numberOfFiles, int numberOfCollections) {
		super();
		this.userId = userId;
		this.storageId = storageId;
		this.title = title;
		this.description = description;
		this.creator = creator;
		this.date = date;
		this.format = format;
		this.describesSeafileLibrary = describesSeafileLibrary;
		this.error = error;
		this.numberOfFiles = numberOfFiles;
		this.numberOfCollections = numberOfCollections;
	}



	public String getUserId() {
		return userId;
	}

	public String getStorageId() {
		return storageId;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getCreator() {
		return creator;
	}

	public String getDate() {
		return date;
	}

	public String getFormat() {
		return format;
	}

	public String getDescribesSeafileLibrary() {
		return describesSeafileLibrary;
	}

	public String getError() {
		return error;
	}

	public int getNumberOfFiles() {
		return numberOfFiles;
	}

	public int getNumberOfCollections() {
		return numberOfCollections;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((describesSeafileLibrary == null) ? 0 : describesSeafileLibrary.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + numberOfCollections;
		result = prime * result + numberOfFiles;
		result = prime * result + ((storageId == null) ? 0 : storageId.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}



}
