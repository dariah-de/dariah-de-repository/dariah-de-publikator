package eu.dariah.de.publikator.dao;

import java.util.Locale;

import eu.dariah.de.publikator.backend.RedisPool;
import redis.clients.jedis.Jedis;

public class RedisLanguageDAO implements LanguageDAO {

	@Override
	public Locale getUserlanguage(String userId) {
		Locale locale = null;
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
		
			String loc = jedis.get("lang#"+userId);
				
			if (loc != null) {
				// language with country
				if (loc.length() > 2) {
					String[] res = loc.split("_");
					locale = new Locale(res[0], res[1]);
				}else {
					// language only
					locale = new Locale(loc);
				}
			}
		}
		
		return locale;
	}

	@Override
	public boolean setUserlanguage(String userId, Locale locale) {
		boolean status = false;
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
			String entry = locale.getLanguage();
			if (locale.getCountry() != null)
				if  (locale.getCountry().length() > 0)
					entry = entry + "_"+locale.getCountry();
			
			String ret = jedis.set("lang#"+userId, entry);
			if (ret.equals("OK"))
				status = true;
		
		}
		
		return status;
	}

}
