package eu.dariah.de.publikator.dao;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
public abstract class DAOFactory {

	// List of available DAO Factory, currently we have only REDIS support
	public static final int REDIS = 1;
	
	// add each interface to the available DAOs here
	public abstract CollectionDAO getCollectionDAO();
	
	public abstract LanguageDAO getLanguageDAO();
	
	public static DAOFactory getDAOFactory(int factory) {
		switch (factory) {
			case REDIS:
				return new RedisDAOFactory();
		default:
				return null;
		}
	}
	
}
