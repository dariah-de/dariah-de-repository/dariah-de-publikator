package eu.dariah.de.publikator.backend;

import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;

import org.apache.commons.beanutils.BeanUtils;

import eu.dariah.de.publikator.beans.Config;

/**
 * Utility to retrieve env entries from the web.xml.
 *  Add new configuration entry:
 * 1. add a new env entry to web.xml
 * 2. add the field with EXACT the same name to beans/Config.java
 *    generate the proper getter and setter methods
 *    and edit the toString method to output the new field
 *    for logging
 * 3. add the field also to publikator's publikator.pp and
 *    context.xml.erb in puppet module dhrep to configure via yaml
 *    file.
 *  Done.
 *
 * @author Johannes
 *
 */
public class Configuration {

	private static Configuration instance;

	private Config config = new Config();

	private static final Logger	LOG	= Logger.getLogger("eu.dariah.de.publicator.backend.Configuration");

	private Configuration() {}

	public static final Configuration getInstance () {
		if (Configuration.instance == null) {
			Configuration.instance = new Configuration();
			Configuration.instance.loadConfiguration();
		}
		return Configuration.instance;
	}

	public Config getConfig() {
		return config;
	}

	private void loadConfiguration() {

		try {
			InitialContext ctx = new InitialContext();
			NamingEnumeration<NameClassPair> list = ctx.list("java:comp/env");
			Context env = (Context) ctx.lookup("java:comp/env");
			while (list.hasMore()) {
				String field = list.next().getName();
			    // now set the value with the beanutils to the conf bean
				BeanUtils.setProperty(config, field, env.lookup(field));
			}
		}catch (Exception e) {
			LOG.severe("Could not retrieve config values :"+e.getMessage());
			e.printStackTrace();
		}

		LOG.info("using the following settings:");
		LOG.info(config.toString());

	}

}
