package eu.dariah.de.publikator.backend;

import java.util.concurrent.Callable;
import javax.ws.rs.core.Response;
import eu.dariah.de.publikator.servicebeans.ClientsServiceBean;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * - #%L DARIAHDE :: Publikator Standalone
 * 
 * %% Copyright (C) 2019 SUB Göttingen (https://www.sub.uni-goettingen.de) %%
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 * 
 *         This class is a wrapper around the PublishClient class. Basically each method call is
 *         translated into an inner class which implements the callable interface. This allows a
 *         retry mechanism with https://github.com/jhalterman/failsafe
 */
public class PublishClientWrapper {

  protected ClientsServiceBean clients = new ClientsServiceBean();

  /**
   *
   */
  public class Publish implements Callable<Response> {

    private String uri;
    private String storageToken;
    private String seafileToken;
    private String logID;

    /**
     * @param uri
     * @param storageToken
     * @param seafileToken
     * @param logID
     */
    public Publish(String uri, String storageToken, String seafileToken, String logID) {
      super();
      this.uri = uri;
      this.storageToken = storageToken;
      this.seafileToken = seafileToken;
      this.logID = logID;
    }

    @Override
    public Response call() throws Exception {
      return PublishClientWrapper.this.clients.getPublishClient().publish(this.uri,
          this.storageToken, this.seafileToken, this.logID);
    }

  }

  /**
   *
   */
  public class PublishSimulate implements Callable<Response> {

    private String uri;
    private String storageToken;
    private String seafileToken;
    private String logID;

    /**
     * @param uri
     * @param storageToken
     * @param seafileToken
     * @param logID
     */
    public PublishSimulate(String uri, String storageToken, String seafileToken, String logID) {
      super();
      this.uri = uri;
      this.storageToken = storageToken;
      this.seafileToken = seafileToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Response call() throws Exception {
      return PublishClientWrapper.this.clients.getPublishClient().publishSimulate(this.uri,
          this.storageToken, this.seafileToken, this.logID);
    }
  }

  /**
   *
   */
  public class GetStatus implements Callable<PublishResponse> {

    private String uri;
    private String storageToken;
    private String logID;

    /**
     * @param uri
     * @param storageToken
     * @param logID
     */
    public GetStatus(String uri, String storageToken, String logID) {
      super();
      this.uri = uri;
      this.storageToken = storageToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public PublishResponse call() throws Exception {
      return PublishClientWrapper.this.clients.getPublishClient().getStatus(this.uri,
          this.storageToken, this.logID);
    }

  }

  /**
   *
   */
  public class GetMiniStatus implements Callable<PublishResponse> {

    private String uri;
    private String storageToken;
    private String logID;

    /**
     * @param uri
     * @param storageToken
     * @param logID
     */
    public GetMiniStatus(String uri, String storageToken, String logID) {
      super();
      this.uri = uri;
      this.storageToken = storageToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public PublishResponse call() throws Exception {
      return PublishClientWrapper.this.clients.getPublishClient().getMinistatus(this.uri,
          this.storageToken, this.logID);
    }

  }

  /**
   *
   */
  public class GetInfo implements Callable<InfoResponse> {

    private String uri;
    private String storageToken;
    private String logID;

    /**
     * @param uri
     * @param storageToken
     * @param logID
     */
    public GetInfo(String uri, String storageToken, String logID) {
      super();
      this.uri = uri;
      this.storageToken = storageToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public InfoResponse call() throws Exception {
      return PublishClientWrapper.this.clients.getPublishClient().getInfo(this.uri,
          this.storageToken, this.logID);
    }

  }

}
