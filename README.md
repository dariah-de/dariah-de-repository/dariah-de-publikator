# The DARIAH-DE Publikator

## Running

The Publikator uses Maven. The generation of the Javascript files is not enabled by default.
Eclipse recompiles the Javascript files on every file change, no matter of the file type
and this renders Eclipse unusable.

So you have to specify

```
mvn -P js-notminified
```
for a not minified build
or
```
mvn -P js-minified
```
for a minified Javascript build.

If you want to enable one of these profiles by default, open the pom.xml file
and set `<activeByDefault>true</activeByDefault>`

## Running locally for development

Copy [context.xml.tmpl](./context.xml.tmpl) to context.xml, replace the localToken.

You can get a token from the [trep publikator](https://trep.de.dariah.eu/publikator/).
Sign in and click on "show storage token". The token is valid for ~24h.

Build with `mvn clean package`

Run with:
```
docker compose up
```

and access the publikator at http://localhost:8080/publikator/

default user/password is publikator/standalone (and can be overridden in context.xml).

The [context.xml.tmpl](./context.xml.tmpl) is prepared for hot deployment. If you just want to
quickly test code or jsp changes running `mvn war:exploded` is sufficient. In case of memory leaks
you need to restart the whole tomcat docker container every now and then.

## Changing configuration
You can see all configuration options in the web.xml.

## Adding views
**TODO**

## Changing the Javascript
**TODO**

## Used Libraries

Apache commons
Apache beanutils
Apache Text
Apache Jena
Jedis
kolibri dhpublish client
crud-common
cxf-rt-js-client
Jackson JAXRS
JIWT
Apache Tika
Failsafe
Java statsd client
Apache httpclient
Ractive.js
jQuery
Bootstrap 2
Dropzone.js

## Releasing a new version

For releasing a new version of the DARIAH-DE Publikator, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).
