package eu.dariah.de.publikator.backend;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Properties;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.commons.beanutils.PropertyUtils;
import eu.dariah.de.publikator.beans.Config;
import eu.dariah.de.publikator.utils.I18NUtils;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
@WebListener
public class MiscStartup implements ServletContextListener {
	
	private static final Logger	LOG	= Logger.getLogger(MiscStartup.class.getName());

    /**
     * Default constructor. 
     */
    public MiscStartup() {
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
        // Load the app.properties file to get the version
    		// this only works if it has been build with maven
        try {
	        	ClassLoader loader = Thread.currentThread().getContextClassLoader();
	        	InputStream is = loader.getResourceAsStream("app.properties");
        	
	        Properties pom = new Properties();
	         if (is != null) {
	        	 	pom.load(is);
	        	 	String versionName = pom.getProperty("versionName");
	        	 	String buildTimestamp = pom.getProperty("buildTimestamp");
	        	 	String buildBranch = pom.getProperty("branch");
	        	 	String buildCommit = pom.getProperty("commit");	        	 	
	        	 	LOG.info("Starting Publikator version " + pom.getProperty("versionName") 
	        	 	+ " build on " + buildTimestamp + " branch: " + buildBranch + " commit: " + buildCommit);
	        	 	sce.getServletContext().setAttribute("versionName", versionName);
	        	 	sce.getServletContext().setAttribute("buildTimestamp", buildTimestamp);
	        	 	sce.getServletContext().setAttribute("buildBranch", buildBranch);
	        	 	sce.getServletContext().setAttribute("buildCommit", buildCommit);	        	 	
	         }else {
	 			LOG.info("Starting publikator version unkown");
	         }
		} catch (IOException e) {
			// Does not matter if we can't load the version
			LOG.info("Starting publikator version unkown");
		}
        
        // now we set the config values into the servlet context for easy access on the servlets/jsps
        Config conf = Configuration.getInstance().getConfig();
        
        if (conf != null) {
        
	        Field[] attributes = conf.getClass().getDeclaredFields();
	        
	        for (Field field : attributes) {
	        		try {
					sce.getServletContext().setAttribute(field.getName(), PropertyUtils.getProperty(conf, field.getName()));
				} catch (Exception e) {
					// ignore, for some reason one field could not be accessed 
				} 
	        }
        }else {
        		LOG.severe("Ups, something went wrong. Configuration values are all NULL!");
        }
        
        // set the available languages of this application for later use 
        sce.getServletContext().setAttribute("availableLocales", I18NUtils.getAvailableLocalesInApp());
        // set the available languages of the dcelement files for later use
        try {
			File dcpath = new File(sce.getServletContext().getRealPath("/schema"));
			sce.getServletContext().setAttribute("availableDCLocales", I18NUtils.getAvailableDCElementsLocalesInApp(dcpath));
		} catch (Exception e) {
			LOG.severe("could not get path to dcelements*.ttl " + e.getMessage());
		}
   	
    }
	
}
