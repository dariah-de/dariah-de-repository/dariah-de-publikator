package eu.dariah.de.publikator.servicebeans;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.WebTarget;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;
import eu.dariah.de.publikator.seafile.SeafileFile;
import eu.dariah.de.publikator.seafile.SeafileLibrary;
import eu.dariah.de.publikator.utils.RDFHelper;
import eu.dariah.de.publikator.utils.SeafileUtils;
import eu.dariah.de.publikator.utils.SeafileUtils.SeafileIdEntry;
import info.textgrid.middleware.common.DariahStorageClient;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

/**
 *
 */
public class SeafileClient {

  public static final String SEAFILE_URL = "https://sftest.de.dariah.eu/files/";
  private static final String DC_PREFIX = "dc";
  private static final String DCTERMS_PREFIX = "dcterms";
  private static final String DARIAH_PREFIX = "dariah";
  public static final String SEAFILE_PREFIX = "seafile";
  private static final String DARIAHSTORAGE_PREFIX = "dariahstorage";
  private static final String RDF_PREFIX = "rdf";
  private static final String RDF_SCHEMA_URL = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

  private String seafileTokenSecret;
  private WebTarget seafileTarget;
  private String token;
  private DariahStorageClient storageClient;

  private static final Logger LOG =
      Logger.getLogger("eu.dariah.de.publicator.servicebeans.SeafileClient");


  /**
   * @param seafileTarget
   * @param seafileTokenSecret
   * @param storageClient
   */
  public SeafileClient(WebTarget seafileTarget, String seafileTokenSecret,
      DariahStorageClient storageClient) {
    this.seafileTarget = seafileTarget;
    this.seafileTokenSecret = seafileTokenSecret;
    this.storageClient = storageClient;
  }

  /**
   * @param SeafileLib2StorageMapping
   * @return
   */
  public SeafileLibrary[] getLibraries(HashMap<String, String> SeafileLib2StorageMapping) {
    LOG.info("getLibs for target: " + seafileTarget.getUri());

    SeafileLibrary[] libs =
        seafileTarget.path("/api2/repos/").request().header("Authorization", "Token " + token)
            .get().readEntity(SeafileLibrary[].class);

    for (SeafileLibrary lib : libs) {
      lib.setFiles(getFiles(lib.getId(), "/"));
      lib.setDariahStorageId(SeafileLib2StorageMapping.get(SEAFILE_URL + lib.getId()));
    }

    return libs;
  }

  /**
   * @param libId
   * @return
   */
  public SeafileLibrary getLibraryInfo(String libId) {
    SeafileLibrary lib = seafileTarget.path("/api2/repos/" + libId + "/").request()
        .header("Authorization", "Token " + token).get().readEntity(SeafileLibrary.class);

    return lib;
  }

  /**
   * list all contents of lib & path (dirs and files)
   * 
   * @param libId
   * @param path
   * @return
   */
  public SeafileFile[] getFiles(String libId, String path) {
    if (libId.equals("")) {
      LOG.info("getfiles: NOFILES");
      return null;
    }
    LOG.info("getFiles for " + libId);
    return seafileTarget.path("/api2/repos/" + libId + "/dir/").queryParam("p", path).request()
        .header("Authorization", "Token " + token).get().readEntity(SeafileFile[].class);
  }

  /**
   * list only files (no dirs) in path
   * 
   * @param libId
   * @param path
   * @return
   */
  public SeafileFile[] listOnlyFiles(String libId, String path) {
    if (libId.equals("")) {
      LOG.info("getfiles: NOFILES");
      return null;
    }
    LOG.info("getFiles for " + libId);
    return seafileTarget.path("/api2/repos/" + libId + "/dir/").queryParam("p", path)
        .queryParam("t", "f").request()
        .header("Authorization", "Token " + token).get().readEntity(SeafileFile[].class);
  }

  /**
   * list only dirs (no files) in path
   * 
   * @param libId
   * @param path
   * @return
   */
  public SeafileFile[] listOnlyDirs(String libId, String path) {
    if (libId.equals("")) {
      LOG.info("getfiles: NOFILES");
      return null;
    }
    LOG.info("getFiles for " + libId);
    return seafileTarget.path("/api2/repos/" + libId + "/dir/").queryParam("p", path)
        .queryParam("t", "d")
        // .queryParam("recursive", "1")
        .request().header("Authorization", "Token " + token).get().readEntity(SeafileFile[].class);
  }

  /**
   * @param libId
   * @param path
   * @return
   */
  public String getDownloadUrl(String libId, String path) {
    LOG.info("getFile: " + path + " from lib: " + libId);
    System.out.println(" token: " + token);
    return seafileTarget.path("/api2/repos/" + libId + "/file/").queryParam("p", path).request()
        .header("Authorization", "Token " + token).get().readEntity(String.class);
  }

  /**
   * @param uri
   * @return
   */
  public InputStream download(URI uri) {
    LOG.info("path is: " + uri.getPath());
    return seafileTarget.path(uri.getPath()).request().get().readEntity(InputStream.class);
  }

  /**
   * @param sflibid
   * @param sfFileId
   * @return
   * @throws URISyntaxException
   */
  public InputStream readSFileToInputStream(String sflibid, String sfFileId)
      throws URISyntaxException {

    InputStream data = null;

    // for(SeafileFile sfile : SeafileUtils.getFiles(sftarget,
    // seafileAuthToken, sflibid)) {

    // redirect if file ~ 5mb
    /*
     * if(sfile.getSize() > 5000000) { // howto do this
     * 
     * //return Response.seeOther(new URI(link)).build();
     * response.addProperty(response.HTTP_STATUS_CODE, "303"); response.addProperty("Location",
     * link); response.flushBuffer();
     * 
     * return;
     * 
     * } else {
     */

    // if(sfile.getId().equals(sfFileId)) {
    String filepath = SeafileUtils.resolveIdString(sfFileId).path;

    String link = getDownloadUrl(sflibid, filepath);

    System.out.println("dl link is: " + link);

    link = link.replaceAll("\"", "");

    data = new BufferedInputStream(download(new URI(link)));
    // }
    // }

    return data;
  }

  /**
   * @return
   */
  public String getToken() {
    return token;
  }

  /**
   * @param token
   * @return
   */
  public SeafileClient setToken(String token) {
    this.token = token;
    return this;
  }

  /**
   * @param encryptedToken
   * @return
   */
  public SeafileClient setEncryptedToken(String encryptedToken) {
    this.token = tokenFromAuthstring(encryptedToken, seafileTokenSecret);
    return this;
  }

  /**
   * @param authString
   * @param secret
   * @return
   */
  public static String tokenFromAuthstring(String authString, String secret) {
    Jws<Claims> jwtres = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(authString);
    String tstring = jwtres.getBody().get("token").toString();
    String token = tstring.substring(tstring.lastIndexOf("@") + 1);
    return token;
  }

  /**
   * @param data
   * @param storageId
   * @return
   */
  public boolean isSeafileLibrary(InputStream data, String storageId) {

    Model model = ModelFactory.createDefaultModel();
    model.read(data, null, "TURTLE");

    String dariahNamespace = model.getNsPrefixURI(DARIAH_PREFIX);
    String dariahStorageNamespace = model.getNsPrefixURI(DARIAHSTORAGE_PREFIX);
    Resource res = model.getResource(dariahStorageNamespace + storageId);

    Property p = model.createProperty(dariahNamespace, "describesSeafileLibrary");

    Set<RDFNode> seafileRefs = RDFHelper.listProperties(res, p);

    if (seafileRefs.size() > 0) {
      return true;
    }

    return false;
  }

  /**
   * @param storageId
   * @return
   */
  public boolean isSeafileLibrary(String storageId) {

    String dariahStorageId = storageId.substring(storageId.lastIndexOf(":") + 1);
    try {
      InputStream data = storageClient.readFile(dariahStorageId);

      return isSeafileLibrary(data, dariahStorageId);

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return false;
  }

  /*
   * public void updateNewCollectionInfo (Long collectionId, String seafileLibraryId) {
   * 
   * SeafileLibrary info = getLibraryInfo(seafileLibraryId);
   * 
   * String rdfContent = "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" +
   * "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n" +
   * "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n" +
   * "@prefix dc: <http://purl.org/dc/elements/1.1/> .\n" +
   * "@prefix dcterms: <http://purl.org/dc/terms/> .\n" + "\n" +
   * "dariahstorage:"+collectionId+" a dariah:Collection;\n " +
   * "  dariah:describesSeafileLibrary seafile:"+seafileLibraryId+";\n" + "  dc:title  '" +
   * info.getName() + "';\n" + "  dc:description '" + info.getDesc() + "'.\n";
   * 
   * try { storageClient.updateFile(collectionId, new ByteArrayInputStream(rdfContent.getBytes()),
   * "text/plain; charset=utf-8" ); } catch (IOException e) { // TODO Auto-generated catch block
   * e.printStackTrace(); } }
   */

  /**
   * @param collectionId
   * @param seafileLibraryId
   * @param collectionName
   * @param collectionPath
   */
  public void updateNewCollectionInfo(String collectionId, String seafileLibraryId,
      String collectionName,
      String collectionPath) {

    String rdfContent = "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
        + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
        + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
        + "@prefix dc: <http://purl.org/dc/elements/1.1/> .\n"
        + "@prefix dcterms: <http://purl.org/dc/terms/> .\n" + "\n" + "dariahstorage:"
        + collectionId
        + " a dariah:Collection;\n " + "  dariah:describesSeafileLibrary seafile:"
        + seafileLibraryId + ";\n"
        + "  dariah:describesSeafileLibraryPath '" + collectionPath + "';\n" + "  dc:title  '"
        + collectionName
        + "'.\n";

    try {
      storageClient.updateFile(collectionId, new ByteArrayInputStream(rdfContent.getBytes()),
          "text/plain; charset=utf-8", token);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  /**
   * @param storageId
   * @param data
   * @return
   */
  public InputStream appendSeafileMetadata(String storageId, InputStream data) {

    System.out.println("sfutils was asked");

    Model model = ModelFactory.createDefaultModel();
    model.read(data, null, "TURTLE");

    String dariahNamespace = model.getNsPrefixURI(DARIAH_PREFIX);
    String dariahStorageNamespace = model.getNsPrefixURI(DARIAHSTORAGE_PREFIX);
    Resource res = model.getResource(dariahStorageNamespace + storageId);

    Property p = model.createProperty(dariahNamespace, "describesSeafileLibrary");
    Set<RDFNode> seafileRefs = RDFHelper.listProperties(res, p);

    if (seafileRefs.size() > 0) {
      // references for seafile found => check for new/changed files in
      // seafile
      model = addSeafileMeta(storageId, model, res, seafileRefs);
    }

    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    model.write(bout, "TURTLE", null);

    return new ByteArrayInputStream(bout.toByteArray());

  }

  /**
   * @param storageId
   * @param model
   * @param res
   * @param refs
   * @return
   */
  private Model addSeafileMeta(String storageId, Model model, Resource res, Set<RDFNode> refs) {

    boolean collectionDataUpdated = false;

    // there should be only one! referenced sflib
    String seafileRef = refs.toArray()[0].toString();
    System.out.println("[REST] seafileRef found: " + seafileRef);

    // sub path
    Property sfPathProperty = model.createProperty(model.getNsPrefixURI(DARIAH_PREFIX),
        "describesSeafileLibraryPath");
    Set<RDFNode> seafilePathRefs = RDFHelper.listProperties(res, sfPathProperty);
    String sfPathRef = "/";
    if (seafilePathRefs.size() > 0) {
      sfPathRef = seafilePathRefs.toArray()[0].toString();
      System.out.println("[REST] seafilePathRef found: " + sfPathRef);
    }

    // connect with seafile
    String sflibid = seafileRef.substring(seafileRef.lastIndexOf("/") + 1);

    Property dcHasPart = model.createProperty(model.getNsPrefixURI(DCTERMS_PREFIX), "hasPart");

    Property rdfType = model.createProperty(RDF_SCHEMA_URL, "type");

    Property dcTitle = model.createProperty(model.getNsPrefixURI(DC_PREFIX), "title");

    // TODO: do we use this later on?
    Property removedSeafileItem =
        model.createProperty(model.getNsPrefixURI(DARIAH_PREFIX), "removedSeafileItem");

    Resource dariahDataObject =
        model.createResource(model.getNsPrefixURI(DARIAH_PREFIX) + "DataObject");

    Resource dariahCollection =
        model.createResource(model.getNsPrefixURI(DARIAH_PREFIX) + "Collection");

    // TODO: possible add title only, if not already added, or use another
    // ref to orig file name
    // than dc:title (dc:source, dc:relation, dariah:orgFileName ???)
    // do we really want strings here, or are rdfnodes better?
    Set<String> parts = RDFHelper.listPropertyStrings(res, dcHasPart);

    Set<String> partsPathRefs = new HashSet<String>();
    for (String part : parts) {
      // TODO, we could check directly for collection here:
      // partRes.hasProperty(typeProp, coltype), need util function
      if (part.startsWith(model.getNsPrefixURI(DARIAHSTORAGE_PREFIX))) {
        Resource partRes = model.createResource(part);
        partsPathRefs.addAll(RDFHelper.listPropertyStrings(partRes, sfPathProperty));
      }
    }

    // list directories, store absolute dir in set, to find missing
    Set<String> subcolPaths = new HashSet<String>();
    for (SeafileFile sfile : listOnlyDirs(sflibid, sfPathRef)) {

      String subcolPath = sfPathRef + sfile.getName() + "/";
      subcolPaths.add(subcolPath);

      if (!partsPathRefs.contains(subcolPath)) {

        // TODO: check if dir was renamed using seafile-filehistory
        LOG.info("new resource (dir): " + subcolPath);

        String subcolId = createNewSFManagedSubcol(sflibid, sfile.getName(), subcolPath);

        Resource subColRes =
            model.createResource(model.getNsPrefixURI(DARIAHSTORAGE_PREFIX) + subcolId);
        model.add(subColRes, rdfType, dariahCollection);
        model.add(subColRes, dcTitle, sfile.getName());
        res.addProperty(dcHasPart, subColRes);

        Property describesPath = model.createProperty(model.getNsPrefixURI(DARIAH_PREFIX),
            "describesSeafileLibraryPath");

        model.add(subColRes, describesPath, sfPathRef + sfile.getName() + "/");
        collectionDataUpdated = true;
      } else {
        LOG.info("resource (dir) already there: " + subcolPath);
      }
    }

    // if all dirs found in seafile are removed from the dirs found in rdf,
    // only deleted items are left
    partsPathRefs.removeAll(subcolPaths);
    for (String pref : partsPathRefs) {
      System.out.println("dir removed: " + pref);
    }

    // list files
    Set<String> fileIds = new HashSet<String>();
    for (SeafileFile sfile : listOnlyFiles(sflibid, sfPathRef)) {

      String fileId =
          SeafileUtils.generateIdString(new SeafileIdEntry(sflibid, sfPathRef + sfile.getName()));
      Resource sfres = model.createResource(model.getNsPrefixURI(SEAFILE_PREFIX) + fileId);
      fileIds.add(sfres.toString());

      System.out.println(sfPathRef + sfile.getName() + " has id: " + sfile.getId());

      if (!parts.contains(sfres.toString())) {

        // TODO: check if file was renamed using seafile-filehistory
        LOG.info("new resource (file): " + sfres.toString());

        res.addProperty(dcHasPart, sfres);
        model.add(sfres, rdfType, dariahDataObject);
        model.add(sfres, dcTitle, sfile.getName());
        tikaMetaFromSeafileFile(sflibid, fileId, model);
        collectionDataUpdated = true;
      } else {
        LOG.info("resource (file) already there: " + sfres.toString());
      }
    }

    // if all ids found in seafile are removed from the ids found in rdf,
    // only deleted items are left
    parts.removeAll(fileIds);
    for (String ref : parts) {
      if (!ref.startsWith(model.getNsPrefixURI(DARIAHSTORAGE_PREFIX))) {
        System.out.println("file removed: " + ref);

        // friendly way, but needs to be communicated by gui
        // model.addLiteral(model.createResource(ref),
        // removedSeafileItem, true);

        // the brute force way, item just disappears
        model.removeAll(model.createResource(ref), null, null);
        model.removeAll(null, null, model.createResource(ref));
        collectionDataUpdated = true;
      }
    }

    if (collectionDataUpdated) {
      System.out.println("sflib updated, saving changes for: " + storageId);
      ByteArrayOutputStream outStream = new ByteArrayOutputStream();
      model.write(outStream, "TURTLE");
      ByteArrayInputStream bis = new ByteArrayInputStream(outStream.toByteArray());
      try {
        this.storageClient.updateFile(storageId, bis, "text/plain", token);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    return model;
  }

  /**
   * TODO: code dup with MainViewController:createNew()
   * 
   * @param seafileLibraryId
   * @param collectionName
   * @param collectionPath
   * @return
   */
  public String createNewSFManagedSubcol(String seafileLibraryId, String collectionName,
      String collectionPath) {

    String rdfContent = "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
        + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n";

    ByteArrayInputStream is = new ByteArrayInputStream(rdfContent.getBytes());
    String storageId = "";
    try {
      storageId = storageClient.createFile(is, "text/plain; charset=utf-8", token);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    updateNewCollectionInfo(storageId, seafileLibraryId, collectionName, collectionPath);

    return storageId;
  }

  /**
   * @param collectionId
   * @param fileId
   * @param response
   */
  public void handleSeafileDownload(String collectionId, String fileId,
      HttpServletResponse response) {

    // find seafile lib id in collection turtle
    String storageId = collectionId.substring(collectionId.lastIndexOf(":") + 1);
    InputStream data;

    try {
      data = storageClient.readFile(storageId);

      Model model = ModelFactory.createDefaultModel().read(data, null, "TURTLE");
      String dariahStorageNamespace = model.getNsPrefixURI(DARIAHSTORAGE_PREFIX);
      Resource res = model.getResource(dariahStorageNamespace + storageId);
      Property p =
          model.createProperty(model.getNsPrefixURI(DARIAH_PREFIX), "describesSeafileLibrary");
      String seafileRef = RDFHelper.listProperties(res, p).toArray()[0].toString();

      Property pathProperty = model.createProperty(model.getNsPrefixURI(DARIAH_PREFIX),
          "describesSeafileLibraryPath");
      String pathRef = RDFHelper.listProperties(res, pathProperty).toArray()[0].toString();

      // get library info from seafile
      String sfFileId = fileId.substring(fileId.lastIndexOf(":") + 1);
      String sflibid = seafileRef.substring(seafileRef.lastIndexOf("/") + 1);

      // get downloadUrl
      // for(SeafileFile sfile : SeafileUtils.getFiles(sftarget,
      // seafileAuthToken, sflibid)) {
      // if(sfile.getId().equals(sfFileId)) {
      // String filename = new String(Base64.decodeBase64(sfFileId));
      String filePath = SeafileUtils.resolveIdString(sfFileId).path;
      // String fileName = filePath.substring(filePath.lastIndexOf("/"));
      String link = getDownloadUrl(sflibid, filePath);
      link = link.replaceAll("\"", "");

      // redirect if file ~ 5mb
      // if(sfile.getSize() > 5000000) {
      /*
       * if(sfile.getSize() > 10) { // howto do this
       * 
       * //return Response.seeOther(new URI(link)).build();
       * response.addProperty(response.HTTP_STATUS_CODE, "303"); response.addProperty("Location",
       * link); response.flushBuffer();
       * 
       * return;
       */

      // } else {
      // detecting the contentType:
      // http://www.rgagnon.com/javadetails/java-0487.html
      InputStream is = new BufferedInputStream(download(new URI(link)));

      // System.out.println("filecontent");
      // IOUtils.copy(is, System.out);

      TikaConfig tika = new TikaConfig();

      Metadata metadata = new Metadata();
      metadata.set(Metadata.RESOURCE_NAME_KEY, filePath);
      MediaType mediatype = tika.getDetector().detect(is, metadata);

      System.out.println(mediatype.toString());
      System.out.println(mediatype.getType());

      // response.addProperty(response.HTTP_STATUS_CODE, 200);
      // return Response.ok(is, mediatype.toString()).build();
      response.setContentType(mediatype.toString());
      IOUtils.copy(is, response.getOutputStream());
      return;
      // }
      // }
      // }

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (TikaException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (URISyntaxException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    // return 404
    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
  }

  /**
   * @param sflibid
   * @param fileId
   * @param model
   */
  public void tikaMetaFromSeafileFile(String sflibid, String fileId, Model model) {

    Resource sfres = model.createResource(model.getNsPrefixURI(SEAFILE_PREFIX) + fileId);

    Property dcTitle = model.createProperty(model.getNsPrefixURI(DC_PREFIX), "title");
    Property dcFormat = model.createProperty(model.getNsPrefixURI(DC_PREFIX), "format");
    Property dcCreator = model.createProperty(model.getNsPrefixURI(DC_PREFIX), "creator");
    Property dcDate = model.createProperty(model.getNsPrefixURI(DC_PREFIX), "date");
    Property dcCoverage = model.createProperty(model.getNsPrefixURI(DC_PREFIX), "coverage");

    try {
      InputStream is = readSFileToInputStream(sflibid, fileId);

      AutoDetectParser parser = new AutoDetectParser();
      BodyContentHandler handler = new BodyContentHandler();
      Metadata metadata = new Metadata();

      TikaConfig tika = new TikaConfig();
      parser.parse(is, handler, metadata);

      /*
       * String[] props = {"dc:format", "dc:creator", "dc:title"}; for (String prop : props) {
       * jsonMap.put(prop, metadata.get(prop)); }
       */

      if (metadata.get("dc:title") != null) {
        model.add(sfres, dcTitle, metadata.get("dc:title"));
      }
      if (metadata.get("dc:format") != null) {
        model.add(sfres, dcFormat, metadata.get("dc:format"));
      } else {
        metadata.set(Metadata.RESOURCE_NAME_KEY,
            new String(SeafileUtils.resolveIdString(fileId).path));
        MediaType mediatype = tika.getDetector().detect(is, metadata);
        model.add(sfres, dcFormat, mediatype.toString());
      }
      if (metadata.get("dc:creator") != null) {
        model.add(sfres, dcCreator, metadata.get("dc:creator"));
      }
      if (metadata.get("dcterms:created") != null) {
        model.add(sfres, dcDate, metadata.get("dcterms:created"));
      }

      if (metadata.get("GPS Latitude") != null) {

        String coordinates =
            "geo:lat=" + metadata.get("geo:lat") + ",geo:long=" + metadata.get("geo:long");

        String coverage[] = {coordinates, "Latitude: " + metadata.get("GPS Latitude"),
            "Longitude: " + metadata.get("GPS Longitude"),
            "Altitude: " + metadata.get("GPS Altitude")};

        for (String entry : coverage) {
          model.add(sfres, dcCoverage, entry);
        }

        // jsonMap.put("dc:coverage", Arrays.asList(coverage));

      }

    } catch (URISyntaxException | TikaException | IOException | SAXException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

}
