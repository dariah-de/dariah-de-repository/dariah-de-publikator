import N3 from 'n3';
import $ from 'jquery'; // for $.get

import orderBy from 'lodash/orderBy';

const _ = {
  orderBy,
}

class TurtleLoader {

  constructor(options = {}) {
    this.store  = N3.Store();
    this.partsAsListEnabled = options.partsAsListEnabled;
  }

  load(url) {
    return new Promise(resolve => {
      //TODO: caching would save schema loading time, turn on when schema is finalized?
      $.get({url, cache: false}).then( response => {
        resolve(this.addTurtle(response));
      });
    });
  }

  // returns a promise
  addTurtle(turtle) {

    //console.log(turtle);

    let promise = new Promise(resolve => {

      let tcount = 0;

      N3.Parser().parse(
        turtle,
        (error, triple, prefixes) => {
          if (triple) {
            let res = this.store.addTriple(triple);
            tcount++;
          } else if (error) {
            console.log('ttl load error: ' + error);
          } else {
            //console.log('ttl load done, triples: ' + tcount);
            resolve(tcount);
          }
        },
        (prefix, uri) => {
          this.store.addPrefix(prefix, uri);
        }
      );

    });

    return promise;
  }

  getSchemaForSubject(subject) {
    let rform = [];

    let iterable = this.getFormTriples(subject);
    for (let i = 0; i < iterable.length; i++) {
      let formTriple = iterable[i];
      rform.push(this.rgetFieldObject(formTriple.subject));
    }

    rform = _.orderBy(rform, ['tgforms_hasPriority'], ['desc']);

    return { schema: rform };
  }

  getDataForSubject(subject) {
    return this.store.find(subject, null, null);
  }

  getFormTriples(subject) {
    let formTriples = [];
    let rdfClasses = this.getClasses(subject);

    for (let i = 0; i < rdfClasses.length; i++) {
      let rdfClass = rdfClasses[i];
      let triples = this.store.find(null, null, rdfClass);
      for (let j = 0; j < triples.length; j++) {
        let triple = triples[j];
        if (triple.predicate === this.expandPrefix("rdfs:domain")) {
          formTriples.push(triple);
        } else if (triple.predicate === this.expandPrefix("rdf:first")) {

          // We expect a statement like
          // "rdfs:domain [ a owl:Class; owl:unionOf (bol:thing bol:person) ]"
          // and want to find its domain

          let listStart = this.getListStart(triple.subject);

          // Replace blank node with RDF class

          listStart.object = expandPrefix(rdfClass);

          if (listStart.predicate === expandPrefix("rdfs:domain")) {
            formTriples.push(listStart);
          }
        }
      }
    }

    return formTriples;
  }

  getClasses(subject) {
    let rdfClasses = [subject];
    let subClassOfTriples = this.store.find(subject, "rdfs:subClassOf", null);

    for (let i = 0; i < subClassOfTriples.length; i++) {
      let subClassOfTriple = subClassOfTriples[i];
      let rdfClass = this.abbrevURI(subClassOfTriple.object);
      rdfClasses.push(rdfClass);

      let iterable = this.getClasses(rdfClass);
      for (let j = 0; j < iterable.length; j++) {
        rdfClass = iterable[j];
        if (rdfClasses.indexOf(rdfClass) === -1) {
          rdfClasses.push(rdfClass);
        }
      }
    }

    return rdfClasses;
  }

  rgetFieldObject(fieldName) {
    let field = {};

    let propTriples = this.store.find(fieldName, null, null);
    field["rdf_Property"] = this.abbrevURI(propTriples[0].subject);
    field["tgforms_hasOption"] = [];

    for (let i = 0; i < propTriples.length; i++) {
      let propTriple = propTriples[i];
      let key = propTriple.predicate;
      key = this.abbrevURI(key);

      let value = propTriple.object;

      if (N3.Util.isLiteral(value)) {
        value = N3.Util.getLiteralValue(value);
      }

      value = this.abbrevURI(value);

      if (key === "tgforms:hasOption") {
        field["tgforms_hasOption"].push(value);
      } else {
        field[key.replace(':', '_')] = value;
      }
    }

    if (!field["tgforms_hasInput"]) {
      field["tgforms_hasInput"] = "tgforms:text";
    } /*else {
      field["tgforms_hasInput"] = field["tgforms_hasInput"].replace(':', 'X');
    }*/

    field["tgforms_hasOption"] = field["tgforms_hasOption"].sort();
    field["tgforms_hasPriority"] = parseInt(field["tgforms_hasPriority"]);

    if (field["tgforms_isRepeatable"] !== "false") {
      field["tgforms_isRepeatable"] = true;
    } else {
      field["tgforms_isRepeatable"] = false;
    }

    field['isResource'] = this.isResource(fieldName);

    // the object of the spo
    field['fieldvalue'] = '';

    return field;
  }

  expandPrefix(string) {
    return N3.Util.expandPrefixedName(string, this.getPrefixes());
  }

  getPrefixes() {
    return this.store._prefixes;
  }

  abbrevURI(string) {
    let iterable = this.getPrefixes();
    for (let prefix in iterable) {
      let uri = iterable[prefix];
      string = string.replace(uri, prefix + ":");
    }

    return string;
  }

  isResource(subject) {
    let rangeObject = this.store.find(subject, "rdfs:range", null)[0].object;
    let result = false;

    if (!this.abbrevURI(rangeObject).match(/^xsd:/)) {
      result = true;

      if (N3.Util.isBlank(rangeObject)) {
        let iterable = this.getList(getUnionOf(subject, "rdfs:range"));
        for (let i = 0; i < iterable.length; i++) {
          let element = iterable[i];
          if (element.match(/^xsd:/)) {
            result = result && false;
          }
        }
      }
    }

    return result;
  }

  getList(subject) {
    let list = [];

    let firstObject = this.store.find(subject, "rdf:first", null)[0].object;
    let restObject = this.store.find(subject, "rdf:rest", null)[0].object;

    list.push(this.abbrevURI(firstObject));

    if (this.abbrevURI(restObject) !== "rdf:nil") {
      let iterable = this.getList(restObject);
      for (let i = 0; i < iterable.length; i++) {
        let element = iterable[i];
        list.push(this.abbrevURI(element));
      }
    }

    return list;
  }

  getListStart(object) {
    let triple = store.find(null, null, object);

    if (N3.util.isBlank(triple[0].subject)) {
      return this.getListStart(triple[0].subject);
    } else {
      return triple[0];
    }
  }

  getPartTriples(uri, partRelation) {
    // for order: check if found uri is blank
    // * if yes -> its a list, get thelistitems in order
    // * if no -> return it in any order
    const res = this.store.find(uri, partRelation, '');
    return res;
  }

  getPartArray(uri, partRelation) {
    const data = this.store.find(uri, partRelation, '');
    let parts = [];
    data.forEach((val) => {
      // check if parts is a list
      if(N3.Util.isBlank(val.object)) {
        parts = parts.concat(this.getList(val.object));
      } else {
        parts.push(this.abbrevURI(val.object));
      }
    });
    return parts;
  }

  getStore() {
    return this.store;
  }

  writeTurtle(triples, partlistSubject,  parts=[]) {

    let prefixes = this.store._prefixes;
    const writer = N3.Writer(prefixes);

    writer.addPrefixes(prefixes);

    //let parts = [];
    //let partlistSubject = '';
    triples.forEach(triple => {
      if(triple.predicate === 'dcterms:hasPart') {
        if(this.partsAsListEnabled) {
          // write the parts as ordered list later
          partlistSubject = triple.subject;
          parts.push(triple.object);
        } else {
          // just write the parts, don't care about order
          writer.addTriple(triple.subject, triple.predicate, triple.object); 
        }
      } else {
       writer.addTriple(triple.subject, triple.predicate, triple.object);
      }
    });

    //console.log('parts: ', parts);
    //console.log('subj: ', partlistSubject);
    if(this.partsAsListEnabled && parts.length > 0) {
      //console.log('writing partslist');
      writer.addTriple(partlistSubject, 'dcterms:hasPart', writer.list(parts));
    }

    // hmm, this looks like an async op, but it isn't,
    // so we just store the output in var result for now
    let result;
    writer.end((error, res) => {
      result = res;
    });
    return result;
  }

}

export default TurtleLoader;
