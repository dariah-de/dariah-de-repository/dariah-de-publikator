package eu.dariah.de.publikator.utils;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;

import org.apache.commons.codec.binary.Base64;

public class SeafileUtils {
	
	public static class SeafileIdEntry {
		
		   public String libraryId;
		   public String path;
		   
		   public SeafileIdEntry(){};
		   public SeafileIdEntry(String projectId, String path){
			   this.libraryId = projectId;
			   this.path = path;
		   };
		   
		   public String getRawId() {
			   return libraryId+":"+path;
		   }
		   
		   public void setRawId(String rawId) {
			   String[] arr = rawId.split(":");
			   this.libraryId = arr[0];
			   this.path = arr[1];
		   }
	}
	
	/**
	 * generate the seafile id by compressing the string "filepath" and encode as base64
	 * TODO: is compression really a good idea, or may results change?
	 * @param	filepath	path to the file
	 * @return				seafileId
	 */
	public static String generateIdString(SeafileIdEntry idEntry){
		String result="";
		try{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			Deflater compresser = new Deflater(Deflater.BEST_COMPRESSION, true);
			DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(stream, compresser);
			deflaterOutputStream.write(idEntry.getRawId().getBytes());
			deflaterOutputStream.close();
			byte[] output = stream.toByteArray();
			result = Base64.encodeBase64URLSafeString(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * read the path to the file from the seafile id, by base64 decode and decompression 
	 * @param 	id	id to get filepath from
	 * @return		filepath
	 */
	public static SeafileIdEntry resolveIdString(String id) {
		SeafileIdEntry result = new SeafileIdEntry();
		try{
		    ByteArrayOutputStream stream = new ByteArrayOutputStream();
		    Inflater decompresser = new Inflater(true);
		    InflaterOutputStream inflaterOutputStream = new InflaterOutputStream(stream, decompresser);
		    inflaterOutputStream.write(Base64.decodeBase64(id));
		    inflaterOutputStream.close();
		    result.setRawId(new String(stream.toByteArray()));	    
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;		
	}

}
