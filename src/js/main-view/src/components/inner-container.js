/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './inner-container.html';
import $ from 'jquery';

import ErrorAlert from './error-alert';
import ErrorDetailAlert from './error-detail-alert';
import PublishStartAlert from './publish-start-alert';
import ReallyDeleteAlert from './really-delete-alert';
import ReallyPublishAlert from './really-publish-alert';
import SuccessAlert from './success-alert';
import ViewProgressAlert from './view-progress-alert';
/* import CancelAlert from './cancel-alert'; */

let InnerContainer = Ractive.extend({
    template : template,

    components: {
      'error-alert' : ErrorAlert,
      'error-detail-alert' : ErrorDetailAlert,
      'publish-start-alert' : PublishStartAlert,
      'really-delete-alert' : ReallyDeleteAlert,
      'really-publish-alert' : ReallyPublishAlert,
      'success-alert' : SuccessAlert,
      'view-progress-alert' : ViewProgressAlert,
/*      'cancel-alert' : CancelAlert, */
    },
    data: {
      formattedDescription: function(text) {
        return text.replace(/(\r\n|\n\r|\r|\n)/g, "<br/>");
      },
      i18nStatus: function() {
        if (this.get('status') !== null && this.get('status') !== undefined) {
          var status = this.get('status').toLowerCase();
          return this.mainViewOptions.i18nMap['status-' + status];
        }
      },
      i18nStatusDescription: function() {
        if (this.get('status') !== null && this.get('status') !== undefined) {
          var status = this.get('status').toLowerCase();
          return this.mainViewOptions.i18nMap['status-description-' + status];
        }
      },
    },

    computed: {
      isOpen: function() {
        return this.get('storageId') == this.get('selectedCollectionUri');
      }
    },

    // Loads status once. Uses /info call with infoURL and /info/update
    // call with updateUrl.
    loadStatus: function(infoUrl) {
      // Check if storage is available (check error param from storage client request)
      if (this.get('error') !== null && this.get('error') !== undefined) {
      this.set('status', 'storage-not-available');
      this.set('description', this.get('error'));
      }
      // If storage is available check collection's info status.
      else {
        const queryStart = new Date().getTime();
        $.getJSON(infoUrl, {collectionId: this.get('storageId')}).done((res) => {
          const queryEnd = new Date().getTime();
          const duration = queryEnd - queryStart;
          this.set('status', res.status);
          this.set('pid', res.pid);
          this.set('doi', res.doi);
          this.set('crid', res.crid);
          this.set('uri', res.uri);
          this.set('module', res.module);
          this.set('task', res.task);
          this.set('progress', res.progress);
          this.set('publishInfoDuration', duration);
        }).fail((res) => {
          const queryEnd = new Date().getTime();
          const duration = queryEnd - queryStart;
          console.log("++ error fetching info status for " + this.get('storageId') + " in " + duration + "millis ++");
          this.set('status', 'service-not-available');
          this.set('publishInfoDuration', duration);
        });
      }
    },

    // Updates progress (/info/update call), and also automatically the bar.
    updateProgress: function() {
      setTimeout(() => {
    	  console.log("in update", this.get('status'));
        // If publish process is in progress (user pressed YES on reallyPublishAlert), ignore DRAFT and ERROR state! OR if publish process was NOT started, wait for ERROR or PUBLISHED as final states!
        if ((this.get('publishInProgress') && this.get('status') !== 'RUNNING') ||
          (this.get('status') !== 'ERROR' && this.get('status') !== 'PUBLISHED')) {
          // Reload status.
          this.loadStatus(this.get('getPublishInfoUrl'));
          // Set publishInProgress to false, means we are not waiting anymore for RUNNING state from DRAFT or ERROR state!
          this.set('publishInProgress', false);
          // Call updateProgress recursively.
          this.updateProgress();
        }
        else if (this.get('status') === 'PUBLISHED') {
          this.set('publishInProgress', false);
          this.set('viewSuccessAlert', true);
        }
        else {
          console.log("++ what now? ++");
          //this.set('publishInProgress', false);
          //event.original.preventDefault();
          //this.parent.updateModel();
          //this.parent.update();
          //this.loadStatus(this.get('getPublishInfoUrl'));
          this.set('publishInProgress', false);
          this.reset();
          this.loadStatus(this.get('getPublishInfoUrl'));
        }
      }, this.get('refresh'));
    },

    onrender: function() {
    // action for the refresh button
    this.on( 'reloadStatus', function (event) {
      event.original.preventDefault();
      this.loadStatus(this.get('getPublishInfoUrl'));
      });

    // Delete buttons are setting the reallyDelete template.
    this.on( 'showReallyDeleteAlert', function (event) {
      this.set('viewReallyDeleteAlert', true);
    });

    // Publish buttons are setting the reallyPublish template.
    this.on( 'showReallyPublishAlert', function (event) {
      this.set('viewReallyPublishAlert', true);
    });

/*
    // Cancel buttons are setting the cancelAlert template.
    this.on( 'showCancelAlert', function (event) {
      this.set('viewCancelAlert', true);
    });
*/

    // Detail error button shows or hides the detail alert template.
    this.on( 'showErrorDetailAlert', function (event) {
      this.set('viewErrorDetailAlert', true);
    });
    this.on( 'hideErrorDetailAlert', function (event) {
      this.set('viewErrorDetailAlert', false);
    });

    this.loadStatus(this.get('getPublishInfoUrl'));
    }
});

export default InnerContainer;
