.. publikator documentation master file, created by
   sphinx-quickstart on Thu May 21 14:55:42 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

[ :doc:`Englisch <index>` | German ]


Publikator [de]
===============

Der Einstiegspunkt zum Einspielen von Kollektionen und Daten in das DARIAH-DE Repository ist der `DARIAH-DE Publikator <https://repository.de.dariah.eu/publikator>`__, der Ihnen die Möglichkeit bietet, Ihre Kollektionen für einen Import in das DARIAH-DE Repository vorzubereiten, zu verwalten, und diese schließlich in das Repository einzuspielen.


Kollektionen
------------

Der Begriff *Kollektion* bedarf im Zusammenhang mit dem `DARIAH-DE Repository <https://repository.de.dariah.eu>`__ bzw. der `DARIAH-DE Forschungsdaten-Föderationsarchitektur <https://de.dariah.eu/en/web/guest/weiterfuhrende-informationen>`__ einer Erklärung. Eine Kollektion bezeichnet hier zunächst eine Menge an Forschungsdaten, also praktisch eine Menge an Dateien, die auf irgendeine Art und Weise zusammen gehören. Der Begriff Sammlung kann hier synonym gebraucht werden.

Sind Ihre Dateien bereits als eine Kollektion öffentlich zugänglich und sind idealerweise schon mit persistenten Identifikatoren versehen und kümmert sich jemand (z. B. ein Rechenzentrum) um deren sichere Aufbewahrung, dann können Sie diese guten Gewissens als Kollektion in die DARIAH-DE Collection Registry eintragen und dort beschreiben. Existiert dann noch eine technische Schnittstelle zu Ihrer Kollektion (z. B. per OAI-PMH), können Sie auch diese in Ihrer Kollektionsbeschreibung angeben. So werden die Inhalte Ihrer Kollektion in der Generischen Suche von DARIAH-DE indiziert und sind dort auffindbar.

Ihre Forschungsdaten können jedoch – als Kollektion oder auch als Einzeldateien – auch nur bei Ihnen lokal auf einer Festplatte, auf einer CD oder an einer anderen nicht öffentlich zugänglichen Stelle gespeichert sein. Dann sind sie für andere Forschende nicht oder nur umständlich zugänglich, Ihre Forschungsdaten können nicht von anderen Interessierten gesucht und gefunden werden und sind unter Umständen, wenn sie nicht gepflegt werden, für die Wissenschaft verloren. Wenn Sie Ihre Daten also für andere Wissenschaftlerinnen und Wissenschaftler zur Verfügung stellen wollen, wenn Sie Ihre Forschungsergebnisse sicher und zitierbar aufbewahrt wissen wollen, dann können Sie diese über den DARIAH-DE Publikator in das DARIAH-DE Repository einspielen. Dann

    * werden Ihre Forschungsdaten sicher im Repository gespeichert,

    * bekommen Ihre Forschungsdaten einen Persistenten Identifikator (die Kollektion selbst und alle darin enthaltenen Dateien).

Ihre Daten sind dann

    * dauerhaft referenzierbar und zitierbar,

    * öffentlich zugänglich,

    * sind in der Collection Registry als Kollektion beschrieben und

    * sind sowohl in der Repository Suche als auch in der DARIAH-DE Generischen Suche recherchierbar.

Ihre Forschungsdaten sind damit in den Forschungsdatenzyklus aufgenommen und stehen so für die Nachnutzung zur Verfügung.


Einloggen mit DARIAH- oder Föderationsaccount
---------------------------------------------

Sie erreichen den DARIAH-DE Publikator im DARIAH-DE Portal von der Seite des

    * `DARIAH-DE Repository <https://de.dariah.eu/repository>`__

aus oder auch direkt über diesen

    * `DARIAH-DE Publikator <https://repository.de.dariah.eu/publikator>`__.


.. figure:: ./pics/figure01-de.png
    :align: center
    :alt: Willkommensseite des Publikators
    :figclass: align-center

    Willkommensseite des Publikators


Nachdem Sie auf der Willkommensseite auf den Button *Mit dem DARIAH-DE Publikator beginnen* geklickt haben (siehe Abbildung 1), melden Sie sich bitte mit Ihrem DARIAH- oder Föderations-Account an. Sollten Sie keinen `DARIAH-Account <https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg>`__ haben, können Sie `HIER <https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg>`__ einen solchen beantragen.


.. figure:: ./pics/figure02.png
    :align: center
    :alt: Auswahl der Organisation
    :figclass: align-center

    Auswahl der Organisation


.. figure:: ./pics/figure03.png
    :align: center
    :alt: DARIAH Anmeldefenster
    :figclass: align-center

    DARIAH Anmeldefenster


Erstmalige Bestätigung Zugriff DARIAH-Account auf Storage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Wenn Sie sich das erste mal beim DARIAH-DE Publikator anmelden, werden Sie gefragt, ob Ihr Account auf den DARIAH-Storage zugreifen darf. DIese Abfrage erscheint, weil die DARIAH Förderationsarchitektur OAuth für die Dienste benutzt. Sie müssen den Zugriff erlauben, sonst können Sie den DARIAH-DE Publikator nicht benutzen. Diese Abfrage erscheint nur bei der erstmaligen Anmeldung.


.. figure:: ./pics/figure04.png
    :align: center
    :alt: Einmaliges Bestätigen des Zugriffs auf den DARIAH-Storage
    :figclass: align-center

    Einmaliges Bestätigen des Zugriffs auf den DARIAH-Storage


Publizieren mit dem DARIAH-DE Publikator
----------------------------------------

Eine Kollektion, die Sie im DARIAH-DE Publikator erstellen, dient zunächst nur der Aggregation Ihrer Forschungsdaten. Sie haben damit eine übergeordnete Einheit geschaffen, die Ihre Daten zu einem Thema zusammenfasst und mit der Sie Ihre Daten als eine Kollektion zusammengehöriger Objekte beschreiben können. Sie können nun Daten dieser Kollektion zuordnen und für die Publikation hochladen. Auch Ihre Dateien werden mit Metadaten beschrieben. Als Metadatenstandard wurde hier `Dublin Core Simple <http://www.dublincore.org/documents/dces/>`__ genutzt, um einen generischen Ansatz zu verfolgen, so dass Ihnen ein kleiner Grundstock an Metadaten zur Verfügung steht, um Ihre Daten zu beschreiben. Es sind nur wenige Angaben verpflichtend.

Nach der Publikation sind dann die Daten dieser Kollektion im DARIAH-DE Repository sicher gespeichert und öffentlich zugänglich. Sie können die Persistenten Identifikatoren (DOIs) Ihrer Kollektion und Daten für die Referenzierung nutzen. Ist der Publikationsvorgang abgeschlossen, sind Ihre Kollektion und deren Daten ebenfalls über die Repository Suche recherchierbar.

Der DARIAH-DE Publikator veröffentlicht für Sie bei der Publikation eine Kollektionsbeschreibung in der Collection Registry des Repositorys, die auf den von Ihnen eingegebenen Metadaten beruht und die von Ihnen bei Bedarf dort ergänzt werden kann. In der Collection Registry werden nicht die Daten der Kollektion selbst, sondern nur Referenzen auf diese Daten gespeichert (bzw. eine Zugriffsmethode auf die Daten angegeben). In der Collection Registry ist Ihre Kollektion beschrieben – samt technischen Schnittstellen, und Sie können dafür auf ein weit umfangreicheres Beschreibungsschema (`DARIAH Collection Description Data Model – DCDDM <https://github.com/DARIAH-DE/DCDDM>`__) zugreifen als dies bei der Veröffentlichung mit Dublin Core Simple möglich ist.

.. note:: Die Dateien werden vom DARIAH-DE Publikator zunächst im DARIAH-DE OwnStorage – eine Implementierung der `DARIAH Storage API <http://hdl.handle.net/11858/00-1734-0000-0009-FEA1-D>`__ – gespeichert. Während des Publikationsvorgangs liefert der DARIAH-DE Publikator die Objekte einer Kollektion samt Metadaten dann an den DARIAH-publish Service. Dieser fordert die DOIs an und gibt wiederum die Daten an den DARIAH-crud Service weiter, der basale Operationen wie CREATE und RETRIEVE auf dem DARIAH-DE OwnStorage und nun auch dem PublicStorage ausführt. Der DARIAH-crud Service führt einige Metadatenkonversionen durch und speichert letztendlich jede einzelne Datei samt deskriptiven, administrativen und technischen Metadaten sicher im Repository.


Die zwei Ansichten des DARIAH-DE Publikators
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Benutzeroberfläche des DARIAH-DE Publikators ist in zwei Ansichten aufgeteilt. Die erste beinhaltet die **Übersicht über Ihre Kollektionen**. Hier können Sie Kollektionen anlegen und es wird Ihnen eine Liste aller Kollektionen angezeigt, die Sie bisher angelegt haben. Für jede Kollektion in dieser Liste werden der Titel und der jeweilige Status des Publikationsvorgangs angezeigt:

    :ENTWURF: Die Kollektion wurde neu angelegt oder befindet sich in Bearbeitung innerhalb des Publikators. Kollektionen im Entwurfsstatus sind nur für Sie als angemeldete Benutzerin oder angemeldeter Benutzer sichtbar.

    :LAUFEND: Ein Publikationsvorgang wurde gestartet und ist gerade in Arbeit .

    :FEHLER: Während eines Publikationsvorgangs trat ein Fehler auf.

    :PUBLIZIERT: Die Kollektion und ihre Daten sind im DARIAH-DE Repository publiziert, in der Collection Registry des Repositorys registriert und in der Repository Suche sowie zusätzlich in der DARIAH-DE Generischen Suche nachgewiesen.


Die Übersichtsseite dient auch zum Starten des Publikationsvorgangs, nachdem Sie eine Kollektion angelegt, Dateien hochgeladen und die Kollektion mit Metadaten beschrieben haben. Sie gelangen in die zweite Ansicht des Publikators indem Sie auf *neue Kollektion anlegen* klicken oder eine bereits existierende Kollektion aufklappen und in dieser Kollektion auf *Kollektion bearbeiten* klicken. In der bearbeiten Ansicht können Sie Inhalte der Kollektion bearbeiten und die Metadaten editieren.


Mit dem Publizieren beginnen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Anlegen einer neuen Kollektion
""""""""""""""""""""""""""""""

.. figure:: ./pics/figure05-de.png
    :align: center
    :alt: Die Einstiegsseite des Publikators
    :figclass: align-center

    Die Einstiegsseite des Publikators


Sollten Sie noch keine Kollektion erstellt haben, können Sie durch Klicken auf *neue Kollektion anlegen* eine neue Kollektion anlegen. Eine neu angelegte Kollektion befindet sich zunächst im Status ENTWURF. Sie gelangen nun direkt in die Ansicht Kollektion bearbeiten.


Auszeichnen der Kollektion mit Metadaten
""""""""""""""""""""""""""""""""""""""""

Alle Änderungen, die Sie in dieser Ansicht durchführen, werden automatisch gespeichert. Wenn Sie also auf den Button *zurück zur Übersicht* klicken können, sind alle Ihre Daten sicher gespeichert.

Sie müssen hier zunächst die angezeigten verpflichtenden Metadatenfelder ausfüllen, um Ihre Kollektion direkt treffend zu beschreiben. Es sind drei Angaben Pflicht:

    :TITEL: (dc:title) Der Name der Ressource. Der Titel ist der von der Schöpferin / vom Urheber vergebene Name des Werkes. Beispiel (bibliographisch): „The Tragedy of Romeo and Juliet“ wäre hier als Beispiel für den vollständigen Titel zu nennen. Alternative Titel können auch in anderen Sprachen aufgeführt werden. Beispiel (Wörterbuch des Klassischen Maya): „Tikal Stele 16“ und „Tikal Stela 16“ kommen als deutsch- und englischsprachige Titel in Frage, „TIK St. 16“ als Abkürzung.

    :SCHÖPFER/IN, URHEBER/IN: (dc:creator) Eine Person oder Einrichtung (bei mehreren Schöpferinnen / Schöpfern bitte jede einzelne mit eigenem Metadatenfeld Schöpfer/in, Urheber/in einfügen), die sich für die Schöpfung der Ressource verantwortlich zeigt. Ein/e Schöpfer/in kann beispielsweise eine Person, eine Organisation oder ein Dienst sein. Üblicherweise wird der Name der Schöpferin / des Schöpfers zur Bezeichnung verwendet. Sollten Sie eine ORCID oder eine GND ID haben, z. B. „orcid.org/0000-0003-1259-2288“ oder „gnd:135494737“, so können Sie diese hier angeben. Beispiel (bibliographisch): William Shakespeare kann in diesem Feld als Autor des Werkes „Romeo und Julia“ eingetragen werden. Tritt der Fall ein, dass der Verfasser einer Edition über William Shakespeare dieses Werk stark bearbeitet hat, kann dieser als weiterer Schöpfer eingetragen werden. Beispiel (Wörterbuch des Klassischen Maya): Meist sind die Schöpfer der Textträger bzw. der Texte der Klassischen Maya-Kultur nicht bekannt, in manchen Fällen kann zumindest eine Schreiber- oder Bildhauerschule identifiziert werden, die als Schöpfer gelten kann.

    :RECHTEVERWALTUNG: (dc:rights) Informationen über Rechte an der Ressource. Üblicherweise beinhalten Rechteinformationen Aussagen über verschiedenartige Eigentumsrechte, die mit der Ressource verbunden sind, einschließlich der Schutz- und Urheberrechte. Beispiel (bibliographisch): Im Fall des Shakespear'schen Originalwerks könnte hier stehen: „This content is public domain.“, bzw. würde ein Link auf „https://creativecommons.org/share-your-work/public-domain/pdm“ ausreichen. Für andere, nicht gemeinfreie Ressourcen, sollte entweder der Copyright-Inhaber oder aber die Art der erlaubten Nutzung bspw. durch Angabe der Lizenz eintragen werden. Für Metadaten gilt meistens CC0 „https://creativecommons.org/share-your-work/public-domain/cc0”, für Bildobjekte (Fotos, Zeichnungen etc.) muss ebenfalls die Art der erlaubten Nutzung bspw. durch Angabe der Lizenz eintragen werden, die für verschiedene Objektgruppen jeweils eine andere sein kann. Generell empfehlen wir die Nutzung von `creative commons Lizenzen <https://creativecommons.org/licenses/>`__.

Die erforderlichen Metadatenfelder sind mit einem Sternchen (*) markiert und erscheinen in roter Schrift, solange die Felder noch nicht ausgefüllt sind. Sollten Sie nicht mit dem Dublin Core Metadatenschema vertraut sein, können Sie sich durch Klick auf das (i) eine Beschreibung des Metadatenfeldes samt Beispielen anzeigen lassen. Dublin Core Simple verfügt über 15 Metadatenfelder, die übrigen zwölf können Sie durch Klicken des Buttons zeige optionale Metadaten hinzufügen und ausfüllen. Alle Felder können mehrfach verwendet werden. Sie können diese mit einem Klick auf (+) beliebig oft hinzufügen und mittels (-) auch wieder löschen. Von jedem Pflichtfeld muss mindestens eines ausgefüllt sein, wenn die Kollektion später publiziert wird.

Weitere Informationen zur Nutzung von DC-Simple Metadaten finden Sie in Sektion 3 der `DCMI Metadata Terms <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#section-3>`__.

Bitte bedenken Sie, dass Ihre publizierten Daten wesentlich besser gefunden und nachgenutzt werden können, je mehr sinnvolle Metadaten Sie vergeben.


.. figure:: ./pics/figure06-de.png
    :align: center
    :alt: Kollektionen bearbeiten im Publikator
    :figclass: align-center

    Kollektionen bearbeiten im Publikator


Anhängen von Dateien – und noch mehr Metadaten...
"""""""""""""""""""""""""""""""""""""""""""""""""

Sie können nun Ihre Forschungsdaten als Dateien durch Klicken auf das Feld *Dateien hinzufügen* oder per Drag and Drop auf das Hochladen-Feld zu Ihrer Kollektion hinzufügen. Diese erscheinen nach dem Hochladen links im Baum Ihrer Kollektion. Zwei Metadatenfelder werden automatisch vergeben: Als Titel wird der Dateiname verwendet, und als Format der Mimetype der Datei, der automatisch ermittelt wird. Diese Daten können Sie gerne übernehmen oder auch anpassen. Auch für jede Datei sind die drei oben erwähnten Metadatenfelder verpflichtend.

Sollten Sie jedoch viele Dateien zu Ihrer Kollektion hinzufügen, müssen Sie nicht alle Metadaten für jede Datei einzeln eintragen. Für Schöpfer/in oder Urheber/in bzw. die Rechteverwaltung oder Lizenzierung können Sie im Baum den Titel Ihrer Kollektion anwählen und dann auf Pfeil-nach-unten klicken. Dann wird der Inhalt des ausgewählten Felds (z. B. Rechteverwaltung) auf alle an die Kollektion angehängten Dateien kopiert. Sollte bereits ein Inhalt für Rechteverwaltung existieren, wird dieser nicht gelöscht, sondern es wird ein weiteres Feld hinzugefügt. Hier sollten Sie vorsichtig sein, dass Sie nicht versehentlich z. B. den Titel der Kollektion für alle Dateien übernehmen. Es existiert **noch keine** zurück- oder undo-Funktion!

Unten sehen Sie eine mit optionalen Metadaten angereicherte Sicht auf die Beispielkollektion mit bereits angehängter Inhaltsdatei:


.. figure:: ./pics/figure07-de.png
    :align: center
    :alt: Metadaten der Kollektion bearbeiten
    :figclass: align-center

    Metadaten der Kollektion bearbeiten


In Abbildung 8 sehen Sie die Sicht auf die Metadaten der angehängten Datei. Jede Datei sowie die Kollektion hat einen eigenen Satz an Metadaten, Sie können diese völlig unabhängig voneinander bearbeiten. Haben Sie eine  Datei links im Baum ausgewählt, können Sie die *Datei ansehen*, die *Datei löschen* und die *Datei austauschen*. Löschen Sie die Datei, wird sie aus dem OwnStorage entfernt und auch die Metadaten werden gelöscht. Die Datei verbleibt selbstverständlich auf Ihrer Festplatte. Wollen Sie die Datei aktualisieren, zum Beispiel weil Sie lokal Änderungen an ihr durchgeführt haben, können Sie die Datei austauschen. Bitte prüfen Sie, ob alle Metadaten nach einem Austausch korrekt sind, möglicherweise werden automatisch ermittelte Metadaten hinzugefügt.


.. figure:: ./pics/figure08-de.png
    :align: center
    :alt: Metadateneingabe für die Dateien
    :figclass: align-center

    Metadateneingabe für die Dateien


Sortieren von Dateien
"""""""""""""""""""""

Standardmäßig werden Dateien nach der Reihenfolge beim Hochladen sortiert. Sie können links im Baum die Dateien sortieren, in dem Sie per Drag and Drop die Reihenfolge ändern. Außerdem können Sie Dateien in Unterkollektionen verschieben und die Reihenfolge von Unterkollektionen ändern.


Zurück zur Übersichtsseite
^^^^^^^^^^^^^^^^^^^^^^^^^^

Sie können Ihre Kollektion beliebig oft bearbeiten, die Daten und Metadaten sind im Publikator sicher zwischengespeichert, bis Sie diese publizieren wollen. Sind Sie mit dem Bearbeiten Ihrer Kollektion fertig, oder vorerst fertig, können Sie *zurück zur Übersicht* wechseln und jederzeit später weiter an Ihrer Kollektion arbeiten. Sie bekommen in der Übersicht eine Liste Ihrer Kollektionen angezeigt. Kommen Sie aus dem Kollektion bearbeiten Modus, ist die zuletzt bearbeitete Kollektion bereits aufgeklappt.

.. figure:: ./pics/figure09-de.png
    :align: center
    :alt: Die Kollektion in der Übersicht
    :figclass: align-center

    Die Kollektion in der Übersicht


Sie können nun weitere Kollektionen anlegen oder mit dieser weiter arbeiten. Da Sie im DARIAH-DE Portal angemeldet sind, sind die Kollektionen dieser Ansicht nur für Sie sichtbar, solange sie nicht publiziert wurden. Diese Kollektion befindet sich im Status ENTWURF. In der Tabelle unter dem Listentitel sind zur Übersicht einige Metadaten der Kollektion aufgeführt. Im Feld darunter ist erklärt, welche Möglichkeiten Sie haben, nun weiter vorzugehen.

.. note::

    **Status: ENTWURF**

    Ihre Kollektion befindet sich im Entwurfsstadium und ist vorerst nur für Sie sichtbar. Klicken Sie auf **Kollektion bearbeiten**, können Sie Dateien zu Ihrer Kollektion hinzufügen und die Kollektion und deren Inhalte mit Metadaten anreichern. Bitte beachten Sie, dass einige Metadatenfelder zwingend erforderlich ausgefüllt werden müssen, bevor Sie diese Kollektion publizieren können. Es stehen Ihnen auch optionale Metadatenfelder zur Verfügung, die die Sichtbarkeit Ihrer Kollektion nach der Publikation erhöhen.

    Haben Sie Ihre Kollektion fertig bearbeitet und sind zufrieden mit der Metadatenauszeichnung, können Sie die **Kollektion publizieren**: Ihre Kollektion und alle beinhalteten Dateien bekommen während des Publikationsvorgangs persistente Identifikatoren und können damit dauerhaft und eindeutig referenziert werden. Es können zu einer publizierten Kollektion keine Dateien mehr hinzugefügt, gelöscht oder ausgetauscht werden.

    Sie können die Kollektion und alle enthaltenen Dateien und Metadaten letztendlich auch aus dem Publikator löschen, wobei Ihre Quelldateien auf Ihrer Festplatte verbleiben. Publizierte Kollektionen können aus dem Publikator gelöscht werden, jedoch **nicht aus dem DARIAH-DE Repository**! Wurde die Kollektion publiziert, kann die Kollektion aus dem Publikator mit **Kollektion löschen** aus der Übersichtsseite entfernt werden. Sie bleibt im DARIAH-DE Repository enthalten.


Publizieren der Kollektion
^^^^^^^^^^^^^^^^^^^^^^^^^^

Sie sind fertig mit dem Anlegen Ihrer Kollektion, alle Dateien wurden hinzugefügt, alle Metadaten (zumindest die verpflichtenden) sind vorhanden? Dann können Sie auf den Button *Kollektion publizieren* klicken.

.. warning:: Seien Sie sich bitte bewusst, dass alle Daten und Metadaten nach dem Publikationsvorgang **öffentlich zugänglich sind und von Ihnen nicht mehr editiert und gelöscht werden können**!


Status: laufend
"""""""""""""""

Nach dem Klick auf *Kollektion publizieren* werden Sie gefragt, ob Sie alle Rechte an der Kollektion und den Dateien zur Veröffentlichung haben und ob Sie den `Nutzungsbedingungen des DARIAH-DE Repositorys <https://doi.org/10.20375/0000-000B-C8F1-3>`__ zustimmen. Bitte lesen Sie diese sorgfältig durch. Sie werden darauf hingewiesen, dass Ihre Kollektion und die dazugehörigen Daten nicht mehr aus dem DARIAH-DE Repository gelöscht werden können und Sie bekommen einen Hinweis, dass der Publikationsvorgang gestartet wurde. Sodann wechselt der Status Ihrer Kollektion in den Status LAUFEND.

.. figure:: ./pics/figure10-de.png
    :align: center
    :alt: Bestätigung der Nutzungsbedingungen
    :figclass: align-center

    Bestätigung der Nutzungsbedingungen


.. figure:: ./pics/figure11-de.png
    :align: center
    :alt: Der Publikationsprozess
    :figclass: align-center

    Der Publikationsprozess


Während des Publikationsvorgangs geschehen viele Aktionen, von denen die wichtigsten in der nachfolgenden Info-Box beschrieben sind (für die Arbeit mit dem Publikator können Sie diese gerne überlesen). Daten und Metadaten Ihrer Kollektion werden vom Publikator an den DARIAH-Publish Service weitergegeben und von dort aus an den DARIAH-crud Service. Informationen zum Status des Publikationsprozesses werden Ihnen in der blauen Box angezeigt. Diese Informationen kommen direkt vom Publish-Service und sind teilweise sehr technisch und im Allgemeinen auch nicht übersetzt.


.. note::

    **Der DARIAH-publish Service...**

    ...ist ein Workflow-Service, der verschiedene Schritte im Rahmen der Publikation ausführt.

    Es werden u. a. die Metadaten validiert, Referenzen auf Objekte innerhalb der einzuspielenden Kollektion von Dateipfaden auf persistente Identifikatoren (DOIs) umgeschrieben und technische Metadaten generiert. Schließlich werden, nach dem Erzeugen der Kollektions-Datei, alle referenzierten Daten samt Metadaten aus dem OwnStorage (per Referenz) an den DARIAH-crud weitergegeben.

    Wird der Aufruf des Publish-Services erfolgreich beendet, ist Ihre Kollektion erfolgreich publiziert worden. Dies bedeutet, dass

        * alle Dateien in den PublicStorage geschrieben wurden, wo sie öffentlich zugänglich sind,

        * alle Dateien einen persistenten Identifikator (DOI) erhalten haben und über die `Datacite Suche <https://search.datacite.org/>`__ recherchierbar sind,

        * die Kollektion und ihre Inhalte über den DARIAH OAI-PMH-Service abfragbar sind,

        * eine Kollektionsbeschreibung für Ihre publizierte Kollektion in der Collection Registry des Repositorys angelegt wurde,

        * Ihre Kollektion in der Repository Suche nachgewiesen ist sowie zusätzlich von der DARIAH-DE Generischen Suche indiziert wird.


    .. figure:: ./pics/figure12.png
        :align: center
        :alt: Architektur des DARIAH-DE Repositorys
        :figclass: align-center

        Architektur des DARIAH-DE Repositorys


.. note::

    **Der DARIAH-crud Service...**

    ...ist der Speicher-Service des DARIAH-DE Repository, und stellt grundlegende Speicheroperationen zur Verfügung.

    Es sind zwei Instanzen des DH-crud Services in Betrieb. Die eine ist nur intern zu erreichen (z. B. vom DARIAH-publish Service). Dieser ist vornehmlich für die Erzeugung und Verwaltung von Daten zuständig. Hier werden die Metadaten und Daten aller Objekte

        * im DARIAH-DE PublicStorage gespeichert,

        * die Metadaten in die Indexdatenbank eingetragen für einen späteren Abruf per OAI-PMH Service und

        * ein DOI erzeugt, der jedes Objekt eindeutig und dauerhaft identifiziert und referenziert.

    Die zweite Instanz, die nur lesenden Zugriff auf die Daten erlaubt, ist extern zu erreichen und gibt Daten- sowie Metadaten der gespeicherten Objekte heraus, sowie eine Einstiegsseite (Landing Page) zur Übersicht über die Kollektion und ihre Inhalte.


Status: publiziert
""""""""""""""""""

War der Publikationsvorgang erfolgreich, ändert sich der Status Ihrer Kollektion von LAUFEND auf PUBLIZIERT und sieht nun in der aufgeklappten Übersicht wie folgt aus:

.. figure:: ./pics/figure13-de.png
    :align: center
    :alt: Die Übersicht nach erfolgreicher Publikation
    :figclass: align-center

    Die Übersicht nach erfolgreicher Publikation


Der erzeugte Persistente Identifikator Ihrer Kollektion wird in der Tabelle als DOI der Kollektion angezeigt (in unserem Beispiel `doi:10.20375/0000-000B-C8EF-7 <https://doi.org/10.20375/0000-000B-C8EF-7>`__). Der dort unterlegte Link führt Sie direkt über den DOI-Resolver auf die für jedes Objekt vorhandene Einstiegsseite im DARIAH-DE Repository. Von dort aus haben Sie dann Zugriff auf die publizierten Metadaten und Daten. Im Folgenden noch der Infotext aus der Übersicht:

.. note::

    **Status: PUBLIZIERT**

    **Herzlichen Glückwunsch!** Ihre Kollektion ist nun publiziert, öffentlich zugänglich und über den angezeigten Identifikator (DOI) referenzierbar!

    Klicken Sie auf den DOI oder auf **Landing Page anzeigen**, gelangen Sie direkt zu der Einstiegsseite Ihrer Kollektion im DARIAH-DE Repository. Von dort aus haben Sie direkten Zugriff auf Daten und Metadaten Ihrer Kollektion und können sich deskriptive, technische und administrative Metadaten Ihrer Kollektion sowie zugehörigen Objekte anzeigen lassen. In der Suche des Repositorys finden Sie Ihre Kollektion über den Klick auf **im Repository anzeigen**​. Die während des Publikationsprozesses automatisch in der Collection Registry des Repositorys angelegte Kollektionsbeschreibung können Sie gern erweitern. Bitte klicken Sie auf **editieren in Collection Registry**. Mit **Kollektion löschen** werden nur die Daten und Metadaten Ihrer Kollektion hier im Publikator gelöscht, **nicht jedoch aus dem DARIAH-DE Repository**!

    Zusätzlich zur Publikation im DARIAH-DE Repository wird Ihre Kollektion auch durch die **DARIAH-DE Generische Suche** indiziert. Ihre Inhalte sind auch dort öffentlich recherchierbar.


Die Einstiegsseite gibt Ihnen einen schnellen Überblick auf Ihre Kollektion und deren Daten. Dort bekommen Sie weiterhin einige Kernmetadaten der jeweiligen Kollektion oder Inhaltsdatei angezeigt und Sie können alle Daten und Metadaten herunterladen.

.. figure:: ./pics/figure14.png
    :align: center
    :alt: Die Einstiegsseite der publizierten Kollektion (Landing Page)
    :figclass: align-center

    Die Einstiegsseite der publizierten Kollektion (Landing Page)


Sie können für Ihre publizierte Kollektion sowie für jede publizierte Datei alle erzeugten und von Ihnen vergebenen Metadaten einzeln über den DARIAH-crud Dienst abrufen. Unter *Metadata* > *descriptive* können Sie alle von Ihnen vergebenen Dublin Core-Simple Metadaten abrufen, unter *Metadata* > *administrative* und *Metadata* > *technical* die generierten administrativen und technischen Metadaten, wie auch die Datei unter *Data* > *download file only*. Alle Metadaten und die Datei selbst werden zusammen in einer Bagit-Bag als ZIP-File im DARIAH-DE PublicStorage gesichert, siehe *Data* > *download complete bag*. Auch die Kollektion selbst ist im Repository als eine einzelne Datei abgelegt, die auf ihre Inhaltsdateien per DOI referenziert.

Mehr zu der DARIAH-crud API finden Sie über den Link im Menü: *Help* > *Repository API Documentation* oder auch direkt `HIER <https://repository.de.dariah.eu/doc/services/submodules/tg-crud/service/dhcrud-webapp-public/docs/index.html>`__.

Weitere -- eher technische -- Links und Referenzen, zum Beispiel zu Handle-Metadaten oder Links direkt zum DARIAH-DE OAI-PMH-Service Ihrer Kollektion finden Sie im Fußbereich der Einstiegsseite (Landing Page) ihrer Objekte.


Status: Fehler
""""""""""""""

Treten während des Publikationsvorgangs Fehler auf, ändert sich der Status Ihrer Kollektion auf FEHLER. Sie bekommen zunächst eine allgemeine Fehlerbeschreibung angezeigt, die Ihre Kollektion betrifft.

.. figure:: ./pics/figure16-en.png
    :align: center
    :alt: Fehler während des Publikationsvorgangs
    :figclass: align-center

    Fehler während des Publikationsvorgangs


Eine detaillierte Fehlerbeschreibung bekommen Sie, wenn Sie auf *zeige Fehlerdetails* klicken. Dort können Sie in einigen Fällen auch gleich an die fehlerhafte Stelle in Ihrer Kollektion springen, die Sie korrigieren müssen, um Ihre Kollektion dann erneut zu publizieren.

.. figure:: ./pics/figure17-en.png
    :align: center
    :alt: Fehlerdetails
    :figclass: align-center

    Fehlerdetails


In unserem Beispiel fehlen einige verpflichtende Metadaten zu Schöpfer/in bzw. Urheber/in. Nachdem Sie den Fehler über den Button *editieren* korrigiert haben, können Sie die Kollektion erneut publizieren.

.. note:: Sollten hier Fehler auftreten, bei denen Sie der Meinung sind, sie nicht beheben zu können, melden Sie bitte gerne an `support@de.dariah.eu <mailto:support@de.dariah.eu>`__. Den Bugtracker finden Sie bei `gitlab.gwdg.de <https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/-/issues>`__. Gerne fügen wir Sie auch als Reporter hinzu, bitte schreiben Sie hierfür an `register@dariah.eu <mailto:register@dariah.eu>`__.


Repository Suche und Collection Registry
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ihre Kollektion ist nun im DARIAH-DE Repository sicher und nachhaltig gespeichert und per DOI persistent referenzierbar. Mittels einer URL samt Handle-Resolver und DOI kann nun auf Ihre Kollektion und deren Daten öffentlich zugegriffen werden. Ihre Kollektion ist in der Collection Registry des Repositorys mit einer Kollektionsbeschreibung veröffentlicht und somit auch gleich in der Repository Suche nachgewiesen.

Wollen Sie Ihre Kollektion nun noch ausführlicher beschreiben, erweitern und bearbeiten Sie die auf Ihren Metadaten basierende Kollektionsbeschreibung Ihrer Kollektion. Dazu klicken Sie bitte auf **editieren in Collection Registry**. Sie gelangen direkt auf die Seite Ihrer Sammlungsbeschreibung in der Collection Registry des Repositorys:

.. note::

    Je nach Browser-Konfiguration könnte es möglich sein, dass Sie noch einmal nach Ihrem Login gefragt werden. Bitte melden Sie sich mit dem selben Account ein, mit dem Sie im Publikator angemeldet sind. Normalerweise werden Sie automatisch mit Ihrem aktuellen Publikator-Login auch an der Collection Registry angemeldet.


.. figure:: ./pics/figure18-de.png
    :align: center
    :alt: Der Sammlungseditor
    :figclass: align-center

    Der Sammlungseditor der Collection Registry des Repositorys


Wenn Sie weitere Informationen zur Kollektion erfassen möchten, so klicken Sie auf **Hinweise einblenden** (Editor-Optionen auf der linken Seite) und ergänzen Sie nach Belieben die Angaben zu Ihrer Kollektion. Soll Ihre Kollektion weiterhin in der Repository Suche verfügbar sein, dürfen Sie die Zugriffsdaten (OAI-PMH) unter **Zugriff auf Kollektion** nicht löschen oder verändern!

Die Suche des Repositorys und die Repository-eigene Collection Registry finden Sie auch direkt unter den Adressen <https://repository.de.dariah.eu> und <https://repository.de.dariah.eu/colreg-ui>.

In der Generischen Suche von DARIAH-DE <https://search.de.dariah.eu> wird Ihre Kollektion ebenfalls indiziert.


Nachweis und Nachnutzung
------------------------


Persistente Identifikatoren (DOIs)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Der Nachweis Ihrer Kollektion und der enthaltenen Daten geschieht hauptsächlich über den persistenten Identifikator (DOI). Die Kollektion selbst sowie jede einzelne Inhaltsdatei bekommt einen solchen DOI und er sieht aus wie oben schon einige Male erwähnt:

..

    10.20375/0000-000B-C8EF-7


Sie können diesen DOI nun verwenden als Referenz auf Ihre Kollektion und auf Ihre Forschungsdaten. Als DOI und Identifikator sollten Sie ihn als

..

    doi:10.20375/0000-000B-C8EF-7


nutzen. Wollen Sie zugleich eine URL nutzen oder weiter geben, können Sie einfach einen DOI-Resolver davor setzen:

..

    `doi.org/10.20375/0000-000B-C8EF-7 <https://doi.org/10.20375/0000-000B-C8EF-7>`__


Landing Page, Daten und Metadaten
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Weiterhin können Sie auch auf alle Metadaten und die Dateien selbst direkt referenzieren, indem Sie die EPIC Handles nutzen (mit DOIs ist das nicht möglich):

    :Einstiegsseite: `hdl.handle.net/21.11113/0000-000B-C8EF-7@landing <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@landing>`__

    :Datei: `hdl.handle.net/21.11113/0000-000B-C8EF-7@data <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@data>`__

    :Deskriptive Metadaten: `hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata>`__

    :Administrative Metadaten: `hdl.handle.net/21.11113/0000-000B-C8EF-7@adm <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm>`__

    :Technische Metadaten: `hdl.handle.net/21.11113/0000-000B-C8EF-7@tech <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@tech>`__

    :ZIP-Datei mit Daten und Metadaten (BagIt): `hdl.handle.net/21.11113/0000-000B-C8EF-7@bag <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@bag>`__


Weitere Zugriffsmöglichkeiten finden Sie dokumentiert in der API-Dokumentation unter `DH-crud <https://repository.de.dariah.eu/doc/services/submodules/tg-crud/service/dhcrud-webapp-public/docs/index.html#api-documentation>`__ und `Resolving and Identifiers <https://repository.de.dariah.eu/doc/services/resolving.html>`__.


Quellkode
---------

Siehe publikator_sources_


Bugtracking
-----------

Siehe publikator_bugtracking_


Lizenz
------

Siehe LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/-/blob/main/LICENSE.txt
.. _publikator_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator
.. _publikator_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/-/issues
