package eu.dariah.de.publikator.utils;

import java.io.File;
import java.io.FilenameFilter;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

public class I18NUtils {

	public static HashMap<String, String> i18n (Locale locale) {
		
		HashMap<String, String> messages = new HashMap<String, String>();
		ResourceBundle rb = ResourceBundle.getBundle("i18n.Language", locale, new UTF8Control());
		for(String key : rb.keySet()) {
			messages.put(key, rb.getString(key));			
		}
		
		return messages;
	}
	
	/**
	 * Helper method if you need a single translated key
	 * @param key
	 * @return translated string
	 */
	public static String getTranslatedString(String key, Locale locale) {
		return I18NUtils.i18n(locale).get(key);
	}	
	
	/**
	 * Helper method if you need a single translated key
	 * and the key has placeholders like
	 * {0} items have been added to your collection named '{1}'
	 * @param key
	 * @param locale
	 * @param params
	 * @return
	 */
	public static String getTranslatedString(String key, Locale locale, Object... params) {
		return MessageFormat.format(I18NUtils.i18n(locale).get(key), params);
	}
	
	// read resource bundle in utf8: http://stackoverflow.com/a/4660195
	private static class UTF8Control extends Control {
	    public ResourceBundle newBundle
	        (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
	            throws IllegalAccessException, InstantiationException, IOException
	    {
	        // The below is a copy of the default implementation.
	        String bundleName = toBundleName(baseName, locale);
	        String resourceName = toResourceName(bundleName, "properties");
	        ResourceBundle bundle = null;
	        InputStream stream = null;
	        if (reload) {
	            URL url = loader.getResource(resourceName);
	            if (url != null) {
	                URLConnection connection = url.openConnection();
	                if (connection != null) {
	                    connection.setUseCaches(false);
	                    stream = connection.getInputStream();
	                }
	            }
	        } else {
	            stream = loader.getResourceAsStream(resourceName);
	        }
	        if (stream != null) {
	            try {
	                // Only this line is changed to make it to read properties files as UTF-8.
	                bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
	            } finally {
	                stream.close();
	            }
	        }
	        return bundle;
	    }
	}
	
	/**
	 * Helper method to map 2 character language code to variants
	 * in Publikator we have both forms and the browser might just
	 * send EN. We need to select en_US in this case.
	 * If we have many variants from one language this method certainly
	 * is too simple, but likely we will not have any variants from one language
	 * availableLocales is the result from getAvailableLocalesInApp()
	 * 
	 * @param availableLocales
	 * @param currentlocale
	 * @return
	 */
	public static Locale mapISOCode(List<Locale> availableLocales, Locale currentlocale) {
		Locale selectedLocale = Locale.forLanguageTag("en-US");
		
		if (null != currentlocale.getLanguage() && null != currentlocale.getCountry()) {
			for (Locale availableLocale : availableLocales) {	
				if (availableLocale.getLanguage() == currentlocale.getLanguage() && availableLocale.getCountry() == currentlocale.getCountry()) {
					return availableLocale;
				}
			}
		}
		
		if (null != currentlocale.getLanguage()) {
			for (Locale availableLocale : availableLocales) {
				if (availableLocale.getLanguage() == currentlocale.getLanguage()) {
					return availableLocale;
				}
			}
		}
		
		return selectedLocale;
	}
	
	/**
	 * Returns the available locales deployed in the application
	 * There is no standard java way on getting this
	 * we have to loop through the resources directory manually
	 * @return 
	 */
	public static List<Locale> getAvailableLocalesInApp() {
		// found and adapted from here https://stackoverflow.com/questions/12072454/in-java-how-do-i-find-out-what-languages-i-have-available-my-resource-bundle
		List<Locale> locales = new ArrayList<Locale>();
		try {
			File resources = new File(I18NUtils.class.getResource("/i18n").toURI());
			final String bundlePrefix = "Language_";// Bundle name prefix.
			for (String resourceFilename : resources.list(new FilenameFilter() {
				// File name filter
				@Override
				public boolean accept(File dir, String name) {
					return name.startsWith(bundlePrefix);
				}
			})) {
				// the substring returns the part between the bundlePrefix and the . for the file extension				
				String language = resourceFilename.substring(bundlePrefix.length(), resourceFilename.indexOf('.'));
				
				Locale locale = null;		
				// for setting de_DE variant stuff
				if (language.length() == 5) {
					locale = new Locale(language.substring(0, 2), language.substring(3));
				// for DE, EN, US etc
				}else {
					locale = new Locale(language);
				}
				locales.add(locale);	
			}
		} catch (URISyntaxException ex) {
			throw new RuntimeException(ex);
		}
		return locales;
	}
	
	/**
	 * Gets the available  dcelements_LANG.ttl languages as List
	 * You need to supply the path to the TTL files (CONTEXT-Root/webapp/schema)
	 * @param path
	 * @return
	 */
	public static List<String> getAvailableDCElementsLocalesInApp(File path) {
		List<String> locales = new ArrayList<String>();
		final String dcprefix = "dcelements_";
		
		for (String resourceFilename : path.list(new FilenameFilter() {
			// File name filter
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(dcprefix);
			}
		})) {
			// the substring returns the part between the bundlePrefix and the . for the file extension				
			String language = resourceFilename.substring(dcprefix.length(), resourceFilename.indexOf('.'));
			locales.add(language);
			
		}			
		
		return locales;
	}
}
