package eu.dariah.de.publikator.backend.threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2018 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * 
 * Servlet Context Listener to start a threadpool for background tasks
 * 
 * @author Johannes
 */

@WebListener
public class ThreadpoolContextListener implements ServletContextListener {
	
	private static final Logger	LOG	= Logger.getLogger(ThreadpoolContextListener.class.getName());

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				10, 						// Initial threads
				100, 					// Maximum threads
				30000L,					// Keep alive time
				TimeUnit.MILLISECONDS, 	// time unit
				new ArrayBlockingQueue<Runnable>(100)); // Array Queue
		sce.getServletContext().setAttribute("executor", executor);
		
		LOG.info("Started ThreadPool for background tasks: initial threads "
				+ executor.getCorePoolSize() + 
				" maximum threads: " + executor.getMaximumPoolSize() +
				" keep alive time: " + executor.getKeepAliveTime(TimeUnit.SECONDS) +"s"
				);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

		ThreadPoolExecutor executor = (ThreadPoolExecutor) sce.getServletContext().getAttribute("executor");
		executor.shutdown();
		LOG.info("ThreadPool shutdown");

	}

}
