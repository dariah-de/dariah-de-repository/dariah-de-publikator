package eu.dariah.de.publikator.backend;

import java.io.InputStream;
import java.util.concurrent.Callable;
import javax.ws.rs.core.Response;
import eu.dariah.de.publikator.servicebeans.ClientsServiceBean;

/**
 * - #%L DARIAHDE :: Publikator Standalone %% Copyright (C) 2019 SUB Göttingen
 * (https://www.sub.uni-goettingen.de) %% Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 * 
 *         This class is a wrapper around the DariahStorageClient class. Basically each method call
 *         is translated into an inner class which implements the callable interface. This allows a
 *         retry mechanism with https://github.com/jhalterman/failsafe. Retry is not needed for
 *         checkAccess.
 * 
 */
public class DariahStorageClientWrapper {

  protected ClientsServiceBean clients = new ClientsServiceBean();

  /**
   * 
   */
  public DariahStorageClientWrapper() {}

  /**
   *
   */
  public class CreateFile implements Callable<String> {

    private InputStream is;
    private String mimetype;
    private String token;
    private String logID;

    /**
     * @param is
     * @param mimetype
     * @param pdpToken
     * @param logID
     */
    public CreateFile(InputStream is, String mimetype, String pdpToken, String logID) {
      this.is = is;
      this.mimetype = mimetype;
      this.token = pdpToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public String call() throws Exception {
      return DariahStorageClientWrapper.this.clients.getDariahStorageClient().createFile(this.is,
          this.mimetype, this.token, this.logID);
    }
  }

  /**
   *
   */
  public class DeleteFile implements Callable<Void> {

    private String id;
    private String token;
    private String logID;

    /**
     * @param id
     * @param pdpToken
     * @param logID
     */
    public DeleteFile(String id, String pdpToken, String logID) {
      this.id = id;
      this.token = pdpToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Void call() throws Exception {
      DariahStorageClientWrapper.this.clients.getDariahStorageClient().deleteFile(this.id,
          this.token, this.logID);
      return null; // void is correct in this usage
    }
  }

  /**
   *
   */
  public class ReadFile implements Callable<InputStream> {

    private String id;
    private String token;
    private String logID;

    /**
     * @param id
     * @param pdpToken
     * @param logID
     */
    public ReadFile(String id, String pdpToken, String logID) {
      super();
      this.id = id;
      this.token = pdpToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public InputStream call() throws Exception {
      return DariahStorageClientWrapper.this.clients.getDariahStorageClient().readFile(this.id,
          this.token, this.logID);
    }

  }

  /**
   *
   */
  public class ReadResponse implements Callable<Response> {

    private String id;
    private String token;
    private String logID;

    /**
     * @param id
     * @param pdpToken
     * @param logID
     */
    public ReadResponse(String id, String pdpToken, String logID) {
      super();
      this.id = id;
      this.token = pdpToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Response call() throws Exception {
      return DariahStorageClientWrapper.this.clients.getDariahStorageClient().readResponse(this.id,
          this.token, this.logID);
    }

  }

  /**
  *
  */
  public class UpdateFile implements Callable<String> {

    String id;
    InputStream is;
    String mimeType;
    private String token;
    private String logID;

    /**
     * @param id
     * @param is
     * @param mimeType
     * @param pdpToken
     * @param logID
     */
    public UpdateFile(String id, InputStream is, String mimeType, String pdpToken, String logID) {
      super();
      this.id = id;
      this.is = is;
      this.mimeType = mimeType;
      this.token = pdpToken;
      this.logID = logID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public String call() throws Exception {
      return DariahStorageClientWrapper.this.clients.getDariahStorageClient().updateFile(this.id,
          this.is, this.mimeType, this.token, this.logID);
    }

  }

}
