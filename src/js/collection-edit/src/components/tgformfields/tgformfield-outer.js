/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './tgformfield-outer.html';

import filter from 'lodash/filter';
import size from 'lodash/size';
import findLast from 'lodash/findLast';

const _ = {
  filter,
  size,
  findLast,
}

let tgformfieldOuter = Ractive.extend({
    template: template,

    onrender: function () {
      this.on('add', (event, index) => {
        let copy = Object.assign({}, this.get('schema')[index]);
        copy.fieldvalue = '';
        // just make sure this field shows up after beeing added
        copy.tgforms_mandatory = true;
        this.get('schema').splice(index + 1, 0, copy);
        this.pubsub.publish('data-changed');
      });

      this.on('remove', (event, index) => {
        this.parent.get('schema').splice(index, 1);
        this.pubsub.publish('data-changed', {uri: this.get('uri')});
      });

      this.on('set-for-members', (event, recursive) => {
        console.log('rec is:', recursive)
        const fieldentry = this.get('fieldvalue');
        const rdf_Property = this.get('rdf_Property');
        this.dataModelUtils.setChildDataField(this.get('uri'), rdf_Property, fieldentry, recursive);
        this.set('showSetForMembersDialog', false);
        this.set('showSetForMembersSuccess', true);
        this.pubsub.publish('data-changed', {uri: this.get('uri')});
      });
    },

    computed: {
      sumSameFields: function() {
        let sameFields = _.filter(this.get('schema'), { 'rdf_Property': this.get('rdf_Property') } );
        return _.size(sameFields);
      },
      isMandatoryOrActive: function() {
        return this.get('tgforms_mandatory') || this.get('fieldvalue') !== "" || this.get('showNonMandatoryFields') || this.get('tgforms_niceToHave');
      },
      isEmptyField: function() {
        return this.get('fieldvalue') === '';
      },
      isMandatoryAndEmpty: function() {
        if(this.get('tgforms_mandatory') && this.get('fieldvalue') === "") {
          // field is mandatory and empty, so we need to check if there are more than one of this kind
          const sameFields = _.filter(this.get('schema'), { 'rdf_Property': this.get('rdf_Property') } );
          const sameFieldSize = _.size(sameFields);
          if(sameFieldSize > 1) {
            // more than one field, check if all are empty
            return _.size(_.filter(sameFields, { 'fieldvalue': ''})) === sameFieldSize;
          } else {
            // only one field, its empty!
            return true;
          }
        } else {
          // field is not mandatory, or not empty
          return false;
        }
      },
    }
});

export default tgformfieldOuter;
