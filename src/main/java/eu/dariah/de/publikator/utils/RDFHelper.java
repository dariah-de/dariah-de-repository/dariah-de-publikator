package eu.dariah.de.publikator.utils;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.core.Response;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import eu.dariah.de.publikator.backend.DariahStorageClientWrapper;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

/**
 *
 */
public class RDFHelper {

  // cmp: http://stackoverflow.com/a/16016415
  /**
   * @param res
   * @param prop
   * @return
   */
  public static Set<RDFNode> listProperties(Resource res, Property prop) {

    Set<RDFNode> set = new HashSet<RDFNode>();
    for (StmtIterator i = res.listProperties(prop); i.hasNext();) {
      set.add(i.nextStatement().getObject());
    }

    return set;
  }

  /**
   * @param res
   * @param prop
   * @return
   */
  public static Set<String> listPropertyStrings(Resource res, Property prop) {

    Set<String> set = new HashSet<String>();
    for (StmtIterator i = res.listProperties(prop); i.hasNext();) {
      set.add(i.nextStatement().getObject().toString());
    }

    return set;
  }

  /**
   * @param data
   * @param storageId
   * @return
   */
  public static String dcTitleFromInputStream(InputStream data, String storageId) {

    String title = "New Collection " + storageId;

    Model model = ModelFactory.createDefaultModel();
    model.read(data, null, "TURTLE");
    String dcNamespace = model.getNsPrefixURI(RDFConstants.DC_PREFIX);
    String storageUrl = model.getNsPrefixURI(RDFConstants.DARIAHSTORAGE_PREFIX);
    Resource res = model.getResource(storageUrl + storageId);
    Property p = model.createProperty(dcNamespace, "title");

    StmtIterator iter = res.listProperties(p);
    if (iter.hasNext()) {
      title = iter.next().getObject().toString();
    }

    return title;
  }

  /**
   * Counts the items within a (root) from a model that has been read before It will return an int
   * array [0] the number of collections [1] the number of data objects (files) Note: number of
   * collections will be 0 if you have just created a new collection and did not yet insert a title.
   * The RDF just looks like
   * 
   * @prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .
   * @prefix dariahstorage: <https://de.dariah.eu/storage/> . You have to take if you display 0 or 1
   *         in that case
   * @param model the whole hena model needs to supplied
   * @return
   * @throws Exception
   */
  public static int[] countCollectionItems(Model model, String logID) throws Exception {
    return countCollectionItems(null, null, false, model, null, logID);
  }

  /**
   * Counts the items within a (root) collection. It will return an int array [0] the number of
   * collections [1] the number of data objects (files) Note: number of collections will be 0 if you
   * have just created a new collection and did not yet insert a title. The RDF just looks like
   * 
   * @prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .
   * @prefix dariahstorage: <https://de.dariah.eu/storage/> . You have to take if you display 0 or 1
   *         in that case
   * @param storageClient initialized instance of DariahStorageClientWrapper
   * @param collectionId the collection id to perform the count supply the whole RDF model
   * @return
   * @throws Exception
   */
  public static int[] countCollectionItems(DariahStorageClientWrapper storageClient,
      String collectionId, String pdpToken, String logID) throws Exception {
    return countCollectionItems(storageClient, collectionId, true, null, pdpToken, logID);
  }


  private static int[] countCollectionItems(DariahStorageClientWrapper storageClient,
      String collectionId, boolean readModel, Model model, String pdpToken, String logID)
      throws Exception {

    int[] counter = {0, 0};

    // check if we need to fetch the model before
    if (readModel == true) {
      Model wholeModel = ModelFactory.createDefaultModel();
      // get the whole model
      readWholeCollectionAsModel(wholeModel, storageClient, collectionId, pdpToken, logID);
      model = wholeModel;
    }

    Resource dataObjects = model.getResource(RDFConstants.DARIAH_DATAOBJECT);
    Resource collections = model.getResource(RDFConstants.DARIAH_COLLECTION);

    int countDataObjects = 0;
    int countCollections = 0;

    // Just loop around the objects and count, one loop for data objects, the other for collections
    StmtIterator iter = model.listStatements(null, null, dataObjects);
    while (iter.hasNext()) {
      iter.nextStatement();
      countDataObjects++;
    }

    iter = model.listStatements(null, null, collections);
    while (iter.hasNext()) {
      iter.nextStatement();
      countCollections++;
    }

    counter[0] = countCollections;
    counter[1] = countDataObjects;

    return counter;
  }

  /**
   * Adapted code from
   * de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish.ReadCollectionModels.readCollectionModels(Model,
   * String, String, PublishObject, String, String) This is a recursive method to load the
   * collection as single model, including all sub-collections.
   * 
   * @param theModel
   * @param storageClient
   * @param collectionId
   */
  public static void readWholeCollectionAsModel(Model theModel,
      DariahStorageClientWrapper storageClient, String collectionId, String pdpToken, String logID)
      throws Exception {

    String storageId = getStorageId(collectionId);
    Response storageResponse = null;

    // set the retries, we catch any exception here
    // in future we can be more specific or exit early
    // if there is a non fixable state


    RetryPolicy retryPolicy = new RetryPolicy()
        .withDelay(50, TimeUnit.MILLISECONDS)
        .withMaxRetries(3)
        .retryOn(Exception.class);

    DariahStorageClientWrapper.ReadResponse rrs =
        storageClient.new ReadResponse(storageId, pdpToken, logID);

    storageResponse = Failsafe.with(retryPolicy).get(() -> rrs.call());

    InputStream data = storageResponse.readEntity(InputStream.class);

    theModel.read(data, "", RDFConstants.TURTLE);
    // Close stream.
    try {
      data.close();
    } catch (IOException e) {
      // Ignore
    }

    // Get objects contained in collection (hasPart).


    Resource res =
        theModel
            .getResource(theModel.getNsPrefixURI(RDFConstants.DARIAHSTORAGE_PREFIX) + storageId);
    Property prop = theModel.createProperty(
        theModel.getNsPrefixURI(RDFConstants.DCTERMS_PREFIX), "hasPart");

    // Loop all the contained objects.
    StmtIterator iter = res.listProperties(prop);

    // Check for containing elements, if none add error.
    if (!iter.hasNext()) {
      return;
    }

    // If elements are existing, do...
    while (iter.hasNext()) {

      Statement s = iter.next();

      List<String> l = RDFUtils.getRDFList(s.getObject());
      for (String str : l) {
        // ...check collection metadata, if type is collection.
        if (RDFUtils.isCollection(theModel, str)) {
          readWholeCollectionAsModel(theModel, storageClient, str, pdpToken, logID);
        }

      }

    }

    return;
  }

  public static Set<String> getAllStorageIdsFromACollection(
      DariahStorageClientWrapper storageClient,
      String collectionId, String pdpToken, String logID) throws Exception {
    Set<String> ids = new HashSet<String>();

    // Read the whole model as RDF
    Model model = ModelFactory.createDefaultModel();
    readWholeCollectionAsModel(model, storageClient, collectionId, pdpToken, logID);

    // Now we need to list all subjects
    ResIterator iter = model.listSubjects();
    while (iter.hasNext()) {
      Resource res = iter.next();
      // ignore blank nodes
      if (!res.isAnon()) {
        String id = getStorageId(res.getURI());
        ids.add(id);
      }
    }

    return ids;
  }

  public static String getStorageId(final String theId) {
    if ((theId.startsWith("http://") || theId.startsWith("https://"))
        && theId.lastIndexOf("/") != -1) {
      return theId.substring(theId.lastIndexOf("/") + 1);
    } else {
      return theId;
    }
  }

}
