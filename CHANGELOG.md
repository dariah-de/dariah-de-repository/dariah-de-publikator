## [5.4.3](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/compare/v5.4.2...v5.4.3) (2023-08-03)


### Bug Fixes

* disable cookies im matomo ([65b8463](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/65b8463cee2230da2f0ce4a253b34849af9589f3))
* fix matomo einrückungen ([6d16010](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/6d16010ba70b302866c1d87bbfb49d1e9b74c3b5))

## [5.4.2](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/compare/v5.4.1...v5.4.2) (2023-06-08)


### Bug Fixes

* add JSON simple to dependencies ([41422ad](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/41422ad297b061cc3512e9d5549aa9afb5d6eeb8))
* fix doc ([f416dd1](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/f416dd17d9b47121fa13df9765864bcba8bb0c53))
* more doc fixes ([72b4b20](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/72b4b20c24411a00b9e0ff798ae0104eb82f4f3f))
* remove REGISTERED flag from publikator code ([caa7c6f](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/caa7c6f151d88fb6f9ba9bc84e8880b0fcb54332))
* remove warnings from JSP files, add needed deps ([9313498](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/931349891d03854643cec02212b4b79f085d8736))

## [5.4.1](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/compare/v5.4.0...v5.4.1) (2023-03-16)


### Bug Fixes

* adapt english doc ([80e9b0f](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/80e9b0f9452c2a6f66dc34117503f2f368d16ea1))
* add more dc simple doc ([81d5ade](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/81d5ade7de973428b5a732df4d014f9995142958))
* add more metadata doc ([ff17dbe](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/ff17dbe22a676bea488ebf61797405c7e4765cec))
* and still more doc ([b09a221](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/b09a2215a289cc8e6bd0d72c2bda946705da99e5))
* fix docs ([11371f1](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/11371f1078ab99228699e1c9d91a35b50d328ab0))
* hrgs, reformat doc ([2380a90](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/2380a907cd68b97c56b6337e31bb068ac23b6a54))
* merging ([e666ccb](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/e666ccbc1aa973066747840cab9c6d56742abd05))
* more doc fixes, better test doc locally in the future! :-D ([6c8fd8d](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/6c8fd8d66467c628380ca6e0c15d8d0c6e4b8c14))

# [5.4.0](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/compare/v5.3.0...v5.4.0) (2023-03-07)


### Features

* local development environment ([fb66a77](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/fb66a7740bd6a78d62d8afde98b4f2232d34e23d))
* remove aptly upload ([4d5b810](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/4d5b81089de356114bdf43483549a45b9b6fd3e3))

# [5.3.0](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/compare/v5.2.2...v5.3.0) (2022-12-09)


### Bug Fixes

* fix gitlab ci packaging ([3f9f10a](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/3f9f10a6fb3a6ac0f376a6fed29d2de84e789149))
* fix pom dependency issues ([fcb5edc](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/fcb5edc6ec141ad11fb4268a05cc1c53746a589b))
* increase pom dependency versions ([79eed6b](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/79eed6bc558f642226ef6e716cd97a53b40fd6bd))
* increase version ([2d4e529](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/2d4e529e0dac3a9c9200a6af6611a3bbcdd7a048))


### Features

* deploy new gitlab ci deployment, finally ([14f3421](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/14f342183c1016ced0e7c3ca06e8b1a3053b8291))
* I WANT A NEW RELEASE! NOW!! ([4f5adcd](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/4f5adcd9e435438362e5600fea01058af8692ec3))
* increase SNAPSHOT pom version ([4a7a4c7](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/4a7a4c776c3587a43e4cd1f7f9bee44eec7e77b3))
* resolve "Add new gitlab ci workflow" ([25c209a](https://gitlab.gwdg.de/dariah-de/dariah-de-repository/dariah-de-publikator/commit/25c209a5f18202fb2643d1c0e28065455efd89e6))

## 5.2.1
- last tag before new gitlab ci workflow deployment

## 5.2.2-SNAPSHOT
- Move code and issues to GWDG Gitlab

## 5.1.0-SNAPSHOT
- Adapt to new common modules

## 4.1.2-SNAPSHOT
- Retire status "registered", status "published" now means, collection's description is published automatically to new CR
- Add and update buttons: "show in CR" --> "edit in CR", "show in rep" goes to new URL (DHREP GS), new: "go to landing page"

## 4.1.1-SNAPSHOT (2021-05-06)
- Fix some typos in description and documentation texts
- Add HTML references to information description skos:note
- Fix some minor syntax issues

## 3.2.0-SNAPSHOT
- Remove publish client API publishSecret param due to new publish service API version

## 3.0.1-SNAPSHOT
- Added storage token support for (no publish token anymore!) to status methods
