package eu.dariah.de.publikator.backend;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class SessionFilter
 */
@WebFilter(filterName = "SessionFilter",
		urlPatterns = {"/*"},
		initParams = {
					// to allow more urls without login, add them to this regex
					@WebInitParam(name = "urlWhitelist", value = "^(/(css|js|img|bootstrap|fonts|schema)/.*)|/(login|welcome|mainView\\?view=language.*)|(/)$")
		}
)
public class SessionFilter implements Filter {

	private Pattern urlWhitelist;	
	
    /**
     * Default constructor. 
     */
    public SessionFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * Check if a HTTP Session exist, if yes then allow the request
	 * if not, redirect to the login page 
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		String url = ((HttpServletRequest) request).getRequestURI();
		// we need to remove the servlet path to get the relative URL
		String dir = ((HttpServletRequest) request).getContextPath();
		if (dir.length() > 0) {
			// remove the path (e.g. /publikator/abc 
			url = url.substring(dir.length());
		}
		
		if (((HttpServletRequest) request).getQueryString() !=null) {
			// append the query string if available
			url += "?"+((HttpServletRequest) request).getQueryString();
		}
			
		// get session, but do not create a new one
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		
		if (null == session) {
			// session is null, check if the request wants to fetch an URL which is NOT allowed without login
			if (!isAllowed(url)) {
				((HttpServletResponse) response).sendRedirect("login");
				return;		
			}	
		}else {
			// session exists
			// now check if the authenticated attribute has been set to the session 
			if (null == session.getAttribute("authenticated") ) {
				// not authenticated, check if the URL is allowed without login
				if (!isAllowed(url)) {	
					((HttpServletResponse) response).sendRedirect("login");
					return;	
				}
			}
		}
		// still here? then we have an valid request, either because of login or
		// the url is allowed without login
				
		chain.doFilter(request, response);
	}

	/**
	 * Parse the init params and populate the allowed URLs without login
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		String pattern = fConfig.getInitParameter("urlWhitelist");
	
		this.urlWhitelist = Pattern.compile(pattern);
	}
	
	private boolean isAllowed(String url) {
		 Matcher m = urlWhitelist.matcher(url);
	     return m.matches();
		
	}

}
