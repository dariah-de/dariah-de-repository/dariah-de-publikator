package eu.dariah.de.publikator.backend.threadpool;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.timgroup.statsd.StatsDClient;

import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.backend.DariahStorageClientWrapper;
import eu.dariah.de.publikator.backend.StatsdClient;
import eu.dariah.de.publikator.dao.CollectionDAO;
import eu.dariah.de.publikator.dao.DAOFactory;
import eu.dariah.de.publikator.servicebeans.SeafileClient;
import eu.dariah.de.publikator.utils.RDFHelper;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

/**
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2018 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * 
 * Runnable implementation to delete files from the storage in background
 * 
 * @author Johannes
 */
public class DeleteCollectionWorker implements Runnable {
	
	private String collectionId;
	private String eppn;
	private boolean removeFromList;
	private String pdpToken; 
	private String logID;
	
	private static final Logger	LOG	= Logger.getLogger(DeleteCollectionWorker.class.getName()+"-"+Thread.currentThread().getName());
	
	/**
	 * This removes a whole collection including all items (data objects,
	 * subcollections etc.)
	 * 
	 * @param collectionId
	 *            the maincollection or subcollection if
	 * @param eppn
	 *            the eppn from the user, makes only sense if
	 *            removeFromList=true
	 * @param removeFromList
	 *            if true, the collection will be removed from the index page
	 */	
	public DeleteCollectionWorker(String collectionId, 
			String eppn, 
			boolean removeFromList, 
			String pdpToken, 
			String logID) {
		this.collectionId = collectionId;
		this.eppn = eppn;
		this.removeFromList = removeFromList;
		this.pdpToken = pdpToken;
		this.logID = logID;
	}

	@Override
	public void run() {
		
		DariahStorageClientWrapper storageClient = new DariahStorageClientWrapper();

		// remove the collection from our local publikator store, too.
		// But only if removeFromList=true
		if (true == removeFromList) {
			CollectionDAO col = DAOFactory.getDAOFactory(DAOFactory.REDIS)
					.getCollectionDAO();
			col.deleteCollection(eppn, collectionId);
		}		
		
		try {
			Set<String> containedCollectionIds = RDFHelper
					.getAllStorageIdsFromACollection(storageClient,
							collectionId, pdpToken, logID);
			Iterator<String> it = containedCollectionIds.iterator();

			while (it.hasNext()) {
				String fileId = it.next();
				if (!fileId.startsWith(SeafileClient.SEAFILE_URL)) {

					LOG.info(logID + " About to delete storageId: " + fileId);
					// set the retries, we catch any exception here
					// in future we can be more specific or exit early
					// if there is a non fixable state
					RetryPolicy retryPolicy = new RetryPolicy()
							.withDelay(100, TimeUnit.MILLISECONDS)
							.withMaxRetries(3).retryOn(Exception.class);

					DariahStorageClientWrapper.DeleteFile df = storageClient.new DeleteFile(
							fileId, pdpToken, logID);

					Failsafe.with(retryPolicy).run(() -> df.call());

				}
			}
		} catch (IOException e) {
			LOG.severe(logID + " error deleting part of file: " + collectionId
					+ " - reason: " + e.getMessage());

			if (Configuration.getInstance().getConfig().isStatsdEnable()) {
				StatsDClient client = StatsdClient.getInstance().getClient();
				client.incrementCounter("storage.delete.failure");
			}
		} catch (Exception e) {
			LOG.severe(logID + " error in loading the collection model "
					+ collectionId + " - reason: " + e.getMessage());
		}

	}

}
