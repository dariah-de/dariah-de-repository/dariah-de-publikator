/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './view-progress-alert.html';

let ViewProgressAlert = Ractive.extend({
  template: template,

  onrender: function() {
    this.on( 'chancelPublishProcess', function (event) {
      this.set('publishInProgress', false);
      this.set('viewCancelAlert', true);
    });
    if (!this.parent.publishStatusRunning) {
    		// query the publish status only if no other process is running
    		this.parent.publishStatusRunning = true;
        this.parent.updateProgress();
    }
  }
});

export default ViewProgressAlert;
