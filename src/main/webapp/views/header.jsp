<%@ page contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/tld/escape.tld" prefix="esc" %>

<!DOCTYPE html>
<html lang="${locale.language}">
<head>
	<link rel="icon" type="image/x-icon" href="https://res.de.dariah.eu/styleguide/2.0/docs/assets/favicon.ico">
	<link rel="stylesheet" href="https://res.de.dariah.eu/styleguide/2.0/docs/assets/styles.css">
    <link rel="stylesheet" href="https://dariah-de.github.io/status/dariah/embed.css">
	<meta charset="utf-8">
	<meta name="description" content="DARIAH-DE Publikator – ${i18n['collection-overview-description']}">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<script src="https://res.de.dariah.eu/dhrep/js/jquery.min.js"></script>
	<script src="https://res.de.dariah.eu/styleguide/2.0/docs/assets/script.js" defer></script>
	<!-- CSS Imports -->
	<link rel="stylesheet" href="https://res.de.dariah.eu/publikator/css/bootstrap.css?${applicationScope.buildTimestamp}" type="text/css" media="screen">
	<link rel="stylesheet" href="https://res.de.dariah.eu/publikator/css/bootstrap-responsive.css?${applicationScope.buildTimestamp}" type="text/css" media="screen">
	<link rel="stylesheet" href="https://res.de.dariah.eu/publikator/css/application.css?${applicationScope.buildTimestamp}" type="text/css" media="screen">
	<link rel="stylesheet" href="https://res.de.dariah.eu/publikator/css/bootstrap-customization.css?${applicationScope.buildTimestamp}" type="text/css" media="screen">
	<link rel="stylesheet" href="https://res.de.dariah.eu/publikator/css/bootstrap-modal.css?${applicationScope.buildTimestamp}" type="text/css" media="screen">
	<link rel="stylesheet" href="https://res.de.dariah.eu/publikator/css/font-awesome.css?${applicationScope.buildTimestamp}">
	<link rel="stylesheet" href="css/main.css?${applicationScope.buildTimestamp}">
	<script src="https://res.de.dariah.eu/dhrep/js/bootstrap.js?${applicationScope.buildTimestamp}"></script>
	<script src="./js/bootstrap-session-timeout.min.js?${applicationScope.buildTimestamp}"></script>
	<script src="https://res.de.dariah.eu/styleguide/2.0/docs/assets/globalmenu.js"></script>
    <script src="https://res.de.dariah.eu/dhrep/js/clipboard-2.0.0.min.js"></script>
    <script>
    // Please test:
    // fetch('https://raw.githubusercontent.com/DARIAH-DE/status/master/dariah/test/embed.html')
    fetch('https://dariah-de.github.io/status/dariah/embed.html')
        .then(function (response) {
    	return response.text();
    }).then(function (data) {
        var domParser = new DOMParser();
        var errorHtml = domParser.parseFromString(data, 'text/html')
            .querySelector('.dariah-repository_status.dariah-status-message-error');
        var warningHtml = domParser.parseFromString(data, 'text/html')
            .querySelector('.dariah-repository_status.dariah-status-message-warning');
        if (errorHtml) {
            $('#dariah-status-message-text').append(errorHtml);
            $('#dariah-status-message-area').removeClass('hide');
        }
        if (warningHtml) {
            $('#dariah-status-message-text').append(warningHtml);
            $('#dariah-status-message-area').removeClass('hide');
        }
    }).catch(function (err) {
    	console.warn('ERROR fetching DARIAH-DE status!', err);
    });
    </script>
    <style>a.no_underline{border-bottom:0px;}a.copy_krams{border-bottom:0px;}</style>
    <c:choose>
    <c:when test="${(applicationScope.instanceName eq 'TEST')}">
    <style>.header{background:${(applicationScope.menuHeaderColor)}!important}</style>
    <title>${i18n['publikator-test']}</title>
    </c:when>
    <c:when test="${(applicationScope.instanceName eq 'DEVELOP')}">
    <style>.header{background:${(applicationScope.menuHeaderColor)}!important}</style>
    <title>${i18n['publikator-dev']}</title>
    </c:when>
    <c:otherwise>
    <title>${i18n['publikator']}</title>
    </c:otherwise>
    </c:choose>
</head>
<body class="site">
	<div class="site_wrap">
		<header class="header">
			<div class="wrap">
				<nav class="nav">
					<button type="button" class="nav_toggle -main">
						<svg class="icon">
							<use xlink:href="#icon-menu"></use>
						</svg>
						${i18n['header-menu']}
					</button>
					<div class="nav_menu -main">
						<ul class="nav_list -level-1">
							<li class="nav_item -level-1 -home">
                                <a class="nav_link" href="${pageContext.request.contextPath}" title="${i18n['publikator']}">
							        <svg class="icon">
								        <use xlink:href="#icon-home"></use>
								    </svg>
                                    <span class="sr-only">${i18n['publikator']}</span>
								</a>
							</li>
<!--
							<li class="nav_item -level-1">
                                <c:choose>
                                    <c:when test="${(applicationScope.instanceName eq 'TEST')}">
                                        <span class="app_title">${i18n['publikator-test']}</span>
                                    </c:when>
                                    <c:when test="${(applicationScope.instanceName eq 'DEVELOP')}">
                                        <span class="app_title">${i18n['publikator-dev']}</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="app_title">${i18n['publikator']}</span>
                                    </c:otherwise>
                                </c:choose>
							</li>
-->
							<li class="nav_item -level-1 -has-children">
								<a class="nav_link" href="javascript:">${i18n['collection-overview-more']}
									<svg class="icon">
										<use xlink:href="#icon-angle-down"></use>
									</svg>
								</a>
								<ul class="nav_list -level-2">
									<li class="nav_item -level-2">
										<a href="" class="nav_link">
											${i18n['landing-documentation']}
										</a>
										<ul class="nav_list -level-3">
											<li class="nav_item -level-3">
												<a href="${linkToDocumentation}" class="nav_link">
													${i18n['collection-overview-documentation']}
												</a>
											</li>
											<li class="nav_item -level-3">
												<a href="${linkToFAQ}" class="nav_link">
													${i18n['collection-overview-faq']}
												</a>
											</li>
											<li class="nav_item -level-3">
												<a href="${linkToAPIDoc}" class="nav_link">
													${i18n['collection-overview-apidoc']}
												</a>
											</li>
										</ul>
									</li>
									<li class="nav_item -level-2">
										<a class="nav_link" href="">
											${i18n['footer-contact']}
										</a>
										<ul class="nav_list -level-3">
											<li class="nav_item -level-3">
												<a href="mailto:${mailOfContact}?subject=${i18n['collection-overview-bug']}" class="nav_link">
													${i18n['collection-overview-support']}
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
                            <li class="nav_item -level-1">
                                <a class="nav_link" href="${applicationScope.genericSearchUrl}" title="${i18n['repository-search']}">${i18n['repository-search']}</a>
                            </li>
<!--
                            <li class="nav_item -level-1">
                                <a class="nav_link" href="${applicationScope.collectionRegistryUrl}" title="DHREP Collection Registry">Collection Registry</a>
                            </li>
-->
						</ul>
					</div>
				</nav>
				<aside class="header_aside">
					<div class="language">
						<button type="button" class="language_toggle" title="${i18n['header-language-change']}">
						<c:forEach var="locale" items="${applicationScope.availableLocales}">
 							<c:if test="${(locale == requestScope.locale) or (locale.language == requestScope.locale)}">
 								<span class="language_label -large">
								<svg class="icon">
									<use xlink:href="#icon-world"></use>
								</svg>
								 ${locale.getDisplayLanguage(requestScope.locale)}
								</span>
								<span class="language_label -small">${locale.language}</span>
 							</c:if>
						</c:forEach>
							<span class="sr-only">${i18n['header-language-change']}</span>
						</button>
						<ul class="language_list">
							<li class="language_item">
							<c:forEach var="locale" items="${applicationScope.availableLocales}">
								<c:choose>
 									<c:when test="${fn:length(locale.country) gt 0}">
										<a class="language_link" href="mainView?view=language&amp;locale=${locale.language}&amp;country=${locale.country}">
									 	${locale.getDisplayLanguage(requestScope.locale)}</a>
   									</c:when>
  									<c:otherwise>
										<a class="language_link" href="mainView?view=language&amp;locale=${locale.language}">
										${locale.getDisplayLanguage(requestScope.locale)}</a>
  									</c:otherwise>
								</c:choose>
							 </c:forEach>
							</li>
						</ul>
					</div>
					<c:if test="${sessionScope.authenticated}">
						<div class="account">
							<span class="account_toggle">
							<span class="account_label -large">${sessionScope.userObject.displayname}</span>
							</span>
						</div>
					</c:if>
					<c:choose>
   						<c:when test="${sessionScope.authenticated}">
 							<div class="account">
								<a class="account_toggle -logged-in" href="login?view=logout" title="${i18n['header-logout']}">
									<svg class="icon">
										<use xlink:href="#icon-logout"></use>
									</svg>
									<span class="sr-only">${i18n['header-logout']}</span>
								</a>
							</div>
   						</c:when>
   						<c:otherwise>
							<div class="account">
								<a class="account_toggle -logged-out" href="login" title="${i18n['header-login']}">
									<svg class="icon">
										<use xlink:href="#icon-import"></use>
									</svg>
									<span class="sr-only">${i18n['header-login']}</span>
								</a>
							</div>
   						</c:otherwise>
					</c:choose>
					<div class="search hidden">
						<a class="search_toggle" href="javascript:" title="Search">
							<svg class="icon">
								<use xlink:href="#icon-search"></use>
							</svg>
							<span class="sr-only">Search</span>
						</a>
						<form class="search_form" action="search" role="search">
							<label for="overlay-search-input" class="sr-only">Search term</label>
							<input type="text" id="overlay-search-input" class="search_input" name="q" maxlength="99" placeholder="Search term">
							<button type="submit" class="search_submit">
								<svg class="icon">
									<use xlink:href="#icon-angle-right"></use>
								</svg>
								<span class="sr-only">Search</span>
							</button>
						</form>
					</div>
				</aside>
				<nav class="nav -right">
					<button type="button" class="nav_toggle -portal">
						<img class="nav_logo -large" src="https://res.de.dariah.eu/styleguide/2.0/docs/assets/logos/dariah-logo-white.svg" alt="${i18n['dariahde']}">
						<img class="nav_logo -small" src="https://res.de.dariah.eu/styleguide/2.0/docs/assets/logos/dariah-logo-white-small.svg" alt="${i18n['dariahde']}">
						<svg class="icon">
							<use xlink:href="#icon-angle-down"></use>
						</svg>
					</button>
					<div class="nav_menu -portal">
						<ul id="home_dropdown_menu" class="nav_list -level-2 -portal"></ul>
					</div>
				</nav>
			</div>
		</header>
		<main id="publikator">
