package eu.dariah.de.publikator.dao;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
import java.util.List;

import eu.dariah.de.publikator.beans.Collection;

public interface CollectionDAO {

	/**
	 * Returns all available collections for a specific user
	 * @param userId
	 * @return
	 */
	public List<Collection> getCollectionsByUserId(String userId);
	
	/**
	 * Deletes a specific collection
	 * @param collectionId
	 * @return
	 */
	public boolean deleteCollection(String userId, String collectionId);
	
	/**
	 * Deletes all collections for a specified user
	 * @param userId
	 * @return
	 */
	public boolean deleteAllCollectionsByUserId(String userId);
	
	/**
	 * Saves a collection
	 * @param collection
	 * @return
	 */
	public boolean saveCollection(Collection collection);
	
	/**
	 * Save a collection for caching
	 * @param collection
	 * @return
	 */
	public boolean saveCollectionCache(Collection collection);
	
	/**
	 * Loads a collection object from cache
	 * @param collectionId
	 * @return
	 */
	public Collection loadCollectionFromCache(String collectionId);
	
	/**
	 * Removes a collection from cache (e.g. because data is outdated)
	 * @param collectionId
	 * @return
	 */
	public boolean deleteCollectionFromCache(String collectionId);	
}
