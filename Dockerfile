###
# build
###
FROM maven:3.8.3-jdk-8 as builder

ARG JOLOKIA_VERSION="1.3.7" # jolokia as of 1.5.0 requires auth for proxy operation

# download and explode jolokia
RUN curl -XGET "https://repo1.maven.org/maven2/org/jolokia/jolokia-war/${JOLOKIA_VERSION}/jolokia-war-${JOLOKIA_VERSION}.war" --output /jolokia.war
RUN mkdir /jolokia && cd /jolokia && jar -xf ../jolokia.war

COPY . /build
WORKDIR /build

# build and assemble app
RUN --mount=type=cache,target=/root/.m2 mvn clean validate package -Pjs-notminified

###
# assemble image
###
FROM tomcat:8.5-jre8

ENV JAVA_OPTS="-Xmx768m"
ENV CATALINA_OPTS="-Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

COPY Dockerfile /

RUN addgroup --system --gid 1015 tomcat-publikator
RUN adduser --system --uid 1015 --gid 1015 tomcat-publikator

COPY --from=builder /jolokia /usr/local/tomcat/webapps/jolokia
COPY --from=builder /build/target/publikator /usr/local/tomcat/webapps/publikator

RUN chown -R tomcat-publikator:tomcat-publikator /usr/local/tomcat/webapps/

USER tomcat-publikator
WORKDIR /usr/local/tomcat/

