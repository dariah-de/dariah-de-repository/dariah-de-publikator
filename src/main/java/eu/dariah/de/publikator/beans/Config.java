package eu.dariah.de.publikator.beans;

/**
 * - #%L DARIAHDE :: Publikator Standalone
 *
 * %% Copyright (C) 2020 SUB Göttingen (https://www.sub.uni-goettingen.de) %%
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 *
 * @author Johannes
 */
public class Config {

  private static final String LINE = "-----------------------------------------------------";

  private String instanceName;
  private String menuHeaderColor;
  private boolean enableAAI;
  private String logoutAAI;
  private boolean subcollectionsEnabled;
  private String dariahStorageUrl;
  private String dhpublishUrl;
  private String dhcrudUrl;
  private String collectionRegistryUrl;
  private String genericSearchUrl;
  private String pidServiceUrl;
  private String pdpTokenServerUrl;
  private String pdpTokenManagerUrl;
  private int refresh;
  private int serviceTimeout;
  private int maxUploadSize;
  private boolean seafileEnabled;
  private String seafileTokenSecret;
  private String seafileUrl;
  private boolean SSLCertVerification;
  private boolean skipPublishStatusCheck;
  private boolean dryrun;
  private boolean debug;
  private boolean overrideEppn;
  private String eppn;
  private String localPassword;
  private String localToken;
  private boolean skipLandingPage;
  private String linkToDocumentation;
  private String linkToFAQ;
  private String linkToAPIDoc;
  private String linkToPrivPol;
  private String mailOfContact;
  private String redisHostname;
  private int redisPort;
  private int redisMaxParallel;
  private String badgeText;
  private boolean statsdEnable;
  private String statsdHostname;
  private int statsdPort;

  public String getInstanceName() {
    return this.instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  public String getMenuHeaderColor() {
    return this.menuHeaderColor;
  }

  public void setMenuHeaderColor(String menuHeaderColor) {
    this.menuHeaderColor = menuHeaderColor;
  }

  public boolean isEnableAAI() {
    return this.enableAAI;
  }

  public boolean isSubcollectionsEnabled() {
    return this.subcollectionsEnabled;
  }

  public String getDariahStorageUrl() {
    return this.dariahStorageUrl;
  }

  public String getDhpublishUrl() {
    return this.dhpublishUrl;
  }

  public String getDhcrudUrl() {
    return this.dhcrudUrl;
  }

  public String getCollectionRegistryUrl() {
    return this.collectionRegistryUrl;
  }

  public String getGenericSearchUrl() {
    return this.genericSearchUrl;
  }

  public String getPidServiceUrl() {
    return this.pidServiceUrl;
  }

  public int getRefresh() {
    return this.refresh;
  }

  public int getServiceTimeout() {
    return this.serviceTimeout;
  }

  public boolean isSeafileEnabled() {
    return this.seafileEnabled;
  }

  public String getSeafileTokenSecret() {
    return this.seafileTokenSecret;
  }

  public String getSeafileUrl() {
    return this.seafileUrl;
  }

  public String getBadgeText() {
    return this.badgeText;
  }

  public void setSeafileUrl(String seafileUrl) {
    this.seafileUrl = seafileUrl;
  }

  public boolean isSSLCertVerification() {
    return this.SSLCertVerification;
  }

  public boolean isSkipPublishStatusCheck() {
    return this.skipPublishStatusCheck;
  }

  public boolean isDryrun() {
    return this.dryrun;
  }

  public boolean isDebug() {
    return this.debug;
  }

  public boolean isOverrideEppn() {
    return this.overrideEppn;
  }

  public String getEppn() {
    return this.eppn;
  }

  public String getLinkToDocumentation() {
    return this.linkToDocumentation;
  }

  public String getMailOfContact() {
    return this.mailOfContact;
  }

  public String getLogoutAAI() {
    return this.logoutAAI;
  }

  public void setLogoutAAI(String logoutAAI) {
    this.logoutAAI = logoutAAI;
  }

  public void setEnableAAI(boolean enableAAI) {
    this.enableAAI = enableAAI;
  }

  public void setSubcollectionsEnabled(boolean subcollectionsEnabled) {
    this.subcollectionsEnabled = subcollectionsEnabled;
  }

  public void setDariahStorageUrl(String dariahStorageUrl) {
    this.dariahStorageUrl = dariahStorageUrl;
  }

  public void setDhpublishUrl(String dhpublishUrl) {
    this.dhpublishUrl = dhpublishUrl;
  }

  public void setDhcrudUrl(String dhcrudUrl) {
    this.dhcrudUrl = dhcrudUrl;
  }

  public void setCollectionRegistryUrl(String collectionRegistryUrl) {
    this.collectionRegistryUrl = collectionRegistryUrl;
  }

  public void setGenericSearchUrl(String genericSearchUrl) {
    this.genericSearchUrl = genericSearchUrl;
  }

  public void setPidServiceUrl(String pidServiceUrl) {
    this.pidServiceUrl = pidServiceUrl;
  }

  public void setRefresh(int refresh) {
    this.refresh = refresh;
  }

  public void setServiceTimeout(int serviceTimeout) {
    this.serviceTimeout = serviceTimeout;
  }

  public void setSeafileEnabled(boolean seafileEnabled) {
    this.seafileEnabled = seafileEnabled;
  }

  public void setSeafileTokenSecret(String seafileTokenSecret) {
    this.seafileTokenSecret = seafileTokenSecret;
  }

  public void setSSLCertVerification(boolean sSLCertVerification) {
    this.SSLCertVerification = sSLCertVerification;
  }

  public void setSkipPublishStatusCheck(boolean skipPublishStatusCheck) {
    this.skipPublishStatusCheck = skipPublishStatusCheck;
  }

  public void setDryrun(boolean dryrun) {
    this.dryrun = dryrun;
  }

  public void setDebug(boolean debug) {
    this.debug = debug;
  }

  public void setOverrideEppn(boolean overrideEppn) {
    this.overrideEppn = overrideEppn;
  }

  public void setEppn(String eppn) {
    this.eppn = eppn;
  }

  public void setLinkToDocumentation(String linkToDocumentation) {
    this.linkToDocumentation = linkToDocumentation;
  }

  public void setMailOfContact(String mailOfContact) {
    this.mailOfContact = mailOfContact;
  }

  public String getRedisHostname() {
    return this.redisHostname;
  }

  public int getRedisPort() {
    return this.redisPort;
  }

  public int getRedisMaxParallel() {
    return this.redisMaxParallel;
  }

  public void setRedisHostname(String redisHostname) {
    this.redisHostname = redisHostname;
  }

  public void setRedisPort(int redisPort) {
    this.redisPort = redisPort;
  }

  public void setRedisMaxParallel(int redisMaxParallel) {
    this.redisMaxParallel = redisMaxParallel;
  }

  public String getLocalPassword() {
    return this.localPassword;
  }

  public void setLocalPassword(String localPassword) {
    this.localPassword = localPassword;
  }

  public String getLocalToken() {
    return this.localToken;
  }

  public void setLocalToken(String localToken) {
    this.localToken = localToken;
  }

  public String getPdpTokenServerUrl() {
    return this.pdpTokenServerUrl;
  }

  public void setPdpTokenManagerUrl(String pdpTokenManagerUrl) {
    this.pdpTokenManagerUrl = pdpTokenManagerUrl;
  }

  public String getPdpTokenManagerUrl() {
    return this.pdpTokenManagerUrl;
  }

  public void setPdpTokenServerUrl(String pdpTokenServerUrl) {
    this.pdpTokenServerUrl = pdpTokenServerUrl;
  }

  public void setBadgeText(String badgeText) {
    this.badgeText = badgeText;
  }

  public boolean isStatsdEnable() {
    return this.statsdEnable;
  }

  public String getStatsdHostname() {
    return this.statsdHostname;
  }

  public int getStatsdPort() {
    return this.statsdPort;
  }

  public void setStatsdEnable(boolean statsdEnable) {
    this.statsdEnable = statsdEnable;
  }

  public void setStatsdHostname(String statsdHostname) {
    this.statsdHostname = statsdHostname;
  }

  public void setStatsdPort(int statsdPort) {
    this.statsdPort = statsdPort;
  }

  public String getLinkToFAQ() {
    return this.linkToFAQ;
  }

  public String getLinkToAPIDoc() {
    return this.linkToAPIDoc;
  }

  public String getLinkToPrivPol() {
    return this.linkToPrivPol;
  }

  public void setLinkToFAQ(String linkToFAQ) {
    this.linkToFAQ = linkToFAQ;
  }

  public void setLinkToAPIDoc(String linkToAPIDoc) {
    this.linkToAPIDoc = linkToAPIDoc;
  }

  public void setLinkToPrivPol(String linkToPrivPol) {
    this.linkToPrivPol = linkToPrivPol;
  }

  public int getMaxUploadSize() {
    return this.maxUploadSize;
  }

  public void setMaxUploadSize(int maxUploadSize) {
    this.maxUploadSize = maxUploadSize;
  }

  public boolean isSkipLandingPage() {
    return this.skipLandingPage;
  }

  public void setSkipLandingPage(boolean skipLandingPage) {
    this.skipLandingPage = skipLandingPage;
  }

  @Override
  public String toString() {
    return "Configuration \n" + LINE + //
        "\nBasic settings " + //
        "\n\tinstanceName=" + this.instanceName + //
        "\n\tmenuHeaderColor=" + this.menuHeaderColor + //
        "\n\tenableAAI=" + this.enableAAI + //
        "\n\tlogoutAAI=" + this.logoutAAI + //
        "\n\tsubcollectionsEnabled=" + this.subcollectionsEnabled + //
        "\n\tdariahStorageUrl=" + this.dariahStorageUrl + //
        "\n\tdhpublishUrl=" + this.dhpublishUrl + //
        "\n\tdhcrudUrl=" + this.dhcrudUrl + //
        "\n\tcollectionRegistryUrl=" + this.collectionRegistryUrl + //
        "\n\tgenericSearchUrl=" + this.genericSearchUrl + //
        "\n\tpidServiceUrl=" + this.pidServiceUrl + //
        "\n\tpdpTokenServerUrl=" + this.pdpTokenServerUrl + //
        "\n\tpdpTokenManagerUrl=" + this.pdpTokenManagerUrl + //
        "\n\trefresh=" + this.refresh + "\n\tserviceTimeout="
        + this.serviceTimeout + //
        "\n\tbadgeText=" + this.badgeText + //
        "\n\tmaxUploadFileSize=" + this.maxUploadSize + " MB" + //
        //
        "\n\nSeafile \n" + //
        "\tseafileEnabled=" + this.seafileEnabled + //
        "\n\tseafileTokenSecret=" + this.seafileTokenSecret + //
        "\n\tseafileUrl=" + this.seafileUrl + //
        //
        "\n\nDeveloper options \n" + //
        "\tSSLCertVerification=" + this.SSLCertVerification + //
        "\n\tskipPublishStatusCheck=" + this.skipPublishStatusCheck + //
        "\n\tdryrun=" + this.dryrun + //
        "\n\tdebug=" + this.debug + //
        "\n\toverrideEppn=" + this.overrideEppn + //
        "\n\teppn=" + this.eppn + //
        "\n\tlocalToken=" + this.localToken + //
        "\n\tskipLandingPage=" + this.skipLandingPage + //
        "\n\tlinkToDocumentation=" + this.linkToDocumentation + //
        "\n\tlinkToFAQ=" + this.linkToFAQ + //
        "\n\tlinkToAPIDoc=" + this.linkToAPIDoc + //
        "\n\tmailOfContact=" + this.mailOfContact + //
        "\n\tlinkToPrivPol=" + this.linkToPrivPol + //
        //
        "\n\nRedis \n" + //
        "\tredisHostname=" + this.redisHostname + //
        "\n\tredisPort=" + this.redisPort + //
        "\n\tredisMaxParallel=" + this.redisMaxParallel + //
        //
        //
        "\n\nStatsd reporting \n" + //
        "\tStatsdEnable=" + this.statsdEnable + //
        "\n\tStatsdHostname=" + this.statsdHostname + //
        "\n\tStatsdPort=" + this.statsdPort + //
        //
        "\n" + LINE + "\n";
  }

}
