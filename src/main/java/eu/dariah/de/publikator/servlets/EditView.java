package eu.dariah.de.publikator.servlets;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.json.simple.JSONObject;
import org.xml.sax.SAXException;
import com.timgroup.statsd.StatsDClient;
import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.backend.DariahStorageClientWrapper;
import eu.dariah.de.publikator.backend.StatsdClient;
import eu.dariah.de.publikator.dao.CollectionDAO;
import eu.dariah.de.publikator.dao.DAOFactory;
import eu.dariah.de.publikator.utils.I18NUtils;
import eu.dariah.de.publikator.utils.RDFHelper;
import eu.dariah.de.publikator.utils.StorageUtil;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

/**
 * - #%L DARIAHDE :: Publikator Standalone %% Copyright (C) 2017 SUB Göttingen
 * (https://www.sub.uni-goettingen.de) %% Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 */
public class EditView extends BaseServlet {

  private static final long serialVersionUID = 1L;
  private static final Logger LOG = Logger.getLogger(EditView.class.getName());
  private static final String DARIAHSTORAGE_PREFIX = "dariahstorage";

  private static final String views[] = {"dariahStorageLoad", "dariahStorageView",
      "dariahStorageUpdateData", "dariahStorageAddFile", "dariahStorageUpdateFile",
      "dariahStorageDeleteFile", "metatdataExtract", "getTitle", "dcElements"};

  /**
   * @see HttpServlet#HttpServlet()
   */
  public EditView() {
    super(new HashSet<String>(Arrays.asList(views)));
  }

  /**
   *
   */
  @Override
  protected void index(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String storageId = request.getParameter("storageId");
    String selectedObjectUri = request.getParameter("selectedObjectUri");

    if (storageId == null) {
      // storageId comes from request-attribute coming from MainView.createNew()
      storageId = request.getAttribute("storageId").toString();
    }
    // storageId should not be null at this stage
    if (storageId == null) {
      throw new ServletException("storageId is null!");
    }

    request.setAttribute("storageId", storageId);
    request.setAttribute("selectedObjectUri", selectedObjectUri);
    request.setAttribute("language", request.getLocale().getLanguage());

    // seafile
    if (Configuration.getInstance().getConfig().isSeafileEnabled()) {
      request.setAttribute("isSeafileLibrary",
          this.clients.getSeafileClient().isSeafileLibrary(storageId));
      String seafileAuthToken = (String) request.getSession().getAttribute("seafileAuthToken");
      // TODO we could access the session directly in the JSP...
      request.setAttribute("seafileAuthToken", seafileAuthToken);
    }

    // if we are editing a collection item, then we need to remove it from the collection cache, we
    // don't know what the user is doing on edit, this is the best we can do about caching
    CollectionDAO dao = DAOFactory.getDAOFactory(DAOFactory.REDIS).getCollectionDAO();
    if (dao.deleteCollectionFromCache(storageId)) {
      LOG.info("Removed collection " + storageId + " from collection cache");
    } else {
      LOG.info("Failed to remove collection " + storageId + " from collection cache");
    }

    // translation array
    request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));
    response.setCharacterEncoding("UTF-8");

    RequestDispatcher rd = request.getRequestDispatcher("views/editCollectionView.jsp");
    rd.forward(request, response);

  }

  /**
   * called via ajax from tgforms, to load turtle data from storage
   * 
   * TODO: storage error handling
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void dariahStorageLoad(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String seafileToken = request.getParameter("seafileToken");

    String logID = StorageUtil.createRandomLogId();

    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);

    String storageId = id.substring(id.lastIndexOf(":") + 1);
    Response storageResponse = null;
    try {

      // set the retries, we catch any exception here in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy()
          .withDelay(150, TimeUnit.MILLISECONDS)
          .withMaxRetries(3)
          .retryOn(Exception.class);

      DariahStorageClientWrapper.ReadResponse rrs =
          this.storageClient.new ReadResponse(storageId, storageToken, logID);

      storageResponse = Failsafe.with(retryPolicy).get(() -> rrs.call());

      if (storageResponse.getStatus() != Response.Status.OK.getStatusCode()) {
        response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        throw new ServletException(
            logID + " Problem loading from storage: code " + storageResponse.getStatus() + " "
                + storageResponse.getStatusInfo().getReasonPhrase());
      }
    } catch (Exception e) {
      LOG.severe(logID + " dariahStorageLoad for storageID failed: " + storageId);
      LOG.severe(ExceptionUtils.getStackTrace(e));

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.read.failure");
      }
    }

    InputStream data = storageResponse.readEntity(InputStream.class);
    response.setContentType(storageResponse.getMediaType().toString());
    // seafile
    if (Configuration.getInstance().getConfig().isSeafileEnabled() && seafileToken != null) {
      LOG.info(logID + " sftoken: " + seafileToken);

      data = this.clients.getSeafileClient().setEncryptedToken(seafileToken)
          .appendSeafileMetadata(storageId, data);
    }

    IOUtils.copy(data, response.getOutputStream());
  }

  /**
   * called via ajax from edit view to just view files
   * 
   * TODO: storage error handling
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void dariahStorageView(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String logID = StorageUtil.createRandomLogId();

    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);

    String storageId = id.substring(id.lastIndexOf(":") + 1);
    Response storageResponse = null;
    try {

      // set the retries, we catch any exception here in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy()
          .withDelay(150, TimeUnit.MILLISECONDS)
          .withMaxRetries(3)
          .retryIf((Response wsResponse) -> response.getStatus() != 200);

      DariahStorageClientWrapper.ReadResponse rrs =
          this.storageClient.new ReadResponse(storageId, storageToken, logID);

      storageResponse = Failsafe.with(retryPolicy).get(() -> rrs.call());

      if (storageResponse.getStatus() != Response.Status.OK
          .getStatusCode()) {
        response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        throw new ServletException(
            "Problem loading object storage: code " + storageResponse.getStatus() + " "
                + storageResponse.getStatusInfo().getReasonPhrase());
      }
    } catch (Exception e) {
      LOG.severe(logID + " dariahStorageView for storageID failed: " + storageId);
      LOG.severe(ExceptionUtils.getStackTrace(e));

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.read.failure");
      }
    }

    InputStream data = storageResponse.readEntity(
        InputStream.class);
    response.setContentType(storageResponse.getMediaType().toString());

    // FIXME it would be good if we could set a filename, something like
    // response.setHeader("Content-Disposition","inline;
    // filename=\"afile.pdf\"");
    // response.setContentType("application/pdf; name=\"afile.pdf\"");
    // but how to get the title and append the filetype?

    IOUtils.copy(data, response.getOutputStream());
  }

  /**
   * called via ajax from tgforms, to update turtle data in storage
   * 
   * TODO: storage error handling
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void dariahStorageUpdateData(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String logID = StorageUtil.createRandomLogId();

    // multipart
    Part filePart = request.getPart("file"); // Retrieves <input type="file"
                                             // name="file">
    // String fileName =
    // Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
    // // MSIE fix.
    InputStream data = filePart.getInputStream();

    // turtle beautifier
    org.apache.jena.rdf.model.Model model = ModelFactory.createDefaultModel();
    model.read(data, null, "TURTLE");
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, "TURTLE");
    ByteArrayInputStream bis = new ByteArrayInputStream(outStream.toByteArray());

    LOG.info(logID + " update for storageId: " + id);
    String storageId = id.substring(id.lastIndexOf(":") + 1);

    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);

    String res = null;
    try {
      // set the retries, we catch any exception here in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy()
          .withDelay(100, TimeUnit.MILLISECONDS)
          .withMaxRetries(3)
          .retryOn(Exception.class);

      DariahStorageClientWrapper.UpdateFile uf = this.storageClient.new UpdateFile(storageId, bis,
          "text/plain; charset=utf-8", storageToken, logID);

      res = Failsafe.with(retryPolicy).get(() -> uf.call());

    } catch (Exception e) {
      response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      LOG.severe(logID + " dariahStorageUpdateData for storageID failed: " + storageId);
      LOG.severe(ExceptionUtils.getStackTrace(e));

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.update.failure");
      }
      return;
    }

    LOG.fine(logID + " " + res);

    response.setCharacterEncoding("UTF-8");
    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);

    response.getWriter().write("done, ok");
    // TODO: error handling for AJAX
  }

  /**
   * called via ajax from tgforms, to add a file to a collection, in this place just save to storage
   * and return a storage id it also returns the metadata extracted with tika in json
   * 
   * TODO: possibly use a separate function for adding a collection? TODO: storage error handling
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  @SuppressWarnings("unchecked")
  protected void dariahStorageAddFile(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    Part filePart = request.getPart("file"); // Retrieves <input type="file"
                                             // name="file">
    if (filePart == null) {
      jsonUploadErrorMessage("no file sent for upload", response);
      return;
    }

    String logID = StorageUtil.createRandomLogId();

    InputStream data = filePart.getInputStream();
    LOG.info(logID + " file uploaded received " + filePart.getName() + " size "
        + filePart.getSize());

    if (Configuration.getInstance().getConfig().isStatsdEnable()) {
      StatsDClient client = StatsdClient.getInstance().getClient();
      client.incrementCounter("files");
      client.count("filesize", filePart.getSize());
    }

    // TODO: do not extract md for subcollections
    LOG.info(logID + " Now extracting metadata for file upload");
    JSONObject mdJson = extractMetadate(data);

    LOG.info(filePart.getContentType());

    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);

    Long timeStart = System.currentTimeMillis();

    // we use getInputstream again, it seams that the extracted metadata call closes the stream
    String res = null;
    try {
      LOG.info(logID + " About to send the uploaded file to the storage");

      // set the retries, we catch any exception here in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy()
          .withDelay(300, TimeUnit.MILLISECONDS).withMaxRetries(3)
          .retryOn(Exception.class);

      DariahStorageClientWrapper.CreateFile cf = this.storageClient.new CreateFile(
          filePart.getInputStream(), filePart.getContentType(),
          storageToken, logID);

      res = Failsafe.with(retryPolicy).get(() -> cf.call());

      LOG.info(logID + " Stored file in dariah storage " + res);

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.recordExecutionTimeToNow("storage.time", timeStart);
      }
    } catch (Exception e) {
      LOG.severe(logID + " Storage of the file has failed");
      LOG.severe(ExceptionUtils.getStackTrace(e));

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.write.failure");
      }
      String msg = I18NUtils.getTranslatedString("upload-failed", getLocale(request));
      msg += ": " + e.getMessage();
      jsonUploadErrorMessage(msg, response);
      return;
    }

    LOG.info(logID + " Stored file " + res);

    if (data != null) {
      try {
        data.close();
      } catch (Exception e) {
        // Ignore
      }
    }

    // the javascript does not like json as content type: why?
    // response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);
    response.setCharacterEncoding("UTF-8");

    JSONObject responseJson = new JSONObject();
    responseJson.put("fileId", DARIAHSTORAGE_PREFIX + ":" + res);
    responseJson.put("metadata", mdJson);

    response.getWriter().write(responseJson.toString());

  }

  /**
   * called via ajax from tgforms to update a file contained in a collection. also returns the
   * metadata extracted with tika in json.
   * 
   * TODO: storage error handling
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void dariahStorageUpdateFile(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String logID = StorageUtil.createRandomLogId();

    // multipart
    Part filePart = request.getPart("file"); // Retrieves <input type="file"
                                             // name="file">
    if (filePart == null) {
      throw new ServletException("File is missing!");
    }

    InputStream data = filePart.getInputStream();

    String storageId = id.substring(id.lastIndexOf(":") + 1);
    LOG.info(logID + " updating " + storageId);

    JSONObject mdJson = extractMetadate(data);
    LOG.info(filePart.getContentType());

    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);

    String res = null;
    try {
      // set the retries, we catch any exception here
      // in future we can be more specific or exit early
      // if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy()
          .withDelay(300, TimeUnit.MILLISECONDS)
          .withMaxRetries(3)
          .retryOn(Exception.class);

      DariahStorageClientWrapper.UpdateFile uf = this.storageClient.new UpdateFile(storageId,
          filePart.getInputStream(), filePart.getContentType(), storageToken, logID);

      res = Failsafe.with(retryPolicy).get(() -> uf.call());

    } catch (Exception e) {
      LOG.severe(logID + " dariahStorageUpdateFile for storageID failed: " + storageId);
      LOG.severe(ExceptionUtils.getStackTrace(e));
      response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.update.failure");
      }

      return;
    }

    if (data != null) {
      try {
        data.close();
      } catch (Exception e) {
        // Ignore
      }
    }

    JSONObject responseJson = new JSONObject();
    responseJson.put("fileId", DARIAHSTORAGE_PREFIX + ":" + res);
    responseJson.put("metadata", mdJson);

    // if we set the content type to json, the javascript breaks, why?
    // response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);
    response.setCharacterEncoding("UTF-8");
    response.getWriter().write(responseJson.toJSONString());
  }

  /**
   * called via ajax from tgforms to delete a file contained in a collection
   * 
   * TODO: storage error handling
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void dariahStorageDeleteFile(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "fileStorageId")) {
      return;
    }

    String id = (String) request.getParameter("fileStorageId");
    String logID = StorageUtil.createRandomLogId();
    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);
    String storageId = id.substring(id.lastIndexOf(":") + 1);
    LOG.info(logID + " will delete " + storageId);

    try {
      // set the retries, we catch any exception here in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy()
          .withDelay(500, TimeUnit.MILLISECONDS)
          .withMaxRetries(3)
          .retryOn(Exception.class);

      DariahStorageClientWrapper.DeleteFile df = this.storageClient.new DeleteFile(
          storageId, storageToken, logID);

      Failsafe.with(retryPolicy).run(() -> df.call());
    } catch (Exception e) {
      LOG.severe(logID + " dariahStorageDeleteFile for storageID failed: " + storageId);
      LOG.severe(ExceptionUtils.getStackTrace(e));
      response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.delete.failure");
      }

      return;
    }

    response.setCharacterEncoding("UTF-8");
    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write("done");
    // TODO: error handling for client
  }

  /**
   * TODO: find out if this function is still used or useful from/for tgforms
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void metatdataExtract(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");

    String logID = StorageUtil.createRandomLogId();

    LOG.info(logID + " md extractReq for " + id);

    if (id.startsWith("dariahstorage:")) {
      String storageToken = getPDPTokenFromSession(request);
      LOG.info(logID + " pdptoken: " + storageToken);

      String storageId = id.substring(id.lastIndexOf(":") + 1);
      InputStream is = null;
      try {

        // set the retries, we catch any exception here
        // in future we can be more specific or exit early
        // if there is a non fixable state
        RetryPolicy retryPolicy = new RetryPolicy()
            .withDelay(200, TimeUnit.MILLISECONDS)
            .withMaxRetries(3).retryOn(Exception.class);

        DariahStorageClientWrapper.ReadFile rf =
            this.storageClient.new ReadFile(storageId, storageToken, logID);

        is = new BufferedInputStream(
            Failsafe.with(retryPolicy).get(() -> rf.call()));
      } catch (Exception e) {
        LOG.severe(logID + " metatdataExtract for storageID failed: " + storageId);
        LOG.severe(ExceptionUtils.getStackTrace(e));
        response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        return;
      }

      JSONObject metaJson = extractMetadate(is);

      OutputStream outStream = response.getOutputStream();
      outStream.write(metaJson.toString().getBytes());
      response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);
      response.setCharacterEncoding("UTF-8");

    } else if (id.startsWith("seafile:")) {
      LOG.severe("mdex for sf not yet implemented");
    }
  }

  /**
   * TODO: find out if this function is still used or useful from/for tgforms
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void getTitle(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String logID = StorageUtil.createRandomLogId();
    String storageToken = getPDPTokenFromSession(request);
    LOG.info(logID + " pdptoken: " + storageToken);
    String storageId = id.substring(id.lastIndexOf(":") + 1);

    InputStream data = null;
    try {

      // set the retries, we catch any exception here in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy().withDelay(200, TimeUnit.MILLISECONDS)
          .withMaxRetries(3).retryOn(Exception.class);

      DariahStorageClientWrapper.ReadFile rf =
          this.storageClient.new ReadFile(storageId, storageToken, logID);

      data = Failsafe.with(retryPolicy).get(() -> rf.call());

    } catch (Exception e) {
      LOG.severe(logID + " getTitle for storageID failed: " + storageId);
      LOG.severe(ExceptionUtils.getStackTrace(e));
      response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      return;
    }

    String title = RDFHelper.dcTitleFromInputStream(data, storageId);
    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.setCharacterEncoding("UTF-8");

    response.getWriter().write(title);
  }

  /**
   * Serves the DC Elements Turtle file depending on the user language it is done by using the
   * servlet because it might happen that a translation of the dcelements is missing and this keeps
   * the logic out of the JSP The default is serving the dcelements_en.ttl
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void dcElements(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    Locale userLocale = getLocale(request);
    List<String> dclocales =
        (List<String>) request.getServletContext().getAttribute("availableDCLocales");
    String dcfile = "dcelements_en.ttl";
    if (dclocales.contains(userLocale.getLanguage())) {
      dcfile = "dcelements_" + userLocale.getLanguage() + ".ttl";
    }

    InputStream file = request.getServletContext()
        .getResourceAsStream("/schema/" + dcfile);

    response.setContentType("text/turtle");
    response.setCharacterEncoding("UTF-8");

    IOUtils.copy(file, response.getOutputStream());
  }

  /**
   * @param is
   * @return
   */
  private static JSONObject extractMetadate(InputStream is) {

    AutoDetectParser parser = new AutoDetectParser();
    BodyContentHandler handler = new BodyContentHandler();
    Metadata metadata = new Metadata();

    try {
      // TikaConfig tika = new TikaConfig();
      parser.parse(is, handler, metadata);
    } catch (SAXException se) {
      LOG.warning("file probably too big for full metadata extraction " + se.getMessage());
    } catch (IOException | TikaException e) {
      LOG.severe("extractMetadata failed");
      LOG.severe(ExceptionUtils.getStackTrace(e));
    }

    // listAvailableMetaDataFields(metadata);

    String[] props = {"dc:format", "dc:creator", "dc:title"};

    Map<String, Object> jsonMap = new TreeMap<String, Object>();

    for (String prop : props) {
      jsonMap.put(prop, metadata.get(prop));
    }
    // check if we have a format. If we don't get DC format type at least we can always retrieve the
    // content type
    if (jsonMap.get("dc:format") == null) {
      jsonMap.put("dc:format", metadata.get(HttpHeaders.CONTENT_TYPE));
    }
    jsonMap.put("dc:date", metadata.get("dcterms:created"));

    if (metadata.get("GPS Latitude") != null) {
      String coordinates = "geo:lat=" + metadata.get("geo:lat") +
          ",geo:long=" + metadata.get("geo:long");
      String coverage[] = {coordinates,
          "Latitude: " + metadata.get("GPS Latitude"),
          "Longitude: " + metadata.get("GPS Longitude"),
          "Altitude: " + metadata.get("GPS Altitude")};

      jsonMap.put("dc:coverage", Arrays.asList(coverage));
    }

    LOG.info("extracted md");
    LOG.fine(metadata.toString());

    return new JSONObject(jsonMap);
  }

  /**
   * Wraps upload error messages into json message { "error": "File could not be saved." }
   * 
   * @param message
   * @return
   * @throws IOException
   */
  private static void jsonUploadErrorMessage(String message, HttpServletResponse response)
      throws IOException {

    response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);
    response.setCharacterEncoding("UTF-8");
    JSONObject json = new JSONObject();
    json.put("error", message);
    response.getWriter().write(json.toJSONString());
  }

}
