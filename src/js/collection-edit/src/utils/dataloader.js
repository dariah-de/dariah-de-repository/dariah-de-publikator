/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
/**
* This class should handle translation between turtle-RDF and the data model
*/

import TurtleLoader from './turtle-loader';
import N3 from 'n3';
import Escape from './escape'

// importing used functions separately saves libsize
//import _ from "lodash";
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import concat from 'lodash/concat';
import flattenDeep from 'lodash/flattenDeep';
import forEach from 'lodash/forEach';

// font-awesome icons
import 'font-awesome/css/font-awesome.css';

const _ = {
  find,
  isEmpty,
  concat,
  flattenDeep,
  forEach,
}

export default class DataLoader {

  constructor(options = {}) {
    this.schemaUrls = _.concat([], options.schemaUrl);
    this.ttloader = new TurtleLoader({partsAsListEnabled: options.partsAsListEnabled});
    this.dataModelUtils = options.dataModelUtils;
    this.loadSchema();
    this.escapeHtml = new Escape();
  }

  /**
  *
  */
  loadSchema() {
    // promise which is checked by loadData, if schema is already loaded
    this.schemaLoaded = new Promise(resolve => {

      //let loadPromises = this.schemaUrl.map(this.ttloader.load);

      let loadPromises = [];
      this.schemaUrls.forEach(url => {
        loadPromises.push(this.ttloader.load(url));
      });

      Promise.all(loadPromises)
      .then(res => {
              resolve();
       });
    });

  }

  /**
  *
  */
  emptyForm(rdfsClass) {
    let promise = new Promise(resolve => {
      this.schemaLoaded.then(res => {
        let schema = this.ttloader.getSchemaForSubject(rdfsClass);
        resolve(schema);
      });
    });
    return promise;
  }

  /**
  *
  */
  loadData(storageLocation, storageUri, rdfsClass, parentUri='') {
    let dataUrl = storageLocation + /[^:]*$/.exec(storageUri)[0];

    console.log('dataloading for: ' + dataUrl);

    let promise = new Promise(resolve => {

      // start only if schema was loaded before
      this.schemaLoaded.then(() => {

        this.ttloader.load(dataUrl).then(res => {
          // the form for this dataUrl
          let forms = [];
          // here we collect the promises for subcollection data loading
          let subColLoadPromises = [];

          const formModel = this.getFormModelForUri(storageUri);
          formModel.parentUri = parentUri;
          formModel.rdftype = rdfsClass;
          forms.push(formModel);

          const parts = this.getPartsForUri(storageUri, 'dcterms:hasPart');
          parts.forEach((uri) => {
            const rdftype = this.getTypeForUri(uri);
            if(rdftype !== 'dariah:Collection') {
              const formModel = this.getFormModelForUri(uri);
              formModel.parentUri = storageUri;
              formModel.rdftype = rdftype;
              formModel.format = this.getFirstLiteral(uri, 'dc:format');
              forms.push(formModel);
            } else {
              // recurse into subcollection data loading
              subColLoadPromises.push(this.loadData(storageLocation, uri, 'dariah:Collection', storageUri));
            }
          });

          // wait for subcol data loading to be done
          Promise.all(subColLoadPromises).then(res => {
            // add this form to subforms
            res.push(forms);
            // flatten the Array
            const allForms = _.flattenDeep(res);
            resolve(allForms);
          });

        });
      });

    });

    return promise;
  }

  /**
  *
  */
  addFile(storageUri, metadata, title, format, rdfsClass) {
    let schema = this.ttloader.getSchemaForSubject(rdfsClass);

    schema.schema.forEach((entry) => {
      entry.subject = storageUri;
    });

    // add title and format info from browser
    this.dataModelUtils.fillOrAppendFormField(schema.schema, 'dc:title', title);
    this.dataModelUtils.fillOrAppendFormField(schema.schema, 'dc:format', format);

    // add extracted metadata from service
    _.forEach(metadata, (value, key) => {
      if(value !== null) {
        if(Array.isArray(value)) {
          value.forEach(entry => {
            this.dataModelUtils.fillOrAppendFormField(schema.schema, key, entry);
          });
        } else {
          this.dataModelUtils.fillOrAppendFormField(schema.schema, key, value);
        }
      }
    });

    schema.schema.push({
      subject: storageUri,
      rdf_Property: 'rdf:type',
      fieldvalue: rdfsClass,
      isResource: true,
    });

    let resObject = {
      uri: storageUri,
      formModel : schema,
      rdftype: rdfsClass,
    };

    if(rdfsClass === 'dariah:Collection') {
      // also freshly created collections should have an empty parts object
      resObject.parts = [];
    }    

    return resObject;
  }

  /**
  *
  */
  getFormModelForUri(uri) {
    let rdfsClass = this.getTypeForUri(uri);

    // rdfs class is undefined we assume a collection / an empty object
    // TODO: possibly merge with emtpy data check below
    if(rdfsClass === undefined) {
      rdfsClass ='dariah:Collection';
    }

    // the schema is actually the datamodel generated from the schema
    // and contains also the values from the data later
    // TODO: possibly rename
    let schema = this.ttloader.getSchemaForSubject(rdfsClass);

    const data = this.ttloader.getDataForSubject(uri);

    // add storageUri to each triple for now, TODO: create a parent
    schema.schema.forEach((entry) => {
      entry.subject = uri;
    });

    // is there any data? else create empty form, and set subject uris
    if(_.isEmpty(data)) {
      // we assume now, that empty forms are of type dariah:Collection
      schema.schema.push({
        subject: uri,
        rdf_Property: 'rdf:type',
        fieldvalue: 'dariah:Collection',
        isResource: true,
      });
    }

    let parts=[];

    data.forEach((val) => {
      let property = this.ttloader.abbrevURI(val.predicate);
      let ent = _.find(schema.schema, { 'rdf_Property' : property });

      // if property is defined in schema
      if(! _.isEmpty(ent)) {
        let fieldentry;
        if(N3.Util.isLiteral(val.object)){
          fieldentry = N3.Util.getLiteralValue(val.object);
          this.dataModelUtils.fillOrAppendFormField(schema.schema, property, fieldentry);
        } else {
          //  we want abbrevURIs for storageIds
          if(property === "dcterms:hasPart") {
            if(N3.Util.isBlank(val.object)) {
              // this is a list, add all its members
              parts = parts.concat(this.ttloader.getPartArray(uri, "dcterms:hasPart"));
            } else {
              parts.push(this.ttloader.abbrevURI(val.object));
            }
          } else {
            fieldentry = val.object;
            this.dataModelUtils.fillOrAppendFormField(schema.schema, property, fieldentry);
          }
        }
      } else {
        // keep elems which are not in schema in data,
        // mainly this is rfds:type (a) for now, so isResource:true fits
        schema.schema.push({
          subject: this.ttloader.abbrevURI(val.subject),
          rdf_Property: val.predicate,
          fieldvalue: val.object,
          isResource: true,
        });
      }
    });

    let resObject = {
      uri,
      formModel : schema,     
    }

    if(rdfsClass === 'dariah:Collection') {
      // add the parts array to object
      resObject.parts = parts;
    }

    return resObject;

  }

  /**
  *
  */
  getTypeForUri(uri) {
    //console.log('getting type for: ' + uri);
    const typeTriple = this.ttloader.getPartTriples(uri, 'rdf:type')[0];
    //console.log(typeTriple);
    if(typeTriple !== undefined) {
      return this.ttloader.abbrevURI(typeTriple.object);
    }
  }

  /**
  *
  */
  getPartsForUri(uri, partRelation) {
    const parts = this.ttloader.getPartArray(uri, partRelation);
    return parts;
  }

  /**
  * TODO: is this used / useful?
  */
  getLiteralValues(uri, predicate) {
    let result = [];
    const data = this.ttloader.getPartTriples(uri, predicate);
    data.forEach((val) => {
      result.push(N3.Util.getLiteralValue(val.object));
    });
    return result;
  }

  /**
  *
  */
  getFirstLiteral(uri, predicate) {
    const data = this.ttloader.getPartTriples(uri, predicate);
    //console.log(data);
    if(data.length > 0) {
      //console.log(data);
      return N3.Util.getLiteralValue(data[0].object);
    } else {
      return '';
    }
  }

  /**
  *
  */
  writeTurtle(data, uri) {
    let triples = [];
    data.forEach(schema => {
      schema.forEach(elem => {
        if(elem.fieldvalue && elem.fieldvalue.length !== 0) {
          let S = elem.subject;
          let P = elem.rdf_Property;
          let O;
          if(elem.isResource) {
            O = this.ttloader.abbrevURI(elem.fieldvalue);
          } else {
            // string literals need to be enclosed with "
            O = `"${elem.fieldvalue}"`;
          }
          //console.log(`<${S}> <${P}> "${O}"`);
          triples.push({subject: S, predicate: P, object: O});
        }
      });
    });

    return this.ttloader.writeTurtle(triples, uri, this.dataModelUtils.getParts(uri));
  }

  /**
  *
  */
  getJSTreeData(storageUri, parentUri = '#'){
    const parts = this.dataModelUtils.getParts(storageUri);

    let treeData = [];

    treeData.push({
      id: storageUri,
      parent: parentUri,
      text: this.escapeHtml.escape(this.getFirstLiteral(storageUri, 'dc:title')),
      icon: 'fa fa-folder-open-o',
      state: { opened: true },
      type: 'collection',
    });

    parts.forEach((uri, idx) => {
      if(this.getTypeForUri(uri) !== 'dariah:Collection') {
        treeData.push({
          id : uri,
          parent: storageUri,
          text:  this.escapeHtml.escape(this.getFirstLiteral(uri, 'dc:title')),
          icon: 'fa ' + this.getIconForMime(this.getFirstLiteral(uri, 'dc:format')),
        });
      } else {
        treeData = treeData.concat(this.getJSTreeData(uri, storageUri));
      }
    });

    return treeData;
  }

  /**
  *
  */
  getIconForMime(format) {
    //console.log('get icon for ' + format);
    if(format.includes('image')) {
      return 'fa-file-image-o';
    } else if(format.includes('text')){
      return 'fa-file-text-o';
    } else if(format.includes('pdf')){
      return 'fa-file-pdf-o';
    } else if(format.includes('audio')){
      return 'fa-file-audio-o';
    } else if(format.includes('video')){
      return 'fa-file-video-o';
    } else {
      return 'fa-file-o';
    }

  }

}
