/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import findIndex from 'lodash/findIndex';
import findLastIndex from 'lodash/findLastIndex';
import find from 'lodash/find';
import findLast from 'lodash/findLast';
import filter from 'lodash/filter';
import isEmpty from 'lodash/isEmpty';
import clone from 'lodash/clone';

const _ = {
  findIndex,
  findLastIndex,
  find,
  findLast,
  filter,
  isEmpty,
  clone,
}

class TGFormDataModelUtils{

  constructor(options = {}) {
    this.formdata = options.formdata;
  }

  /**
  * methods which work directly on the schema provided with the constructor
  */

  findEntryForUri(uri) {
    return _.find(this.formdata, {uri});
  }

  findIndexForUri(uri) {
    return _.findIndex(this.formdata, {uri});
  }

  findDirectChildrenForUri(uri) {
    return _.filter(this.formdata, {parentUri: uri});
  }


  /**
   * methods for managing the parts of an collection
   */
  getParts(parentUri) {
    return this.formdata.filter(o => o.uri === parentUri)[0].parts;
  }

  addPart(parentUri, uri) {
    this.getParts(parentUri).push(uri);
  }

  removePart(parentUri, uri) {
    console.log('trying to remove haspart rel: ' + uri + ' from parent: ' + parentUri);
    let parts = this.getParts(parentUri);
    let idx = parts.indexOf(uri);
    if(idx > -1) {
      parts.splice(idx, 1);
    } else {
      console.error('uh, oh, not found uri trying to remove ' + uri + ' in parent ' +  parentUri);
    }
  }

  movePart(uri, fromParent, fromPos, toParent, toPos) {
    console.log('moving node ' + uri + ' from ' + fromParent + "|"+ fromPos + " to " + toParent + "|" +  toPos);
    // remove from old parent
    this.removePart(fromParent, uri);
    // attach on new parent in new pos
    this.getParts(toParent).splice(toPos, 0, uri);

    // mark dataobject belonging to new parent
    if(fromParent !== toParent) {
      this.formdata.filter(o => o.uri == uri).map(o => o.parentUri = toParent);
    }

  }

  /**
  * methods which do not work on the schema provided with the constructor
  */

  // this function also removes duplicate entries
  fillOrAppendFormField(schema, property, value) {
    // check for duplicate entry, do nothing if field + value already there
    if(_.findIndex(schema, {rdf_Property: property, fieldvalue: value}) > 0 ){
      console.log('found duplicate: ' + property + " - " + value);
      return;
    }

    const entry = _.findLast(schema, {rdf_Property: property});
    // if formfield is not yet set, fill it now
    if (_.isEmpty(entry.fieldvalue)) {
      entry.fieldvalue = value;
    // clone and fill formfield otherwise
    } else {
      let idx = _.findLastIndex(schema, { rdf_Property: property });
      let clone = _.clone(entry);
      clone.fieldvalue = value;
      schema.splice(idx+1, 0, clone);
    }
  }
  setChildDataField(uri, rdf_Property, fieldentry, recursive) {
    let children = this.findDirectChildrenForUri(uri);
    children.forEach(child => {
      //let field = _.findLast(child.formModel.schema, {rdf_Property: rdf_Property});
      this.fillOrAppendFormField(child.formModel.schema, rdf_Property, fieldentry);
      if(recursive && child.rdftype === 'dariah:Collection') {
        this.setChildDataField(child.uri, rdf_Property, fieldentry, recursive);
      }
    });
  }


}

export default TGFormDataModelUtils;
