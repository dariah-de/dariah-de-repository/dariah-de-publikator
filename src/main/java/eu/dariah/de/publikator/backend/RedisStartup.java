package eu.dariah.de.publikator.backend;

import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
@WebListener
public class RedisStartup implements ServletContextListener {

	private static final Logger	LOG	= Logger.getLogger(RedisStartup.class.getName());
	
    /**
     * Default constructor. 
     */
    public RedisStartup() {
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // close REDIS
    		RedisPool.getInstance().disconnect();
    		LOG.info("Connection to Redis server stopped");
    		
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
         // start REDIS pool
    		RedisPool.getInstance().connect();
    		
    		// test if server is reachable
    		try {
    			RedisPool.getInstance().getJedis().select(0);
    		}catch (Exception e) {
    			LOG.severe("Connection to Redis server failed");
    			LOG.severe(e.getMessage());
    			e.printStackTrace();
    			
    		}
    		LOG.info("Connection to Redis server started");
    }
	
}
