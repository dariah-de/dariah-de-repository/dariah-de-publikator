/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './really-publish-alert.html';
import $ from 'jquery';

let ReallyPublishAlert = Ractive.extend({
  template: template,

  onrender: function() {
    this.on( 'publish', function (event) {
      this.set('visible', false);
      this.parent.set('publishInProgress', true);
      this.parent.set('viewPublishStartAlert', true);
      var collectionId = this.get('storageId');
      $.post(this.get('publishCollectionUrl'), {
        collectionId : collectionId
      }).done((data) => {
    	  console.log("really publish");
    	  	//if (!this.parent.publishStatusRunning) {
	    	  	this.parent.publishStatusRunning = true;
	        this.parent.updateProgress();
    	  	//}
      }).fail((data) => {
        console.log("++ error starting publish process for " + collectionId + " ++");
            this.set('status', 'service-not-available');
      });
    });
    
    this.observe( 'allrights', function (newVar, oldVar, p) {
    		if (newVar.length == 2) {
    			this.set('agreed', true);
    		}else{
    			this.set('agreed', false);
    		}
    });

    this.on( 'close', function (event) {
      this.set('visible', false);
    })
  }
});

export default ReallyPublishAlert;
