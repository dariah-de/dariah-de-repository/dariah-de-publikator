package eu.dariah.de.publikator.servlets;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.exception.ExceptionUtils;
import eu.dariah.de.publikator.backend.DariahStorageClientWrapper;
import eu.dariah.de.publikator.backend.PublishClientWrapper;
import eu.dariah.de.publikator.beans.StatusMessage;
import eu.dariah.de.publikator.beans.User;
import eu.dariah.de.publikator.dao.DAOFactory;
import eu.dariah.de.publikator.dao.LanguageDAO;
import eu.dariah.de.publikator.servicebeans.ClientsServiceBean;
import eu.dariah.de.publikator.utils.I18NUtils;

/**
 * - #%L DARIAHDE :: Publikator Standalone %% Copyright (C) 2019 SUB Göttingen
 * (https://www.sub.uni-goettingen.de) %% Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 */
public abstract class BaseServlet extends HttpServlet {

  /**
   * 
   */
  private static final long serialVersionUID = -5669875118202275564L;
  private Set<String> views;

  // this bean holds all web service clients, no-retry possible
  protected ClientsServiceBean clients;
  // this is the wrapper around the storage client for retry implementation
  protected DariahStorageClientWrapper storageClient;
  // this is the wrapper around the publish clients for retry implementation
  protected PublishClientWrapper publishClient;

  private static final Logger LOG = Logger.getLogger(BaseServlet.class.getName());

  /**
   * @param views
   */
  public BaseServlet(Set<String> views) {
    super();
    this.views = views;
    this.views.add("index");
    this.clients = new ClientsServiceBean();
    this.publishClient = new PublishClientWrapper();
    this.storageClient = new DariahStorageClientWrapper();
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  abstract protected void index(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException;

  /*
   * (non-Javadoc)
   * 
   * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
   * javax.servlet.http.HttpServletResponse)
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    router(req, resp);
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
   * javax.servlet.http.HttpServletResponse)
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    router(req, resp);
  }

  /**
   * Handles 404 errors from the router
   * 
   * @param req
   * @param resp
   * @param url
   * @throws ServletException
   * @throws IOException
   */
  private static void notFound(HttpServletRequest req, HttpServletResponse resp, String url)
      throws ServletException, IOException {

    req.setAttribute("errorheadline", "Error 404");
    req.setAttribute("details", "The requested view (" + url + ") was not found!");
    // translation array
    req.setAttribute("i18n", I18NUtils.i18n(getLocale(req)));
    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
    resp.setCharacterEncoding("UTF-8");

    LOG.info("HTTP 404 URL: " + url);

    RequestDispatcher rd = req.getRequestDispatcher("views/error.jsp");
    rd.include(req, resp);
  }

  /**
   * you can call this method if you want to display an error page Note: Either pass an exception
   * object OR leave this empty and set alternativeErrortext instead.
   * 
   * @param request
   * @param response
   * @param errorHeadline
   * @param error
   * @param alternativeErrortext
   * @throws ServletException
   * @throws IOException
   */
  protected void displayErrorpage(HttpServletRequest request, HttpServletResponse response,
      String errorHeadline, Exception error, String explanationText)
      throws ServletException, IOException {

    request.setAttribute("errorheadline", errorHeadline);
    if (error != null) {
      request.setAttribute("stacktrace", ExceptionUtils.getStackTrace(error));
    }
    if (explanationText != null) {
      request.setAttribute("details", explanationText);
    }
    // translation array
    request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));
    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    response.setCharacterEncoding("UTF-8");

    RequestDispatcher rd = request.getRequestDispatcher("views/error.jsp");
    rd.include(request, response);
  }

  /**
   * @param request
   * @param response
   * @param message
   * @throws ServletException
   * @throws IOException
   */
  protected void displayErrorpage(HttpServletRequest request, HttpServletResponse response,
      StatusMessage message) throws ServletException, IOException {

    if (message.getMessageHeadlineKey() != null) {
      request.setAttribute("errorheadline",
          I18NUtils.getTranslatedString(message.getMessageHeadlineKey(), getLocale(request)));
    }

    if (message.getMessageDetailKey() != null) {
      request.setAttribute("details",
          I18NUtils.getTranslatedString(message.getMessageDetailKey(), getLocale(request)));
    } else {
      request.setAttribute("details", message.getMessage());
    }
    // translation array
    request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));

    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    response.setCharacterEncoding("UTF-8");

    RequestDispatcher rd = request.getRequestDispatcher("views/error.jsp");
    rd.include(request, response);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void router(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // set the context URL into the request object, this is very handy on the JSPs
    String scheme = request.getScheme(); // http or https
    String serverName = request.getServerName(); // hostname
    int serverPort = request.getServerPort(); // port
    String contextPath = request.getContextPath(); // application path
    String url = scheme + "://" + serverName + ":" + serverPort + contextPath;
    request.setAttribute("url", url);

    // set selected locale in request
    request.setAttribute("locale", getLocale(request));

    // do the routing, find the declared methods and see if they match against the request.
    Method[] methods = this.getClass().getDeclaredMethods();

    String currentView = null;
    if (doesParamExist(request, "view")) {
      currentView = request.getParameter("view");
    }

    if (currentView == null) {
      index(request, response);
      return;
    }

    for (Method method : methods) {
      if (this.views.contains(currentView) && method.getName().equals(currentView)) {
        try {
          method.invoke(this, request, response);
          return;
        } catch (Exception e) {
          // we are doing error handling here. If a servlet action
          // throws an uncaught exception it will land here
          // we try to give as much details as we can
          displayErrorpage(request, response, e.getCause().getMessage(), e, null);
          LOG.severe(
              "Problem invoking method: " + method.getName() + " " + e.getCause().getMessage());
          return;
        }
      }
    }

    // Still here ? then we have a list of urls, but no matches. 404.
    // FIXME better get full URL here, but behind proxy tomcat needs to be configured correctly.
    // else we get locahost:8080/abc ...
    notFound(request, response, currentView);
  }

  /**
   * @param request
   * @return
   */
  @SuppressWarnings("unchecked")
  public static Locale getLocale(HttpServletRequest request) {

    // check if the session has a locale attribute, then use this
    // the user can override the languages via dropdown menu
    // else we set it to the locale which the browser has submitted
    // this is convenient for all methods, therefore we set it here.
    Locale locale = null;

    // if logged in try to see if the user has set a language
    if (getUser(request) != null) {
      LanguageDAO lang = DAOFactory.getDAOFactory(DAOFactory.REDIS).getLanguageDAO();
      locale = lang.getUserlanguage(getUser(request).getEppn());
      // Did the user change the language on the drop down? Then it is in the session
    }
    if (request.getSession().getAttribute("locale") != null) {
      locale = (Locale) request.getSession().getAttribute("locale");
    } else {
      // try to load the language from the request (browser)
      if (locale == null) {
        locale = request.getLocale();
      }
      // Fallback
      if (locale == null) {
        locale = Locale.ENGLISH;
      }
      // we need to map 2 letter language codes to variants to get the right translation
      // we run it here, in the session and user settings we assume that a correct value is present
      locale = I18NUtils.mapISOCode(
          (List<Locale>) request.getServletContext().getAttribute("availableLocales"), locale);
    }

    // set it to the session
    request.getSession(false).setAttribute("locale", locale);

    return locale;
  }

  /**
   * check if parameter exists, if not set HTTP status
   * 
   * @param request
   * @param response
   * @param param
   * @return
   * @throws IOException
   */
  protected boolean doesParamExist(HttpServletRequest request, HttpServletResponse response,
      String param) throws IOException {

    boolean valid = false;
    if (request.getParameterMap().containsKey(param)) {
      String value = request.getParameter(param);
      if (value == null) {
        response.getWriter().write(param + " is missing!");
        response.setStatus(405);
        return false;
      } else if (value.length() > 0) {
        valid = true;
      }
    }

    return valid;
  }

  /**
   * just check of a parameter exists and if the length is > 0
   * 
   * @param request
   * @param param
   * @return
   * @throws IOException
   */
  protected boolean doesParamExist(HttpServletRequest request, String param) throws IOException {

    boolean valid = false;
    if (request.getParameterMap().containsKey(param)) {
      String value = request.getParameter(param);
      if (value == null) {
        return false;
      } else if (value.length() > 0) {
        valid = true;
      }
    }

    return valid;
  }

  /**
   * This will display an HTML error page if a parameter is not present you have to check the
   * boolean return, if it is true, you have add a return statement into your method else the
   * processing will not stop!
   * 
   * @param request
   * @param response
   * @param param
   * @throws IOException
   * @throws ServletException
   */
  protected boolean ensureParameterIsPresentOrDisplayErrorpage(HttpServletRequest request,
      HttpServletResponse response, String param) throws IOException, ServletException {

    boolean present = doesParamExist(request, param);
    if (!present) {
      displayErrorpage(request, response,
          I18NUtils.getTranslatedString("misc-error", getLocale(request)), null,
          I18NUtils.getTranslatedString("misc-parameter-missing", getLocale(request)) + " "
              + param);
    }

    return present;
  }

  /**
   * the the user object with the user details
   * 
   * @return
   */
  public static User getUser(HttpServletRequest request) {

    if (request.getSession(false) != null) {
      return (User) request.getSession(false).getAttribute("userObject");
    }

    return null;
  }

  /**
   * @param request
   * @return
   */
  public static String getPDPTokenFromSession(HttpServletRequest request) {
    return (String) request.getSession().getAttribute("pdpAuthToken");
  }

  /**
   * checks if the user is logged in
   * 
   * @param request
   * @return
   */
  public static boolean isUserAuthenticated(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (null == session) {
      return false;
    }
    if (null == session.getAttribute("authenticated")) {
      return false;
    }

    // still here? then we are authenticated
    return true;
  }

}
