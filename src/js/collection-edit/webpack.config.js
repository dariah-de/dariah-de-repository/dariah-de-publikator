var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/dariah-collection-edit.js',
    output: {
        filename: './dist/js/dariah-collection-edit.js',
        library: 'DariahCollectionEdit',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                loader: 'raw'
            },
            {
              test: /\.css$/,
              loader: ExtractTextPlugin.extract('style', 'css')
            },
            {
              test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
              loader: 'url'
            }
        ]
    },
    // This is to load polyfills (http://mts.io/2015/04/08/webpack-shims-polyfills/)
    plugins: [
        new webpack.ProvidePlugin({
            'es6-promise': 'es6-promise'
        }),
        // this css is only needed for local test with npm start
        // basically it includes the font awesome, which is already
        // part of the DARIAH bootstrap template
        new ExtractTextPlugin('./dist/css/dariah-collection-edit.css'),
    ]
};
