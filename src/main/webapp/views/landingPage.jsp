<%@ page contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>

<div class="main">
	<div class="main_content">

        <!-- Status messages =============================================== -->

        <div id="dariah-status-message-area" class="hide">
            <div id="dariah-status-message-text"></div>
            <br/>
        </div>

        <!-- Topic Text ==================================================== -->

		<h1>${i18n['publikator']}<c:if test="${not empty badgeText}"> &nbsp;<span class="badge">${badgeText}</span></c:if></h1>
		<div>

			<p>${i18n['landing-welcome']}</p>

            <br/>

            <!-- TEST and DEV messages ========================================= -->

      		<c:if test="${(applicationScope.instanceName eq 'TEST') || (applicationScope.instanceName eq 'DEVELOP')}">
    		<div class="well">
    			<button type="button" class="close" data-dismiss="alert">&times;</button>
    			<p class="text-success">
    				<c:if test="${(applicationScope.instanceName eq 'TEST')}">
    					<strong>${i18n['collection-overview-instance-msg-test']}</strong>
    				</c:if>
    				<c:if test="${(applicationScope.instanceName eq 'DEVELOP')}">
    					<strong>${i18n['collection-overview-instance-msg-develop']}</strong>
    				</c:if>
    			</p>
    		</div>
    		</c:if>

			<h2>${i18n['landing-documentation']}</h2>

			<p>${i18n['landing-documentation-havealook']}&nbsp;<a href="${applicationScope.linkToDocumentation}">${i18n['landing-documentation']}</a>.</p>
			<p>${i18n['landing-note']}</p>
		</div>
		<br/>
		<div>
			<form action="login" method="GET">
				<button class="btn btn-primary" title="">
					<i class="fa fa-arrow-right"></i> ${i18n['landing-go']}
				</button>
			</form>
		</div>
	</div>
</div>

<%@ include file="footer.jsp" %>
