package eu.dariah.de.publikator.dao;

import java.util.ArrayList;
/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.dariah.de.publikator.backend.RedisPool;
import eu.dariah.de.publikator.beans.Collection;
import redis.clients.jedis.Jedis;

public class RedisCollectionDAO implements CollectionDAO {

	@Override
	public List<Collection> getCollectionsByUserId(String userId) {
		
		if (userId == null)
			return null;
		
		List<Collection> collections = new ArrayList<Collection>();
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
		
			Set<String> collectionIds = jedis.smembers(userId);
			
			for (String key : collectionIds) {
				Collection collection = new Collection(userId, key);
				collections.add(collection);
			}
		
		}
		
		return collections;
	}


	@Override
	public boolean deleteCollection(String userId, String collectionId) {
		boolean result = false;
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
		
			Long numberDeleted = jedis.srem(userId, collectionId);
			if (numberDeleted > 0)
				result = true;	

		}
		
		return result;
	}

	@Override
	public boolean deleteAllCollectionsByUserId(String userId) {

		boolean result = false;
		
			try (Jedis jedis = RedisPool.getInstance().getJedis()) {
			
			Long numberDeleted = jedis.del(userId);
			if (numberDeleted > 0)
				result = true;	
	
		}
		
		return result;
	}

	@Override
	public boolean saveCollection(Collection collection) {
		
		boolean result = false;
		
		// user id and storage ID needs to be present!
		if (collection.getUserId() == null || collection.getStorageId() == null)
			return false;
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
		
			Long number = jedis.sadd(collection.getUserId(), collection.getStorageId());
			
			if (number > 0)
				result = true;	

		}
		
		return result;
	}


	@Override
	public boolean saveCollectionCache(Collection collection) {
		boolean result = false;
		
		if (collection.getStorageId() == null)
			return false;
		String hashKey = "colcache#"+collection.getStorageId();
		long number = 0;
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
			
			number += jedis.hset(hashKey, "userId", collection.getUserId());
			number += jedis.hset(hashKey, "title", collection.getTitle());
			number += jedis.hset(hashKey, "description", collection.getDescription());
			number += jedis.hset(hashKey, "creator", collection.getCreator());
			number += jedis.hset(hashKey, "seafileRef", collection.getDescribesSeafileLibrary());
			number += jedis.hset(hashKey, "numberOfFiles", String.valueOf(collection.getNumberOfFiles()));
			number += jedis.hset(hashKey, "numberOfCollections", String.valueOf(collection.getNumberOfCollections()));
		
		}
	
		if (number > 0)
			result = true;	
		
		return result;
	}


	@Override
	public Collection loadCollectionFromCache(String collectionId) {
		
		if (collectionId == null)
			return null;
		
		Collection col = null;
		
		try (Jedis jedis = RedisPool.getInstance().getJedis()) {
		
			String key = "colcache#"+collectionId;
			// test if item is in DB
			if (!jedis.exists(key))
				return null;
			
			Map<String, String> fields = jedis.hgetAll(key);
			String userId = fields.get("userId");
			String title = fields.get("title");
			String description = fields.get("description");
			String creator = fields.get("creator");
			String seafileRef = fields.get("seafileRef");
			String numberOfFiles = fields.get("numberOfFiles");
			String numberOfCollections = fields.get("numberOfCollections");
	
			col = new Collection(userId, 
					collectionId, 
					title, 
					description, 
					creator, 
					"", 
					"", 
					seafileRef, 
					"", 
					Integer.parseInt(numberOfFiles), 
					Integer.parseInt(numberOfCollections));
		
		}
		
		return col;
	}


	@Override
	public boolean deleteCollectionFromCache(String collectionId) {
		boolean result = false;

		Jedis jedis = RedisPool.getInstance().getJedis();
		
		String key = "colcache#"+collectionId;
		
		Long numberDeleted = jedis.del(key);
		if (numberDeleted > 0)
			result = true;	
	
		jedis.close();
		
		return result;
	}

}
