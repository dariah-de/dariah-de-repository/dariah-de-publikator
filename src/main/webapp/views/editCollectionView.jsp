<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file="header.jsp" %>

<link type="text/css" rel="stylesheet" href="css/jstree-theme-default/style.min.css?${applicationScope.buildTimestamp}">
<link type="text/css" rel="stylesheet" href="css/jstree-customization.css?${applicationScope.buildTimestamp}">

<!-- show only if seafile enabled -->
<c:if test="${applicationScope.seafileEnabled}">
	<c:if test="${isSeafileLibrary}">
		<div class="seafileActionBox">
		    <i class="icon-info-sign"></i> ${i18n['seafile-managed-collection']}
	    	<br/>
			<strong>Seafile:</strong>
			<c:if test="${not empty seafileAuthToken}">
				${i18n['seafile-connected']} |
				<button class="btn btn-small" onclick="location.reload();"
				title="${i18n['tt-seafile-reload-files']}"><i class="icon-refresh"></i> ${i18n['seafile-reload-files']}</button>
			</c:if>
			<c:if test="${empty seafileAuthToken}">
				${i18n['seafile-not-connected']} |
				<a href="${seafileviewURL}" class="btn btn-small" title="${i18n['tt-seafile-connect']}">
					<i class="icon-signin"></i> ${i18n['seafile-connect']}
				</a>
			</c:if>
			| <a href="https://sftest.de.dariah.eu/docs/" target="_blank"
			title="${i18n['tt-seafile-help']}"><i class="icon-question-sign"></i> ${i18n['seafile-help']}</a>
		</div>
	</c:if>
</c:if>

<!-- Status messages ======================================================= -->

<div id="dariah-status-message-area" class="hide">
    <div id="dariah-status-message-text"></div>
    <br/>
</div>

<!-- Topic Text ============================================================ -->

<h1>${i18n['publikator']}: ${i18n['edit-collection']}<c:if test="${applicationScope.debug}">&nbsp; <span class="badge">DEBUG</span></c:if><c:if test="${not empty badgeText}">&nbsp; <span class="badge">${badgeText}</span></c:if></h1>

<p>${i18n['edit-collection-description']}</p>

<br/>

<div id="dariah-collection-edit"></div>

<h4 id="gathering">
	<img src="https://res.de.dariah.eu/dhrep/img/spinning-flower_slow.gif" width="24" alt="...">
</h4>

<link type="text/css" rel="stylesheet" href="css/upload.css?${applicationScope.buildTimestamp}">
<script src="js/upload.js?${applicationScope.buildTimestamp}"></script>
<script src="js/dariah-collection-edit.js?${applicationScope.buildTimestamp}"></script>

<script>
	const i18nMap = {
		<c:forEach items="${i18n}" var="entry">
			'${entry.key}' : '${esc:escapeEcmaScript(entry.value)}',
		</c:forEach>
	};

	const edit = new DariahCollectionEdit.default({
		i18nMap,
		elementId: 'dariah-collection-edit',
		schemaUrls: ['${pageContext.request.contextPath}/schema/dariahDataobjects.ttl', '${pageContext.request.contextPath}/editView?view=dcElements'],
		storageUri: 'dariahstorage:${storageId}',
		storageLocation: '${applicationScope.dariahStorageUrl}',
		loadFileUrl: 'editView?view=dariahStorageLoad&storageId=',
		viewFileUrl: 'editView?view=dariahStorageView&storageId=',
		updateDataUrl: 'editView?view=dariahStorageUpdateData',
		deleteFileUrl: 'editView?view=dariahStorageDeleteFile',
		deleteSubcollectionUrl: 'mainView?view=deleteSubCollection',
		addFileUrl: 'editView?view=dariahStorageAddFile',
		updateFileUrl: 'editView?view=dariahStorageUpdateFile',
	    storageUrl: '${applicationScope.dariahStorageUrl}',
	    storageToken: '${sessionScope.pdpAuthToken}',
	    statusUrl: 'status',
	    enableAAI: ${applicationScope.enableAAI},
		maxUploadSize: ${applicationScope.maxUploadSize},
		subcollectionsEnabled: ${applicationScope.subcollectionsEnabled},
		partsAsListEnabled: true,
		autosaveEnabled: true,
		<c:if test="${applicationScope.seafileEnabled}">
			seafileEnabled: ${applicationScope.seafileEnabled},
			seafileDownloadUrl: 'seafileView?view=seafileDownload',
			seafileAuthToken: '${seafileAuthToken}',
			isSeafileLibrary: ${isSeafileLibrary},
		</c:if>
		debug: ${applicationScope.debug},
		selectedObjectUri: '${selectedObjectUri}',
		mainViewUrl: 'mainView?',
		publishimgpath : 'https://res.de.dariah.eu/dhrep/img',
	 });
</script>

<%@ include file="footer.jsp" %>
