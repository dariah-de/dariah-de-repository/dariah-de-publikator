export default class Escape {

  constructor(options = {}) {

  }
  
  escape (string) {
	   string = string.replace(/</g, '&lt;');
	   string = string.replace(/>/g, '&gt;');
	   return string;
	}
  
} 