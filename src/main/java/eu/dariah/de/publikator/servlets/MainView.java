package eu.dariah.de.publikator.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpStatus;
import org.omg.CORBA.SystemException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.timgroup.statsd.StatsDClient;
import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.backend.DariahStorageClientWrapper;
import eu.dariah.de.publikator.backend.PublishClientWrapper;
import eu.dariah.de.publikator.backend.StatsdClient;
import eu.dariah.de.publikator.backend.threadpool.DeleteCollectionWorker;
import eu.dariah.de.publikator.beans.Collection;
import eu.dariah.de.publikator.beans.StatusMessage;
import eu.dariah.de.publikator.dao.CollectionDAO;
import eu.dariah.de.publikator.dao.DAOFactory;
import eu.dariah.de.publikator.dao.LanguageDAO;
import eu.dariah.de.publikator.seafile.SeafileLibrary;
import eu.dariah.de.publikator.servicebeans.ClientsServiceBean;
import eu.dariah.de.publikator.servicebeans.SeafileClient;
import eu.dariah.de.publikator.utils.HTTPWrapperUtils;
import eu.dariah.de.publikator.utils.HTTPWrapperUtils.WrapperResponse;
import eu.dariah.de.publikator.utils.I18NUtils;
import eu.dariah.de.publikator.utils.StorageUtil;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

/**
 * - #%L DARIAHDE :: Publikator Standalone
 * 
 * %% Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de) %%
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 *
 * @author Johannes
 */
@WebServlet(urlPatterns = {"/mainView"})
public class MainView extends BaseServlet {

  private static final long serialVersionUID = 1L;

  private static final Logger LOG = Logger.getLogger(MainView.class.getName());
  private static final String views[] =
      {"token", "deleteCollection", "deleteSubCollection", "getPublishInfo", "getPublishStatus",
          "listCollections", "publishCollection", "publishVersion", "createNew", "language",
          "refreshSession", "publishInfoProxy", "publishStatusProxy"};

  /**
   * @see HttpServlet#HttpServlet()
   */
  public MainView() {
    super(new HashSet<String>(Arrays.asList(views)));
    this.clients = new ClientsServiceBean();
  }

  /*
   * (non-Javadoc)
   * 
   * @see eu.dariah.de.publikator.servlets.BaseServlet#index(javax.servlet.http.HttpServletRequest,
   * javax.servlet.http.HttpServletResponse)
   */
  @Override
  protected void index(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String selectedCollection = request.getParameter("storageId");

    request.setAttribute("storageId", selectedCollection);

    // translation array
    request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));

    response.setCharacterEncoding("UTF-8");

    // if we already have the access token, we set it to true so that the JSP can remove the java
    // script call to the PDP server. we also remove the java script, if we are using no pdp and
    // are logged in locally.
    String pdpToken = getPDPTokenFromSession(request);
    if (pdpToken != null || !Configuration.getInstance().getConfig().isEnableAAI()) {
      request.setAttribute("hasAccessToken", true);
      request.setAttribute("accessToken", pdpToken);

      // we check here, if we can write to the storage, if we have a token or not. maybe we are
      // logged in locally and are using a non-pdp-storage implementation.
      StatusMessage message =
          StorageUtil.canWriteinStorage(this.clients.getDariahStorageClient(), pdpToken);
      if (!message.isSuccess()) {
        // we have a problem.
        displayErrorpage(request, response, message);
        return;
      }
    }

    RequestDispatcher rd = request.getRequestDispatcher("views/mainView.jsp");
    rd.forward(request, response);
  }


  /**
   * <p>
   * Handle the PDP token in a separate method. Yes this causes one redirect more, but we can hide
   * the access token from the URL
   * </p>
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void token(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String incomingAccessToken = request.getParameter("access_token");

    if (incomingAccessToken != null) {
      LOG.info("got incoming pdpAuthToken: " + incomingAccessToken);
      request.getSession().setAttribute("pdpAuthToken", incomingAccessToken);
    } else {
      throw new ServletException("pdpAuthToken is missing");
    }

    // this a client side redirect to hide the token a forward would leave the URL in the browser
    response.sendRedirect("mainView");
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void deleteCollection(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String storageToken = getPDPTokenFromSession(request);
    if (!doesParamExist(request, response, "collectionId")) {
      return;
    }
    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "collectionId")) {
      return;
    }

    String collectionId = request.getParameter("collectionId");
    String logID = StorageUtil.createRandomLogId();

    // if we are editing a collection item, then we need to remove it from the collection cache we
    // don't know what the user is doing on edit, this is the best we can do about caching
    CollectionDAO dao = DAOFactory.getDAOFactory(DAOFactory.REDIS).getCollectionDAO();
    if (dao.deleteCollectionFromCache(collectionId)) {
      LOG.info(logID + " Remove collection " + collectionId + " from collection cache");
    } else {
      LOG.severe(logID + " Failed to remove collection " + collectionId + " from collection cache");
    }

    LOG.info(logID + " deleteCollection pdptoken: " + storageToken);
    LOG.info(logID + " Delete Collection with ID: " + collectionId);

    ThreadPoolExecutor executor =
        (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");

    executor.execute(new DeleteCollectionWorker(collectionId, getUser(request).getEppn(), true,
        storageToken, logID));

    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write("ok, deleted " + collectionId);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void deleteSubCollection(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String storageToken = getPDPTokenFromSession(request);
    if (!doesParamExist(request, response, "fileStorageId")) {
      return;
    }
    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "fileStorageId")) {
      return;
    }
    String logID = StorageUtil.createRandomLogId();

    String collectionId = request.getParameter("fileStorageId");
    // the JS keeps the dariahstorage:thing, remove it
    collectionId = collectionId.substring(collectionId.lastIndexOf(":") + 1);

    LOG.info(logID + " deleteSubCollection pdptoken: " + storageToken);
    LOG.info(logID + " Delete Sub Collection with ID: " + collectionId);

    ThreadPoolExecutor executor =
        (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");

    executor.execute(new DeleteCollectionWorker(collectionId, "", false, storageToken, logID));

    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write("ok, deleted sub collection " + collectionId);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void getPublishInfo(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String storageToken = getPDPTokenFromSession(request);
    if (!doesParamExist(request, response, "collectionId")) {
      return;
    }
    String collectionId = request.getParameter("collectionId");
    String logID = StorageUtil.createRandomLogId();

    // Get (cached) info response from DH-publish.
    InfoResponse iResponse = null;
    try {
      // set the retries, we catch any exception here
      RetryPolicy retryPolicy = new RetryPolicy().withDelay(100, TimeUnit.MILLISECONDS)
          .withMaxRetries(3).retryOn(Exception.class);

      PublishClientWrapper.GetInfo gi =
          this.publishClient.new GetInfo(collectionId, storageToken, logID);

      iResponse = Failsafe.with(retryPolicy).get(() -> gi.call());
    } catch (Exception e) {
      LOG.severe(logID + " could not get publish info for collectionId " + collectionId);
      LOG.severe(ExceptionUtils.getStackTrace(e));
      response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("publish.info.failure");
      }

      return;
    }

    // Map to JSON.
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(Include.NON_EMPTY);

    response.setCharacterEncoding("UTF-8");
    response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);

    // Write response.
    mapper.writeValue(response.getOutputStream(), iResponse);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void getPublishStatus(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String storageToken = getPDPTokenFromSession(request);
    if (!doesParamExist(request, response, "collectionId")) {
      return;
    }
    String collectionId = request.getParameter("collectionId");
    String logID = StorageUtil.createRandomLogId();

    // Get status response from DH-publish.
    PublishResponse pResponse = null;
    try {
      // set the retries, we catch any exception here
      RetryPolicy retryPolicy = new RetryPolicy().withDelay(100, TimeUnit.MILLISECONDS)
          .withMaxRetries(3).retryOn(Exception.class);

      PublishClientWrapper.GetStatus gs =
          this.publishClient.new GetStatus(collectionId, storageToken, logID);

      pResponse = Failsafe.with(retryPolicy).get(() -> gs.call());

    } catch (Exception e) {
      LOG.severe(logID + " could not get publish status for collectionId " + collectionId);
      LOG.severe(ExceptionUtils.getStackTrace(e));
      response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);

      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("publish.status.failure");
      }

      return;
    }

    // Map to JSON.
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(Include.NON_EMPTY);

    // Write response.
    response.setCharacterEncoding("UTF-8");
    response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);
    mapper.writeValue(response.getOutputStream(), pResponse);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void listCollections(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String userId = getUser(request).getEppn();
    String pdpToken = getPDPTokenFromSession(request);
    String logID = StorageUtil.createRandomLogId();

    LOG.info(logID + " Listing collections");

    List<Collection> cont = null;
    try {
      cont = getCollectionContainers(userId,
          Configuration.getInstance().getConfig().isSkipPublishStatusCheck(), request.getSession(),
          getLocale(request), pdpToken, logID);

      ObjectMapper mapper = new ObjectMapper();
      // mapper.enable(SerializationFeature.INDENT_OUTPUT);
      mapper.setSerializationInclusion(Include.NON_EMPTY);
      response.setCharacterEncoding("UTF-8");
      response.setContentType(javax.ws.rs.core.MediaType.APPLICATION_JSON);
      mapper.writeValue(response.getOutputStream(), cont);

    } catch (NumberFormatException e) {
      /*
       * model.addAttribute("errormessage", I18NUtils.i18n(renderRequest) .get("error-storage"));
       * return "mainView";
       */
    } catch (IOException e) {
      /*
       * model.addAttribute("errormessage", I18NUtils.i18n(renderRequest) .get("error-storage") +
       * e.getMessage()); return "mainView";
       */
    }
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void publishCollection(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!doesParamExist(request, response, "collectionId")) {
      return;
    }

    String collectionId = request.getParameter("collectionId");
    String logID = StorageUtil.createRandomLogId();
    String eppn;
    String seafileToken = (String) request.getSession().getAttribute("seafileAuthToken");
    String pdpToken = BaseServlet.getPDPTokenFromSession(request);

    LOG.info(logID + " sftoken:  " + seafileToken);
    LOG.info(logID + " pdptoken: " + pdpToken);

    if (Configuration.getInstance().getConfig().isOverrideEppn()) {
      eppn = Configuration.getInstance().getConfig().getEppn();
    } else {
      eppn = getUser(request).getEppn();
    }

    boolean dryrun = Configuration.getInstance().getConfig().isDryrun();
    boolean debug = Configuration.getInstance().getConfig().isDebug();

    LOG.info(logID + " publishing collection with ID " + collectionId
        + (debug ? " from user " + getUser(request).getDisplayname() + " (eppn: " + eppn + ")" : "")
        + (dryrun ? " in dryrun mode" : ""));
    LOG.info(logID + " storageToken: " + pdpToken + ", seafileToken: " + seafileToken);

    if (dryrun) {
      this.clients.getPublishClient().publishSimulate(collectionId, pdpToken, seafileToken, logID);
    } else {
      this.clients.getPublishClient().publish(collectionId, pdpToken, seafileToken, logID);
    }

    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write("ok");
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void publishVersion(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String version = "";

    if (Configuration.getInstance().getConfig().isSkipPublishStatusCheck()) {
      version = "Connection to DH-publish disabled";
    } else {
      version = this.clients.getPublishClient().getVersion();
    }
    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write(version);
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void createNew(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // FIXME check if present
    String seafileLibraryId = request.getParameter("seafileLibraryId");
    String seafileToken = request.getParameter("seafileToken");

    String logID = StorageUtil.createRandomLogId();

    LOG.info(logID + " called createnew");

    String rdfContent = "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
        + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n";

    ByteArrayInputStream is = new ByteArrayInputStream(rdfContent.getBytes());
    String storageId = "";

    String pdpToken = getPDPTokenFromSession(request);
    LOG.info(logID + " storage token: " + pdpToken);

    try {
      // set the retries, we catch any exception here. in future we can be more specific or exit
      // early if there is a non fixable state
      RetryPolicy retryPolicy = new RetryPolicy().withDelay(250, TimeUnit.MILLISECONDS)
          .withMaxRetries(3).retryOn(Exception.class);

      DariahStorageClientWrapper.CreateFile cf =
          this.storageClient.new CreateFile(is, "text/plain; charset=utf-8", pdpToken, logID);

      storageId = Failsafe.with(retryPolicy).get(() -> cf.call());

    } catch (Exception e) {
      if (Configuration.getInstance().getConfig().isStatsdEnable()) {
        StatsDClient client = StatsdClient.getInstance().getClient();
        client.incrementCounter("storage.write.failure");
      }

      LOG.severe(logID + " Problem saving new collection");
      LOG.severe(ExceptionUtils.getMessage(e));

      displayErrorpage(request, response, "Problem saving new collection", e, null);

      return;
    }

    Collection collection = new Collection(getUser(request).getEppn(), storageId);
    CollectionDAO dao = DAOFactory.getDAOFactory(DAOFactory.REDIS).getCollectionDAO();
    if (false == dao.saveCollection(collection)) {
      throw new ServletException("Error in saving new collection");
    }

    // add seafileLibId, in case a seafile lib is managed
    if (seafileLibraryId != null) {
      SeafileClient sfclient = this.clients.getSeafileClient().setEncryptedToken(seafileToken);
      SeafileLibrary info = sfclient.getLibraryInfo(seafileLibraryId);
      sfclient.updateNewCollectionInfo(storageId, seafileLibraryId, info.getName(), "/");
    }

    // send a HTTP redirect request to the client. it is more convenient to force a redirect, on
    // page refresh it does not submit the form again and we the URL in the browser bar
    response.sendRedirect("editView?storageId=" + storageId);
  }

  /**
   * <p>
   * Changes the language of the application on the fly
   * </p>
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void language(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // get out here if there has no locale been supplied
    if (!doesParamExist(request, response, "locale")) {
      return;
    }

    Locale locale = null;
    String localeCode = request.getParameter("locale");
    // handle de or de_DE stuff
    if (doesParamExist(request, "country")) {
      String countryCode = request.getParameter("country");
      locale = new Locale(localeCode, countryCode);
    } else {
      locale = new Locale(localeCode);
    }

    request.getSession(false).setAttribute("locale", locale);

    if (getUser(request) != null) {
      // store the selected language if logged in
      LanguageDAO lang = DAOFactory.getDAOFactory(DAOFactory.REDIS).getLanguageDAO();
      lang.setUserlanguage(getUser(request).getEppn(), locale);
    }
    // try to redirect to the previous action
    String referrer = request.getHeader("referer");
    if (referrer != null && referrer.length() > 5) {
      response.sendRedirect(referrer);
    } else {
      response.sendRedirect("mainView");
    }
  }

  /**
   * <p>
   * keeps the user session alive, called via ajax from time to time
   * </p>
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void refreshSession(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // some dummy operation on session to keep it alive
    request.getSession(false).getLastAccessedTime();

    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write("OK");
    response.setStatus(200);
  }

  /**
   * <p>
   * if debug enabled, there is a link for getting publish info. This methods will perform the call
   * with the credentials.
   * </p>
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void publishInfoProxy(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String logID = StorageUtil.createRandomLogId();
    String pdpToken = getPDPTokenFromSession(request);

    try {
      WrapperResponse result = HTTPWrapperUtils.getPublishInfo(id, pdpToken, logID);
      response.setContentType(result.getMimeType());
      response.getWriter().write(result.getContent());
      response.setStatus(result.getStatus());
    } catch (Exception e) {
      response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
      response.getWriter().write("Problem fetching info from dhpublish");
      response.setStatus(500);

      LOG.severe("could not get dhpublish info for storageId " + id);
      LOG.severe(ExceptionUtils.getStackTrace(e));
    }
  }

  /**
   * <p>
   * if debug enabled, there is a link for getting publish status. This methods will perform the
   * call with the credentials.
   * </p>
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void publishStatusProxy(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!ensureParameterIsPresentOrDisplayErrorpage(request, response, "storageId")) {
      return;
    }

    String id = request.getParameter("storageId");
    String logID = StorageUtil.createRandomLogId();
    String pdpToken = getPDPTokenFromSession(request);

    try {
      WrapperResponse result = HTTPWrapperUtils.getPublishStatus(id, pdpToken, logID);
      response.setContentType(result.getMimeType());
      response.getWriter().write(result.getContent());
      response.setStatus(result.getStatus());
    } catch (Exception e) {
      response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
      response.getWriter().write("Problem fetching info from dhpublish");
      response.setStatus(500);

      LOG.severe("could not get dhpublish status for storageId " + id);
      LOG.severe(ExceptionUtils.getStackTrace(e));
    }
  }

  /**
   * <p>
   * Gets storage ID, title, and description from OwnStorage for further use and fetching info
   * status later.
   * </p>
   *
   * @param userId
   * @param skipPublishStatusCheck
   * @param prefs
   * @return
   * @throws NumberFormatException
   * @throws IOException
   * @throws SystemException
   */
  private List<Collection> getCollectionContainers(String userId, boolean skipPublishStatusCheck,
      HttpSession session, Locale locale, String pdpToken, String logID)
      throws NumberFormatException, IOException {

    CollectionDAO dao = DAOFactory.getDAOFactory(DAOFactory.REDIS).getCollectionDAO();

    List<Collection> collectionsdao = dao.getCollectionsByUserId(userId);
    List<Collection> collections = new ArrayList<Collection>();

    // get the translated new title string if the user did not enter a title yet
    String unnamedCollectionTitle = I18NUtils.getTranslatedString("new-collection-notitle", locale);

    // now we have fetch all additional information from the dariah storage. Collections are
    // immutable now, so we need to initialize new objects here
    long startTime = System.currentTimeMillis();
    for (Collection daoCollection : collectionsdao) {
      // try to load the collection from cache, if it is not present, then col will be null
      Collection col = dao.loadCollectionFromCache(daoCollection.getStorageId());
      if (col == null) {
        // we do not have the collection in cache, fetch it via RDF descriptions
        col = StorageUtil.fetchTitleAndDescription(this.storageClient, daoCollection,
            unnamedCollectionTitle, pdpToken, logID, locale);
        // add the item to the cache, but only if the error attribute is an empty string. we do not
        // cache error items
        // NOTE: if this item is a new item, without a title, the current temporary title
        // (unnamedCollectionTitle) is saved to cache. If the user changes the language, the old
        // title will stay until the user edits the collection
        if (col.getError().length() == 0) {
          boolean result = dao.saveCollectionCache(col);
          if (!result) {
            LOG.severe(
                logID + " Failed to add collection to the cache " + daoCollection.getStorageId());
          } else {
            LOG.info(logID + " " + daoCollection.getStorageId() + " added to collection cache");
          }
        }
      }

      collections.add(col);
    }

    long elapsedTime = System.currentTimeMillis() - startTime;
    LOG.info(logID + " MainView: Fetching Title & description took " + elapsedTime + "ms");

    // sort alphabetically by title. At this point we have only title and description
    List<Collection> colSorted = Arrays.asList(
        collections.stream().sorted((s1, s2) -> s1.getTitle().compareToIgnoreCase(s2.getTitle()))
            .toArray(Collection[]::new));

    return colSorted;
  }

}
