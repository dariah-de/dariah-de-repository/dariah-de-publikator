package eu.dariah.de.publikator.utils;

import java.io.StringWriter;
import java.net.URI;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import eu.dariah.de.publikator.backend.Configuration;
import info.textgrid.middleware.tgpublish.api.DHPublishService;

/**
 * - #%L DARIAHDE :: Publikator Standalone %%
 * 
 * Copyright (C) 2019 SUB Göttingen (https://www.sub.uni-goettingen.de) %%
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes Wrapper around some DH Publish calls. It needs HTTP authentication now. (No, not
 *         anymore :-) Do we still need this class, anyway?
 */
public class HTTPWrapperUtils {

  /**
   * @param storageId
   * @param pdpToken
   * @param logID
   * @return
   * @throws Exception
   */
  public static WrapperResponse getPublishInfo(String storageId, String pdpToken, String logID)
      throws Exception {
    return performRequest(
        Configuration.getInstance().getConfig().getDhpublishUrl() + storageId + "/info", pdpToken,
        logID);
  }

  /**
   * @param storageId
   * @param pdpToken
   * @param logID
   * @return
   * @throws Exception
   */
  public static WrapperResponse getPublishStatus(String storageId, String pdpToken, String logID)
      throws Exception {
    return performRequest(
        Configuration.getInstance().getConfig().getDhpublishUrl() + storageId + "/status",
        pdpToken, logID);
  }

  /**
   * @param baseURL
   * @param pdpToken
   * @param logID
   * @return
   * @throws Exception
   */
  private static WrapperResponse performRequest(String baseURL, String pdpToken, String logID)
      throws Exception {

    WrapperResponse resp = new WrapperResponse();

    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpGet httpGet = new HttpGet(baseURL);

    httpGet.addHeader(DHPublishService.STORAGE_TOKEN, pdpToken);
    httpGet.addHeader(DHPublishService.TRANSACTION_ID, logID);

    URI uri = new URIBuilder(httpGet.getURI()).build();
    httpGet.setURI(uri);
    CloseableHttpResponse response = httpclient.execute(httpGet);

    try {
      HttpEntity entity = response.getEntity();

      ContentType contentType = null;
      if (entity != null) {
        contentType = ContentType.get(entity);
      }

      resp.setStatus(response.getStatusLine().getStatusCode());
      if (contentType != null && contentType.getMimeType() != null) {
        resp.setMimeType(contentType.getMimeType());
      }

      StringWriter writer = new StringWriter();
      IOUtils.copy(entity.getContent(), writer, "UTF-8");
      resp.setContent(writer.toString());

      EntityUtils.consume(entity);
    } finally {
      response.close();
    }

    return resp;
  }

  /**
  *
  */
  public static class WrapperResponse {

    private int status = 500;
    private String content;
    private String mimeType = "text/plain";

    /**
     * @return
     */
    public int getStatus() {
      return this.status;
    }

    /**
     * @return
     */
    public String getContent() {
      return this.content;
    }

    /**
     * @return
     */
    public String getMimeType() {
      return this.mimeType;
    }

    /**
     * @param status
     */
    public void setStatus(int status) {
      this.status = status;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
      this.content = content;
    }

    /**
     * @param mimeType
     */
    public void setMimeType(String mimeType) {
      this.mimeType = mimeType;
    }

  }

}
