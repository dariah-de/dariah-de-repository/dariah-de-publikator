<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- we must not render the page completely if we do not have the access token yet. Fixes font issue on Safari --%>
<c:choose>
<c:when test = "${(not hasAccessToken) and (enableAAI)}">
		<script>
 			if (window.location.hash) {
 				window.location = window.location.origin
 						+ window.location.pathname + "?view=token&"
 						+ window.location.hash.substr(1);
 			} else {
 				window.location = "${applicationScope.pdpTokenServerUrl}"
 						+ window.location.origin + window.location.pathname;
 			}
		</script>
</c:when>
<c:otherwise>
<%-- Main Page, displayed if we have an access token --%>
<%@ include file="header.jsp" %>

	<div class="publish-gui">

		<!-- Show only if Seafile enabled ================================== -->

		<c:if test="${seafileEnabled}">
			<div class="seafileActionBox">
				<strong>Seafile:</strong>
				<c:if test="${not empty seafileAuthToken}">
					${i18n['seafile-connected']} |
					<a href="seafileView" class="btn btn-small"
					title="${i18n['tt-seafile-connected']}">${i18n['seafile-manage-libraries']}</a>
				</c:if>
				<c:if test="${empty seafileAuthToken}">
					${i18n['seafile-not-connected']} |
					<a href="seafileView" class="btn btn-small" title="${i18n['tt-seafile-not-connected']}"> <i class="icon-signin"></i>${i18n['seafile-connect']}</a>
				</c:if>
				<a href="https://sftest.de.dariah.eu/docs/" target="_blank"
					title="${i18n['seafile-help-title']}"><i
					class="icon-question-sign"></i> ${i18n['seafile-help']}
				</a>
			</div>
		</c:if>

        <!-- Status messages =============================================== -->

        <div id="dariah-status-message-area" class="hide">
            <div id="dariah-status-message-text"></div>
            <br/>
        </div>

        <!-- Topic Text ==================================================== -->

		<h1>${i18n['publikator']}: ${i18n['collection-overview']}<c:if test="${applicationScope.debug}">&nbsp; <span class="badge">DEBUG</span></c:if><c:if test="${not empty badgeText}">&nbsp; <span class="badge">${badgeText}</span></c:if></h1>

		<p>${i18n['collection-overview-description']}</p>

        <br/>

        <!-- TEST and DEV messages ========================================= -->

  		<c:if test="${(applicationScope.instanceName eq 'TEST') || (applicationScope.instanceName eq 'DEVELOP')}">
		<div class="well">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<p class="text-success">
				<c:if test="${(applicationScope.instanceName eq 'TEST')}">
					<strong>${i18n['collection-overview-instance-msg-test']}</strong>
				</c:if>
				<c:if test="${(applicationScope.instanceName eq 'DEVELOP')}">
					<strong>${i18n['collection-overview-instance-msg-develop']}</strong>
				</c:if>
			</p>
		</div>
		</c:if>

		<!-- New Collection, Reveal and Manage Token Buttons =============== -->

		<div>
			<form action="mainView?view=createNew" method="POST">
				<button class="btn btn-primary" title="${i18n['tt-new-collection']}">
					<i class="icon-plus-sign"></i> ${i18n['new-collection']}
				</button>

                <c:if test="${(applicationScope.instanceName eq 'DEVELOP')}">
				    &nbsp;
				    <a href="#" id="revtokButton" class="btn" title="${i18n['tt-reveal-token']}">
				        <i class="icon-lock"></i> ${i18n['reveal-token']}
				    </a>
				    &nbsp;
				    <a href="${applicationScope.pdpTokenManagerUrl}" class="btn" title="${i18n['tt-manage-tokens']}" target="_blank">
					    <i class="icon-wrench"></i> ${i18n['manage-tokens']}
				    </a>
                </c:if>

			</form>
		</div>

		<!-- Reveal token or not reveal token? -->
		<div id="revtokAlert" class="hide">
			<br/>
			<div class="well">
				<p>
					<button type="button" class="close" id="revtokAlertClose">&times;</button>
					${i18n['reveal-token-prefix']} &nbsp;
					<strong>${requestScope.accessToken}</strong> &nbsp;
					<a id="revtokClipboard" class="no_uderline copy_krams" data-clipboard-text="${requestScope.accessToken}" title="${i18n['tt-copy-token-to-clipboard']}" href="#"><svg class="icon no_uderline"><use xlink:href="#icon-copy"></use></svg></a>
					<br/><br/>
					${i18n['reveal-token-suffix']}
				</p>
			</div>
		</div>

		<!-- Gathering collections -->

		<h4 id="gathering">
			<br/>
			${i18n['gathering-collections']} <img src="https://res.de.dariah.eu/dhrep/img/spinning-flower_slow.gif" alt="${i18n['gathering-collections']}" width="24" />
		</h4>

		<!-- Collections-View as an Accordion ============================== -->

		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>
	</div>

	<script src="js/dariah-main-view.js?${applicationScope.buildTimestamp}"></script>

	<script>
		// Setting i18n strings straight!
		const i18nMap = {
			<c:forEach items="${i18n}" var="entry">
				'${entry.key}' : '${esc:escapeEcmaScript(entry.value)}',
			</c:forEach>
		};

		// Constants.
		const mainView = new DariahMainView.default({
			i18nMap,
			elementId: 'accordion',
			listCollectionsUrl: 'mainView?view=listCollections',
			pidServiceUrl: '${applicationScope.pidServiceUrl}',
 			collectionRegistryUrl: '${applicationScope.collectionRegistryUrl}',
			genericSearchUrl: '${applicationScope.genericSearchUrl}',
			editviewURL: 'editView',
			dhcrudUrl: '${applicationScope.dhcrudUrl}',
			mainviewURL: 'mainView',
			debug: ${applicationScope.debug},
			refresh: ${applicationScope.refresh},
			serviceTimeout: ${applicationScope.serviceTimeout},
			selectedCollectionUri: '${requestScope.storageId}',
			getPublishInfoUrl: 'mainView?view=getPublishInfo',
			deleteCollectionUrl: 'mainView?view=deleteCollection',
			publishCollectionUrl: 'mainView?view=publishCollection',
			publishVersionUrl: 'mainView?view=publishVersion',
			getPublishStatusUrl: 'mainView?view=getPublishStatus',
			publishimgpath : 'https://res.de.dariah.eu/dhrep/img',
		});

		// Reveal or not reveal storage token.
		$('#revtokButton').click(()=>{
			if ($('#revtokAlert').hasClass('hide')) {
		    	$('#revtokAlert').attr('class','show');
				$('#revtokButton').attr('title',"${i18n['tt-hide-token']}");
				$('#revtokButton').contents()[2].nodeValue=" ${i18n['hide-token']}";
			} else {
				$('#revtokAlert').attr('class','hide');
				$('#revtokButton').attr('title',"${i18n['tt-reveal-token']}");
				$('#revtokButton').contents()[2].nodeValue=" ${i18n['reveal-token']}";
			}
		});
		$('#revtokAlertClose').click(()=>{
		    $('#revtokAlert').attr('class','hide');
			$('#revtokButton').attr('title',"${i18n['tt-reveal-token']}");
			$('#revtokButton').contents()[2].nodeValue=" ${i18n['reveal-token']}";
		});
	</script>

<%@ include file="footer.jsp" %>
</c:otherwise>
</c:choose>
