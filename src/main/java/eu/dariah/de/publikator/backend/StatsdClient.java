package eu.dariah.de.publikator.backend;

import java.util.logging.Logger;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 * 
 * StatsdClient Singleton
 * 
 */
public class StatsdClient {
	private static StatsdClient instance;
	private static StatsDClient statsdClient;
	
	private StatsdClient () {
			statsdClient = new NonBlockingStatsDClient("publikator", 
					Configuration.getInstance().getConfig().getStatsdHostname(), 
					Configuration.getInstance().getConfig().getStatsdPort());	
			LOG.info("Inizalised statsdclient on " + Configuration.getInstance().getConfig().getStatsdHostname()
					+" Port: "+ Configuration.getInstance().getConfig().getStatsdPort());
	}
	
	private static final Logger	LOG	= Logger.getLogger(StatsdClient.class.getName())	;
	
	public static final StatsdClient getInstance () {
		if (StatsdClient.instance == null) {
			StatsdClient.instance = new StatsdClient();
		}
		return StatsdClient.instance;
	}	
	
	public StatsDClient getClient() {
		return statsdClient;
	}
}
