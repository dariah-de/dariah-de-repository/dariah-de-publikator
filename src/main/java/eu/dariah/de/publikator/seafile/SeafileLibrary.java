package eu.dariah.de.publikator.seafile;

/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeafileLibrary {

	private String name;
	private String desc;
	private String id;
	private String type;
	private long size;
	private SeafileFile[] files;
	private String dariahStorageId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public SeafileFile[] getFiles() {
		return files;
	}
	public void setFiles(SeafileFile[] files) {
		this.files = files;
	}
	public String getDariahStorageId() {
		return dariahStorageId;
	}
	public void setDariahStorageId(String dariahStorageId) {
		this.dariahStorageId = dariahStorageId;
	}

	
}
