package eu.dariah.de.publikator.dao;

import java.util.Locale;

public interface LanguageDAO {

	/**
	 * Retrieves the stored language 
	 * @param userId
	 * @return
	 */
	public Locale getUserlanguage(String userId);
	
	/**
	 * Stores the selected language persistent
	 * @param locale
	 * @param userId
	 * @return
	 */
	public boolean setUserlanguage(String userId, Locale locale);
	
}
