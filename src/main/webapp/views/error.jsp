<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jsp" %>

<c:url value="${requestScope.details}" var="mailbody"></c:url>
<c:url value="${requestScope.errorheadline}" var="mailHeadline"></c:url>

<h2 style="color: red;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ${i18n['misc-error-headline']}</h2>
<h3>${requestScope.errorheadline}</h3>
<div id="error">
${requestScope.details}
</div>
<hr/>
<p>${i18n['misc-error-general']}&nbsp;<a href="mailto:${mailOfContact}?subject=${i18n['publikator']}%20${i18n['status-error']}%20${mailHeadline}&body=${mailbody}">${mailOfContact}</a></p>

<c:if test="${debug && stacktrace != null}">
<div id="stacktrace">
<strong>Stacktrace</strong>
<pre>
${requestScope.stacktrace}
</pre>
</div>
</c:if>

<%@ include file="footer.jsp" %>
