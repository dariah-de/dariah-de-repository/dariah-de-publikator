package eu.dariah.de.publikator.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.timgroup.statsd.StatsDClient;
import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.backend.StatsdClient;

/**
 * - #%L DARIAHDE :: Publikator Standalone %% Copyright (C) 2017 SUB Göttingen
 * (https://www.sub.uni-goettingen.de) %% Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 * 
 *         Small servlet for gathering upload statistics and do the reporting to statsD, if enabled.
 */

@WebServlet(urlPatterns = {"/status"})
@MultipartConfig
public class StatsdServlet extends BaseServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 3161147272698860322L;

  private static final Logger LOG = Logger.getLogger(StatsdServlet.class.getName());

  private static final String views[] = {};

  public StatsdServlet() {
    super(new HashSet<String>(Arrays.asList(views)));
  }


  @Override
  protected void index(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    Part filePart = request.getPart("status");

    if (filePart == null) {
      LOG.severe("huh? status message is missing");
      return;
    }

    InputStream is = filePart.getInputStream();
    if (is == null) {
      LOG.severe("huh? the JSON message is missing");
      return;
    }

    StringWriter writer = new StringWriter();
    IOUtils.copy(is, writer, "UTF-8");
    String jsonStatus = writer.toString();

    boolean statsdEnabled = Configuration.getInstance().getConfig().isStatsdEnable();

    JSONParser jsonParser = new JSONParser();
    try {
      JSONObject status = (JSONObject) jsonParser.parse(jsonStatus);

      String logID = (String) status.get("transactionID");
      JSONArray entries = (JSONArray) status.get("fileStatus");
      LOG.info("Upload status data from client with logID " + logID + ":");

      long bytes = 0;
      int noOK = 0;
      int noFailure = 0;
      for (int i = 0; i < entries.size(); i++) {
        JSONObject entry = (JSONObject) entries.get(i);
        long size = (long) entry.get("size");
        boolean ok = (boolean) entry.get("status");
        String msg = (String) entry.get("message");

        if (ok) {
          LOG.info(logID + " Uploaded file with size " + size / 1024 + " KiB");
          bytes += size;
          noOK++;
        } else {
          LOG.info(
              logID + " Failed to upload file with size " + size / 1024 + " KiB. Reason: " + msg);
          noFailure++;
        }

        if (statsdEnabled && ok) {
          StatsDClient client = StatsdClient.getInstance().getClient();
          client.incrementCounter("files");
          client.count("filesize", size);
        } else if (statsdEnabled && !ok) {
          StatsDClient client = StatsdClient.getInstance().getClient();
          client.incrementCounter("upload.failure");
        }
      }
      LOG.info(logID + " Uploaded " + (noOK + noFailure) + " files in this request. "
          + "OK: " + noOK + ", failed: " + noFailure + ". Total upload success size: "
          + bytes / 1024 + " KiB");

    } catch (ParseException e) {
      e.printStackTrace();
      LOG.severe("Problem parsing JSON data: " + ExceptionUtils.getMessage(e));
    }
    response.setContentType(javax.ws.rs.core.MediaType.TEXT_PLAIN);
    response.getWriter().write("OK");
  }

}
