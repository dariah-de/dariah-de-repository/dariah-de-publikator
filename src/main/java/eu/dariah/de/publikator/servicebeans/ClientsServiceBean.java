package eu.dariah.de.publikator.servicebeans;

import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.dariah.de.publikator.backend.Configuration;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.dhpublish.client.PublishClient;
import info.textgrid.middleware.tgpublish.api.DHPublishService;

/**
 * helper to create only one webtarget for each service in bean lifecycle.
 * 
 * would not work if the same portlet is used in same liferay with different dhpublish locations
 * 
 * compare http://cxf.apache.org/docs/jax-rs-client-api.html#JAX-RSClientAPI- ThreadSafety for
 * thread safety and performance implications
 * 
 * @author ubbo
 *
 */
public class ClientsServiceBean {

  // **
  // CLASS
  // **

  private PublishClient publishClient;
  private Client seafileClient = ClientBuilder.newClient().property("thread.safe.client", "true");
  private DariahStorageClient storageClient = new DariahStorageClient();

  /**
   * @param prefs
   * @return
   */
  public DariahStorageClient getDariahStorageClient() {
    return this.storageClient
        .setStorageUri(URI.create(Configuration.getInstance().getConfig().getDariahStorageUrl()));
  }

  /**
   * @param prefs
   * @return
   */
  public SeafileClient getSeafileClient() {
    WebTarget seafileTarget = this.seafileClient.register(JacksonJsonProvider.class)
        .target(Configuration.getInstance().getConfig().getSeafileUrl());
    return new SeafileClient(seafileTarget,
        Configuration.getInstance().getConfig().getSeafileTokenSecret(), getDariahStorageClient());
  }

  /**
   * @param prefs
   * @return
   */
  public PublishClient getPublishClient() {
    if (this.publishClient == null) {
      this.publishClient =
          new PublishClient(Configuration.getInstance().getConfig().getDhpublishUrl(),
              Configuration.getInstance().getConfig().getServiceTimeout());
    }
    return this.publishClient;
  }

  /**
   * called from PortletConfigController, when conf changes
   */
  public void reset() {
    this.publishClient = null;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @return
   * @deprecated
   */
  @Deprecated
  private static TLSClientParameters getTLSClientParameters() {

    TLSClientParameters result = new TLSClientParameters();

    // Create a trust manager that does not validate certificate chains
    TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {

      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      public void checkClientTrusted(X509Certificate[] certs, String authType) {}

      public void checkServerTrusted(X509Certificate[] certs, String authType) {}
    }};

    // create a hostname verifier, which does not verify host names in SSL certificates ;-)
    HostnameVerifier nullHostnameVerifier = new HostnameVerifier() {
      @Override
      public boolean verify(String hostname, SSLSession session) {
        return true;
      }
    };

    SSLContext sc = null;
    // Install the all-trusting trust manager
    try {
      sc = SSLContext.getInstance("TLS");
      sc.init(null, trustAllCerts, new SecureRandom());
    } catch (NoSuchAlgorithmException | KeyManagementException e) {
      ;
    }

    result.setHostnameVerifier(nullHostnameVerifier);
    result.setSslContext(sc);

    return result;
  }

  /**
   * @param theEndpoint
   * @param theTimeout
   * @return
   * @deprecated
   */
  @Deprecated
  private static DHPublishService getTrustAllCertsClient(String theEndpoint, int theTimeout) {

    DHPublishService result = JAXRSClientFactory.create(theEndpoint + "/", DHPublishService.class);

    // Set timeout first.
    HTTPConduit conduit = WebClient.getConfig(result).getHttpConduit();
    HTTPClientPolicy policy = new HTTPClientPolicy();
    policy.setReceiveTimeout(theTimeout);
    conduit.setClient(policy);
    conduit.setTlsClientParameters(getTLSClientParameters());

    return result;
  }

}
