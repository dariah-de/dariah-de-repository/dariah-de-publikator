<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file="header.jsp" %>


	<link type="text/css" rel="stylesheet" href="css/main.css?${applicationScope.buildTimestamp}"></link>	
	
	<c:if test="${empty authToken}">

		<script>
			window.location = "https://sftest.de.dariah.eu/shib-login?next=/api2/dariah-publish-token/";
		</script>
	</c:if>

</head>
<body>

	<div class="seafileActionBox">
		<strong>Seafile:</strong>
		<c:if test="${not empty seafileAuthToken}">		
			${i18n['seafile-connected']} |
			<button class="btn btn-small" onclick="location.reload();"><i class="icon-refresh"></i> ${i18n['seafile-reload-libraries']}</button>	
		</c:if>			
		<c:if test="${empty seafileAuthToken}">
			${i18n['seafile-not-connected']} | 
			<button class="btn btn-small">
				<i class="icon-signin"></i> ${i18n['seafile-connect']}
			</button>
		</c:if>
		| <a href="https://sftest.de.dariah.eu/docs/" target="_blank" title="${i18n['seafile-help-title']}"><i class="icon-question-sign"></i> ${i18n['seafile-help']}</a>
	</div>

	<h1>${i18n['seafile-your-libraries']}</h1>
	
	<a href="mainView" class="btn btn-default"><i class="icon-arrow-left"></i> ${i18n['back']}</a>

	<c:if test="${newCreatedCollectionId != null}" >
		<div style="clear:both; padding: 0.5em;"></div>
		<div class="alert alert-success">
		
			<h4>Success</h4>
				A new Publish-GUI collection has been created, click here to edit:
			<br/>
			
			<a href="editView?storageId=${newCreatedCollectionId}" class="btn btn-success" style="margin-right: 20px;">
				<i class="icon-pencil"> ${i18n['edit']}</i>
			</a>
		</div>
	</c:if>
	

	<c:if test="${not empty seafileAuthToken}">
		
		<div id="myToggler" class="alloy-accordion">
			<c:forEach items="${seafileLibraries}" var="lib">
			
				<h4 class="header toggler-header toggler-header-collapsed arrow">${lib.name}
					<c:if test="${not empty lib.dariahStorageId}">
						(${i18n['seafile-lib-in-publishgui']})
					</c:if>
				</h4>
				<div class="content toggler-content toggler-content-collapsed">
					<dl>
						<!-- <dt>id</dt><dd>${lib.id}</dd> -->
						<dt>${i18n['seafile-lib-name']}</dt><dd>${lib.name}</dd>
						<!-- 
						<dt>${i18n['seafile-lib-description']}</dt><dd>${lib.desc}</dd>
						-->
						<dt>${i18n['status']}</dt>
						<dd>
							<c:if test="${not empty lib.dariahStorageId}">
								${i18n['seafile-lib-in-publishgui']}
							</c:if>						
							<c:if test="${empty lib.dariahStorageId}">
								${i18n['seafile-lib-not-in-publishgui']}
							</c:if>
						</dd>
						
						<dt>${i18n['seafile-lib-content']}</dt>
						<dd>
							<ul>
							<c:forEach items="${lib.files}" var="file">
								<li>${file.name} - ${file.type}</li>
							</c:forEach>
							</ul>
						</dd>
					</dl>

					<hr/>

					<c:if test="${not empty lib.dariahStorageId}">
						<a href="editView?storageId=${lib.dariahStorageId}" class="btn btn-primary" style="margin-right: 20px;">
							<i class="icon-pencil"></i>	${i18n['edit']}
						</a>
						<a class="btn" href="https://sftest.de.dariah.eu/#common/lib/${lib.id}/" target="_blank">${i18n['seafile-view-webinterface']}</a>
	
					</c:if>
					
					<c:if test="${empty lib.dariahStorageId}">
						<form action="mainView?view=createNew" method="POST">
							<input type="hidden" name="seafileLibraryId" value="${lib.id}" />
							<input type="hidden" name="seafileToken" value="${authToken}" />
							<button class="btn btn-primary" style="margin-right: 20px;" >
								<i class="icon-plus-sign"></i> ${i18n['seafile-add-to-publishgui']}
							</button>
							<a class="btn" href="https://sftest.de.dariah.eu/#common/lib/${lib.id}/" target="_blank">${i18n['seafile-view-webinterface']}</a>
						</form>	
					</c:if>				

				</div>

			</c:forEach>
		</div>
	</c:if>


<%@ include file="footer.jsp" %>
	