package eu.dariah.de.publikator.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import eu.dariah.de.publikator.backend.Configuration;
import eu.dariah.de.publikator.utils.I18NUtils;

/**
 * - #%L DARIAHDE :: Publikator Standalone
 * 
 * %% Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de) %%
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * This servlet just displays the landing page, it is that simple
 * 
 * @author Johannes
 */

@WebServlet(urlPatterns = {"/welcome"})
public class LandingPage extends BaseServlet {

  /**
   * 
   */
  public LandingPage() {
    super(new HashSet<String>(Arrays.asList(views)));
  }

  private static final long serialVersionUID = -8778353162895802572L;
  // has no other views
  private static final String views[] = {};

  /*
   * (non-Javadoc)
   * 
   * @see eu.dariah.de.publikator.servlets.BaseServlet#index(javax.servlet.http.HttpServletRequest,
   * javax.servlet.http.HttpServletResponse)
   */
  @Override
  protected void index(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // do we want to skip the landing page? then just redirect to mainView. also if we are already
    // logged in we can skip this
    if (Configuration.getInstance().getConfig().isSkipLandingPage()
        || isUserAuthenticated(request)) {
      response.sendRedirect("mainView");
    } else {
      // no, then display the landingPage.jsp
      request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));

      response.setCharacterEncoding("UTF-8");

      RequestDispatcher rd = request.getRequestDispatcher("views/landingPage.jsp");
      rd.forward(request, response);
    }
  }

}
