package eu.dariah.de.publikator.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.dariah.de.publikator.servicebeans.SeafileClient;
import eu.dariah.de.publikator.utils.I18NUtils;

/**-
 * #%L
 * DARIAHDE :: Publikator Standalone
 * %%
 * Copyright (C) 2017 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 * @author Johannes
 */
@WebServlet("/seafileView/*")
public class SeafileView extends BaseServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger	LOG	= Logger.getLogger(SeafileView.class.getName());
  
	private static final String urls[] = {
			 "seafileDownload"
			 };	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeafileView() {
        super(new HashSet<String>(Arrays.asList(urls)));
    }
    
    protected void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		String incomingAuthToken = request.getParameter("authToken");

    		String authToken;

    		if (incomingAuthToken != null) {
    			// save authToken to session
    			request.getSession().setAttribute("seafileAuthToken", incomingAuthToken);
    			authToken = incomingAuthToken;
    			LOG.info("sf-token from request: " + authToken);
    		} else {
    			// try to get authToken from session
    			authToken = (String) request.getSession().getAttribute("seafileAuthToken");
    			LOG.info("sf-token from session: " + authToken);
    		}

    		if (authToken != null) {
    			String userId = getUser(request).getEppn();

    			SeafileClient sfclient = clients.getSeafileClient().setEncryptedToken(authToken);

    			request.setAttribute("seafileLibraries", sfclient.getLibraries(getSeafileLib2StorageMapping(userId)));
    			request.setAttribute("authToken", authToken);

    		}

    		request.setAttribute("i18n", I18NUtils.i18n(getLocale(request)));

    }
    
	/**
	 * @param userId
	 * @param prefs
	 * @return
	 */
	private HashMap<String, String> getSeafileLib2StorageMapping(String userId) {
//FIXME new backend
	/*	HashMap<String, String> libId2storageId = new HashMap<String, String>();
		List<Collection> collections = CollectionLocalServiceUtil.findByUser(userId);

		List<CollectionContainer> collectionContainers = new ArrayList<CollectionContainer>(collections.size());

		for (int i = 0; i < collections.size(); i++) {
			CollectionContainer container = new CollectionContainer(collections.get(i));

			// TODO: really clean this mess up, storageclient should contain the
			// url
			StorageUtil.fetchTitleAndDescription(clients.getDariahStorageClient(), container);

			collectionContainers.add(container);
			if (container.getDescribesSeafileLibrary() != null) {
				libId2storageId.put(container.getDescribesSeafileLibrary(), container.getStorageId());
				System.out.println(container.getDescribesSeafileLibrary());
				System.out.println(container.getStorageId());
			}
		}

		return libId2storageId;
		*/
		return null;
	}    

	protected void seafileDownload(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String collectionId = request.getParameter("collectionId"); // TODO:
		// better
		// add
		// seafileLibraryRef,
		// would
		// save
		// one
		// storageread?
		String fileId = request.getParameter("fileId");
		String seafileToken = request.getParameter("seafileToken");

		clients.getSeafileClient().setEncryptedToken(seafileToken).handleSeafileDownload(collectionId, fileId,
				response);
	}
}
