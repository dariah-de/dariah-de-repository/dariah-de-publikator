/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './tgform.html';
import $ from 'jquery'; // for post/get,etc

//import tgformfieldOuterTemplate from './tgformfields/tgformfield-outer.html';

import findIndex from 'lodash/findIndex';
import find from 'lodash/find';
import forEach from 'lodash/forEach';

const _ = {
  findIndex,
  find,
  forEach,
}

import tgformsText  from './tgformfields/text';
import tgformsTextarea  from './tgformfields/textarea';
import tgformsHidden from './tgformfields/hidden';

let TGForm = Ractive.extend({
  template: template,
  components: {
    'tgforms:text': tgformsText,
    'tgforms:textarea': tgformsTextarea,
    'tgforms:hidden': tgformsHidden,
  },
  data: {
    // here happens the injection of components basesd on there tgforms:hasInput value
    // e.g.
    getComponent: function(name){
        if (!!this.partials[name]) return name;
        this.partials[name] = '<' + name + '/>';
        return name;
    },
  },
  computed: {
    fileViewLocation: function() {
      const uriParts = this.get('uri').split(':');
      if(uriParts[0] === 'dariahstorage') {
        return this.tgformOptions.viewFileUrl + uriParts[1];
      } else if(uriParts[0] === 'seafile') {
        //return `${this.tgformOptions.seafileDownloadUrl}&collectionId=${this.get('parentUri')}`
        //      + `&fileId=${uriParts[1]}&seafileToken=${this.tgformOptions.seafileAuthToken}`;
        return `https://sftest.de.dariah.eu/files/${uriParts[1]}?token=${this.tgformOptions.seafileAuthToken}`;
      }
    },
    isTopLevelCollection: function(){
        return this.tgformOptions.storageUri === this.get('uri');
    }
  },
  onrender: function() {

    this.on('delete-file', (event) => {
      const uri = this.get('uri');
      const parentUri = this.get('parentUri'); 

      var deleteURI = "";
      if (event === 'collection') {
    	  	deleteURI = this.tgformOptions.deleteSubcollectionUrl;
      } else {
    	  	deleteURI = this.tgformOptions.deleteFileUrl;
      }
      
      $.post(deleteURI, { fileStorageId: uri }).done(result => {
        console.log(`deleted ${uri} from dariahstorage, result: ${result}`);
      });

      // remove the hasPart from the parent
      this.dataModelUtils.removePart(parentUri, uri);
      // remove object itself from formadata array
      const delid =  this.get('formdata').findIndex(e => e.uri == uri);
      this.get('formdata').splice(delid, 1);
      this.pubsub.publish('resource-removed', {uri, parentUri});

    });

    this.on('update-file', (event) => {
      console.log('want to update file');
      console.log(event.original.target.files[0]);

      // input type is not multiple, so only one file arrives here
      const file = event.original.target.files[0];
      const uri = this.get('uri');
      const uriParts = uri.split(':');
      const storageURL = this.tgformOptions.storageUrl + uriParts[1];
      console.log("Storage URL "+ storageURL);

      // disabled, we send the data raw
      //const data = new FormData();
      //data.append('file', file);
      //data.append('storageId', uri);

      const filetype = file.type;
      const filename = file.name;
      
      const ractive = this;

      $.ajax({    	  
        data: file,
        type: 'PUT',
        url: storageURL,
        headers: {
            'Authorization':'bearer '+this.tgformOptions.storageToken,
            'Content-Type': filetype
        },
        processData: false,
        contentType: false,
        beforeSend : function(xhr, settings){
            $('#file-progress').attr('style', 'display: block;');
            ractive.parent.set('showSpinner', true);
            ractive.parent.disableGUI();
        },           
        xhr: function()
        {
          var xhr = new window.XMLHttpRequest();     
          //Upload progress
          xhr.upload.addEventListener("progress", function(evt){
            if (evt.lengthComputable) {
              var progress = Math.round ((evt.loaded / evt.total) * 100);
                $('#file-progress .bar').attr('style', 'width: '+progress + '%');

            }
          }, false);
          return xhr;      
        },  
      }).done((response, status, xhr) => {
            $('#file-progress').attr('style', 'display: none;');
            $('#file-progress .bar').attr('style', 'width: 0%'); 
            ractive.parent.set('showSpinner', false);
            ractive.parent.enableGUI();
            
        let model = this.get('formModel');

        // add title and format info from browser
        this.dataModelUtils.fillOrAppendFormField(model.schema, 'dc:title', filename);
        this.dataModelUtils.fillOrAppendFormField(model.schema, 'dc:format', filetype);
        
        // Disabled because of changed upload to cdstar, in future this might change
        //const jsonResponse = JSON.parse(response);
        //const metadata = jsonResponse.metadata;
        // add extracted metadata from service
       /* _.forEach(metadata, (value, key) => {
          if(value !== null) {
            if(Array.isArray(value)) {
              value.forEach(entry => {
                this.dataModelUtils.fillOrAppendFormField(model.schema, key, entry);
              });
            } else {
              this.dataModelUtils.fillOrAppendFormField(model.schema, key, value);
            }
          }
        }); */
        this.pubsub.publish('data-changed', {uri});
      });
    });
  },

});

export default TGForm;
