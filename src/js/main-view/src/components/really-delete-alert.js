/*-
 * #%L
 * DARIAHDE :: Publikator :: Portlet
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import Ractive from 'ractive';
import template from './really-delete-alert.html';
import $ from 'jquery';

let ReallyDeleteAlert = Ractive.extend({
  template : template,

  onrender: function() {
    this.on( 'delete', function (event) {
    		var collectionId = this.get('storageId');
    		$("<div id='spinner-overlay'><div class='scontent'></div>" ).insertAfter( "body" );
    		this.set('visible', false);
      $.post(this.get('deleteCollectionUrl'), {
        collectionId: collectionId
      }).done((data) => {
    	  	// this causes wrong information on the following collections ...
        this.parent.get('cols').splice(this.get('index'), 1);
    	  	$("#spinner-overlay").remove();
    	  	// workaround Bug #23008
    	  	location.reload(true);
      }).fail((data) => {
        console.log("++ error deleting collection " + collectionId + " ++");
        alert("error deleting collection " + collectionId);
        //    this.set('status', 'service-not-available');
        $("#spinner-overlay").remove();
      });
    });

    this.on( 'close', function (event) {
      this.set('visible', false);
    })
  }
});

export default ReallyDeleteAlert;
